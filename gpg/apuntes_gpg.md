# GPG
    PGP --> Pretty Good Privacy, es el programa original, GPG la version
            de linux, freesoftware

## Teoria

### Proteger datos

            Encriptar/cifrar/amagar
    +-----------+
    |           |
    |           |
    +-----------+

    +----------------------+
    | DOCKER               |
    |           signat     |
    |        +----------+  |
    |  |     |          |  |
    |  |     |     -----|--|-------------------->
    |  |     |          |  |                      apt-get install
    |  |     +----------+  |                      +---------------+
    +--|-------------------+                +---> |               |
       |                                    |     +---------------+
       +-->  /\                             |
           Claves de verificacion ----------+

## Signar
**Propiedades**
1. Intgridad: Los datos son lo que eran inicialmente, sin modificaciones
   Seguridad que los datos no se han alterado

2. Autentificacion: Tenemos la veracidad que el propietario es quien dice ser

3. No repudi: No puedes negar la autoria ni el contenido del mensaje

## Criptografia

1. Simetrica: misma clave para encriptar y para desencriptar, se debe compartir\
                La clave por lo que puede ser vunerada

2. Asimetrica: Dos claves, una publica y otra privada, la publica debe hacerse publica

### Modelos

1. Web of trust --> Confianza entre pares (peers) --> GPG
2. PKI --> public Key infraestructures --> TLS, SSL --> certificats digitals

    *Certificados digitales /= claves publicas y privadas (aunque estas lo conformen)

## Funcionamiento cifrado/signat

            Pere                                    Anna
        -------------              MSG             --------------
            PrivKey  |    <---------------------        Clau publica de Pere
            PubKey   |
        -----------------------------------------------------------------------
        PrivKey --> decifrar  <----cifrado----        PubKey Pere --> cifrar
                               mensaje
        -----------------------------------------------------------------------
        PrivKey --> Signar  <------mensaje signat----   PubKey Pere --> Verificar


    *Las 2 llaves funcionan conjuntamente lo que una hace, la otra lo deshace

        cifrar - deshifrar
        signar - verificar
    
    *Cifrar es más ligero
     Signar es más costoso (tarda más)

          Pere
            +-----------------+
            |    Documento    |             +-----------------+
            |    Grande   ----+------+      |                 |
            |       |         |      |      |       ISO   ----+-----> SHA256
            +-------+---------+      |      |          |      |            +------------+
                    |                |      +----------+------+            +------------+
                    |                |                 |                            =
                   \|/               |                 +------------> SHA256        =
                +-----------------+  |                                     +------------+
                |  +-----------+  |  |                                     +------------+ 
                |  +-----------+  |  |
                |     256 bits    |  +--------->Anna
                +--------------+--+             +--------------+
                               |                |              |
                               +->firma         |   Documento  |
            +-------------+                     |              |
            |   HASH      | <-------------------+              |
            +-------------+                     +--------------+
             =
            +-------------+  <------------------+--------------+
            |   HASH      |                     |   signado    |
            +-------------+                     +--------------+
                                                    PubKey Pere

## Practicando

    useradd -m -s /bin/bash pere
    useradd -m -s /bin/bash marta
    useradd -m -s /bin/bash anna

    passwd pere --> mpere
    passwd marta --> mmarta
    passwd anna --> manna

## Practicando en clase

**PERE**

    export GPT_TTY="$(tty)"
    
    gpg --full-generate-key --> En terminal de texto
        ...pere pou prat
        ...per@edt.org
        ... -->no crea phrase

        *gpg --gen-key --> no hace las preguntas (solo full name y correo)
    
    gpg --list-keys
        //... /home/pere/.gnupg/pubring.kbx --> llavero de claves publicas
            -----------------------------
            pub   rsa3072 2023-03-13 [SC]
                61599E4811C73B740406098942687E14724630BC
            uid           [ultimate] pere pou prat (lo pere de lleide) <per@edt.org>  ---> [ultimate] -->trust (nivel de confianza): ultimate --> maxima
            sub   rsa3072 2023-03-13 [E]
    
    *entropy (antropia): caos, impredecible para generar keys

### exportar pubkey

    gpg --output /tmp/pere.gpg --export per@edt.org

        gpg --armor --output /tmp/pere.gpg --export per@edt.org ---> PEM

        cat /tmp/pere.gpg --> el contenido de una clave convertida a base 64 con un capzalera y un peu --> PEM

**MARTA**

### importamos key de pere

    gpg --import /tmp/pere.gpg

    gpg --list-keys
        /home/marta/.gnupg/pubring.kbx
          ------------------------------
          pub   rsa3072 2023-03-13 [SC]
                61599E4811C73B740406098942687E14724630BC
          uid           [ unknown] pere pou prat (lo pere de lleide) <per@edt.org>
          sub   rsa3072 2023-03-13 [E]

    gpg --edit-key 61599E4811C73B740406098942687E14724630BC  --> tambien se puede con el
                                                                    correo o fullname
        gpg> fpr
        gpg> quit
    
   **En la terminal de texto**

    gpg --gen-key

        ...marta mas moreu
        ...marta@edt.org
        ...
    
   **podemos vovler a terminal grafica**
   
    gpg --list-keys
        //..gpg: checking the trustdb
            gpg: marginals needed: 3  completes needed: 1  trust model: pgp
            gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
            gpg: next trustdb check due at 2025-03-14
            /home/marta/.gnupg/pubring.kbx
            ------------------------------
            pub   rsa3072 2023-03-13 [SC]
                61599E4811C73B740406098942687E14724630BC
            uid           [ unknown] pere pou prat (lo pere de lleide) <per@edt.org>
            sub   rsa3072 2023-03-13 [E]  ------> **trust: unknown**

            pub   rsa3072 2023-03-15 [SC] [expires: 2025-03-14]
                1666E4CD44F9C3D3329538B222C2CA1C419D14C6
            uid           [ultimate] marta mas moreu <marta@edt.org>
            sub   rsa3072 2023-03-15 [E] [expires: 2025-03-14]
   
    gpg --edit-key 61599E4811C73B740406098942687E14724630BC

        gpg> fpr
        gpg> sign --> y
        gpg> check
        gpg> quit --> save chances: y
    
    gpg --list-keys
        //..gpg: checking the trustdb
            gpg: marginals needed: 3  completes needed: 1  trust model: pgp
            gpg: depth: 0  valid:   1  signed:   1  trust: 0-, 0q, 0n, 0m, 0f, 1u
            gpg: depth: 1  valid:   1  signed:   0  trust: 1-, 0q, 0n, 0m, 0f, 0u
            gpg: next trustdb check due at 2025-03-14
            /home/marta/.gnupg/pubring.kbx
            ------------------------------
            pub   rsa3072 2023-03-13 [SC]
                61599E4811C73B740406098942687E14724630BC
            uid           [  full  ] pere pou prat (lo pere de lleide) <per@edt.org>
            sub   rsa3072 2023-03-13 [E]  ------> **trust: full**

            pub   rsa3072 2023-03-15 [SC] [expires: 2025-03-14]
                1666E4CD44F9C3D3329538B222C2CA1C419D14C6
            uid           [ultimate] marta mas moreu <marta@edt.org>
            sub   rsa3072 2023-03-15 [E] [expires: 2025-03-14]
    
        *Ahora marta puede mandarle mensajes cifrados a pere

    cp /etc/os-release /tmp
    mv /tmp/os-realease /tmp/msg
    vim /tmp/msg

        *Alice and Bob --> personjes usados(...)
         Malory --> maligne
    
## Encriptar mensaje (marta)

    gpg --output /tmp/msg.gpg --encrypt --recipient per@edt.org /tmp/msg

    file /tmp/msg.gpg

    cat /tmp/msg.gpg  ---> lo vemos asi porque esta en formato DEM

**PERE**
## Desencriptar

    gpg --decrypt /tmp/msg.gpg --> desencripta y muestra

    gpg --output /tmp/patata --decrypt /tmp/msg --> desencripta y guarda en /tmp/patata

**MARTA**

    gpg -a --output /tmp/msg.gpg --encrypt --recipient per@edt.org /tmp/msg
         |
         +---> --armor
    cat /tmp/msg.gpg --> formato PEM

**PERE**

    cat > dades
        asdasd
        asdasdd
        asdasddd
        11111111111
        22222222222
        3333
    
   **Terminal de texto**

    gpg --output /tmp/dades.gpg --symetric dades

        pasphrase --> jupiter00
    
**MARTA**

    gpg --decrypt /tmp/dades.gpg

        passphrase --> jupiter00

   **ya podemos salir del terminal de texto**

## Signar dades

        Datos
    +-----------+                              +-------------------+
    | _________ |       +-----------+          |  ____________     |
    | _________ |   +   |   Firma   | ----->   |  ____________     |
    | _________ |       +-----------+          |  Datos + firma    |
    +-----------+                              +-------------------+
                    *--sign
                                               +-------------------+
                                               |  _______________  |
                                               |  _______________  |
    +-----------+                              |      Datos        |
    | _________ |       +-----------+          |  _______________  |
    | _________ |   +   |   Firma   | ----->   |  _______________  |
    | _________ |       +-----------+          |  +------------+   |
                                               |  |   Firma    |   |
                    *--clearsign               |  +------------+   |
                                               +-------------------+
    
                                               +-------------------+
                                               | _________________ |
    +-----------+                              | _________________ |
    | _________ |       +-----------+ ------>  +-------------------+
    | _________ |   +   |   Firma   |
    | _________ |       +-----------+ ------>  +-------------------+
    +-----------+                              |    Firma          |
                    *--detach                  +-------------------+



**PERE**

    gpg --output /tmp/dades.gpg --sign dades

    cat /tmp/dades.gpg

    gpg -a --output /tmp/dades.gpg --sign dades

    cat /tmp/dades.gpg

**MARTA**

    gpg --decrypt /tmp/dades

    gpg --output /tmp/patata --decrypt /tmp/dades.gpg

    gpg --verify /tmp/dades.gpg ---> solo verifica la firma
        //..gpg: Signature made Wed 15 Mar 2023 10:17:07 AM CET
            gpg:                using RSA key 61599E4811C73B740406098942687E14724630BC
            gpg: Good signature from "pere pou prat (lo pere de lleide) <per@edt.org>" [full]
    
**PERE**

    gpg --output /tmp/dades-firma.gpg --clearsign /tmp/dades

**MARTA**

    gpg --verify /tmp/dades-firma.gpg

    gpg --decrypt /tmp/dades-firma.gpg

**PERE**

    gpg --output /tmp/dades-firma.gpg --detach-sign dades

    cat /tmp/dades-firma.gpg

    cp dades /tmp/dades

**MARTA**

    gpg --verify /tmp/dades-firma.gpg /tmp/dades
        //..gpg: Signature made Wed 15 Mar 2023 10:32:15 AM CET
            gpg:                using RSA key 61599E4811C73B740406098942687E14724630BC
            gpg: Good signature from "pere pou prat (lo pere de lleide) <per@edt.org>" [full]
    
    gpg --verify /tmp/dades-firma.gpg /etc/os-release 
        //..gpg: Signature made Wed 15 Mar 2023 10:32:15 AM CET
            gpg:                using RSA key 61599E4811C73B740406098942687E14724630BC
            gpg: BAD signature from "pere pou prat (lo pere de lleide) <per@edt.org>" [full]

**PERE**

   **terminal de texto**

    gpg --genkey
        ...perico prat porut
        ...pere@tinder.cat

   **Podemos salir de la terminal de texto**

    gpg --list-keys

    gpg --edit perico
        gpg> list
        gpg> toggle
        gpg> quit

## Exportamos clave
    gpg --output /tmp/pere.gpg --export

**MARTA**

    gpg --import /tmp/pere.gpg

    gpg --list-keys


Mar 20

**Hibridos** --> Actualmente la mayoria usa hibridos; Comparten el secreto mediante
                    algoritmo asimetricos pero, el secreto es algortimo simetrico.

**PERE**

Exportar todas las claves de pere
    gpg -a --output /tmp/pere.gpg --export

**MARTA**

    gpg --import /tmp/pere.gpg
    gpg --list-keys

    gpg --edit-key pere
        gpg> trust
            >3
        gpg> q
    
        PERE                    MARTA
    +----------+            +-----------+                   +---> --edit-key --> sign
    |   Key    | -------->  |           |                   +---> Marta firma la clave
    +----------+ exporta    +--||-------+                   |
                               ||                           |
                               |+--                         |
                               +---             Vality: Unknown --- firma --> Full
                               +-- key pere     Trust: Unknown


    gpg -a --output /tmp/marta.gpg --export

**ANNA**

   **Terminal de texto**

    gpg --gen-key
        ...anna andreu aranu
        ...anna@edt.org
        ...la anna de sempre
   **Podemos salir de la terminal de texto**

    gpg --import /tmp/marta.gpg
    gpg --edit-key pere
    gpg --edit-key marta

**PERE**

firmamos datos
    gpg -a --output /tmp/dades.gpg --clearsign dades

**ANNA**

    gpg --decrypt /tmp/dades.gpg
        //..gpg: WARNING: This key is not certified with a trusted signature!
            --> No nos puede confirmar que realmente sea pere quien lo ha firmado
                unicamente que las claves coinciden
    
**MARTA**

    cat > noms
        ...
        ...
    
    gpg -a --output /tmp/noms.gpg --clearsign noms

**ANNA**

    gpg --decrypt /tmp/noms.gpg
        //..gpg: WARNING: This key is not certified with a trusted signature!
    
    gpg --edit-key marta@edt.org
        gpg>sign
        gpg>q --> y
    
    gpg --list-keys

    gpg --decrypt /tmp/noms.gpg
        //..gpg: Good signature from "marta mas moreu <marta@edt.org>" [full]
    
    gpg --decrypt /tmp/dades.gpg
        //..gpg: WARNING: This key is not certified with a trusted signature!
    
    gpg --edit-keys marta
        gpg>trust
            >5

                    ===> si nos fiamos de marta totalmente, nos lo creemos

    gpg --edit-key pere
        //..vality: Full
    
    gpg /tmp/dades.gpg
        //.. Good...   ---> Ahora ya no hay el WARNING, ya nos fiamos, por que confiamos
                            en marta y ella consfirma que es pere

## Practicando --> enviar mensaje cifrado a Anna

**ANNA**

    exportar keys
    gpg -a --output /tmp/anna.gpg --export

**PERE**

    importar key de anna
    gpg --import /tmp/anna.gpg

    cat > mensaje
        ...
        ...
    
    gpg -a --output /tmp/mensaje.gpg --recipient anna --encrypt mensaje

**ANNA**

    gpg --decrypt /tmp7mensaje.gpg