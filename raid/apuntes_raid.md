# RAID

**raid0** --> Leer siimultaneamente (divide en diferentes stripes)
          reparte datos entre diferentes discos.


**raid1** --> Mirror (copia) // 2 discos
                                                *Tolerancia error: 3\

          200G        300G      |    200G       200G
        +------+   +-------+    | +--------+  +------+
        |      |   |       |    | |        |  |      |
        +------+   +-------+    | +--------+  +------+
                                +-------------------------
                    raid --> Solo aprovechara 200G

**raid4** -->
                                                                                    *Inconveniente:...\

                200G                    300G                        200G
        +------------------+    +--------------------+      +--------------------+
        |   +----------+   |    |   +----------+     |      |   +------------+   |
        |   |   A1     |   |    |   |    A2    |     |      |   |     p1     |   |
        |   +----------+   |    |   +----------+     |      |   +------------+   |
        |                  |    |                    |      |                    |
        |   +----------+   |    |   +----------+     |      |   +------------+   |
        |   |   A3     |   |    |   |   A4     |     |      |   |     p2     |   |
        |   +----------+   |    |   +----------+     |      |   +------------+   |
        +------------------+    |--------------------|      +--------------------+
                                |   XXXXXXXXXXXXXX   |                      |
                                |   XXXXXXXXXXXXXX   |                      +--------> Disco de paridad
                                +--------------------+
                                                                                    *Paridad: |0101010|1| --> #0 = #1
                    "2  3   5"                                                           Codigo de deteccion de errores
                    9  '' 16
                        ? -->7

**raid5** --> Igual al raid4 pero se reparte la paridad entre los discos que hay --> minimo 3 discos

--
Practicando
--

## Crear raid
Con los discos creados en VLM o si no, creamos unos nuevos

    losetup /dev/loop1 disk01.img
    losetup /dev/loop1 disk02.img
    losetup /dev/loop1 disk03.img
    losetup /dev/loop1 disk04.img

    losetup -a

    hexdump -C /dev/loop4 | head

## instalamos paquete RAID                                          
                                                                *DM: device Mapa
    apt-get install mdadm                                        MD: Multi desc

## creamos

    mdadm --create /dev/md0 --chunk=4 --level=1 --raid-devices=3 /dev/loop1 /dev/loop2 /dev/loop3
        *De 3 discos estoy formando 1
            --> Continue creating array? y
    
    cat /proc/mdstat
        +-----------------------+
        | ...............       |
        | .......... [UUU] -----|--> Up Up Up --> los tres discos funcionan
        +-----------------------+
## Lo formateamos

    mkfs -t ext4 /dev/md0

    blkid
        *raid_member

    mount -t ext4

    df -h

    cp ........ /mnt

## comandos mdadm

    mdadm --detail --scan --> busca detalles
    mdadm --detail /dev/md0
    mdadm --query /dev/loop1
    mdadm --examine /dev/loop1
    mdadm /dev/md0 --fail /dev/loop1

    cat /proc/mdstat
        +-------------------+
        |  ..............   |
        |          [_UU]----|-->Un fallo
        +-------------------+
    
    mdadm /dev/md0 --remove /dev/loop1

## Añadimos
**--manage**

    mdadm /dev/md0 --add /dev/loop4
         +
         \-----> --manage --> no es necesario pero se presupone que alli esta

## Parar raid

### antes...
    umount /mnt

### parar

    mdadm --stop /dev/md0

    cat /proc/mdstat

## Como vuelvo a encender el raid

    mdadm --assemble --scan --> busca y crea (aleatorio)

        mdadm --stop /dev/md0

    mdadm --assemble /dev/md0 --run /dev/loop1 /dev/loop2 /dev/loop3
            * No deja el loop1 --> esta marcado como fail, el resto bien
    
**Borrar siempre todo antes de cerrar**

--
--

## Raid5

    mdadm -v --create /dev/md0 --level=5 --raid-devices=3 /dev/loop1 /dev/loop2 /dev/loop3 --spare-devices=1 /dev/loop4

    mdadm /proc/mdstat
        +-----------------------------------+
        |   loop3 loop4[3](S) loop2 loop1   |
        |                  |                |
        |                  +----------------|----->Spare --> recambio
        |                       [UUU]       |
        +-----------------------------------+

## fromateamos y montamos
...

## provocamos fallo
    
    mdadm /dev/md0 --fail /dev/loop1

    cat /proc/mdstat
        *Todo esta bien por que el loop4 ahora ocupa el lugar del loop1

    mdadm /dev/md0 --remove /dev/loop1

    mdadm /dev/md0 --fail /dev/loop2

## Añadimos otra vez loop1

    mdadm /dev/md0 --add /dev/loop1

    mdadm /dev/md0 --remove /dev/loop2

    mdadm /dev/md0 --add /dev/loop2

------------------------+
umount /mnt
mdadm --stop /dev/md0

--
MAr 6
--

losetup /dev/loop1 disk01.img
losetup /dev/loop1 disk02.img
losetup /dev/loop1 disk03.img
losetup /dev/loop1 disk04.img

losetup -a

mdadm --create /dev/md/backup --level=1 level=1 --raid-devices=2 /dev/loop1 /dev/loop2 --spare-devices=1 /dev/loop3

cat /proc/mdstat
mdadm --stop /dev/md0


mdadm --detail /dev/md/backup

tree /dev/disk

cat /etc/mdadm/mdadm.conf --->

cp /etc/mdadm/mdadm.conf   etc/mdadm/mymdadm.conf

mdadm --examine --scan

mdadm --examine --scan > /etc/mdadm/mdadm.conf

vim /etc/mdadm/mdadm.conf

cat /proc/mdstat

mdadm --stop /dev/md/backup

mdadm --assemble /dev/md/bacup

mdadm --stop /dev/md/backup

mdadm --assemble --scan

mdadm --stop /dev/md/backup

rm /etc/mdadm/mdadm.conf

mdadm --assemble --scan ---> Ahora me enciende 2 RAID5

    *--scan --> Si existe mdadm.conf --> aplica file
            --> Si no existe mdadm.conf --> buscara trozos

cat /proc/mdstat

mdadm --stop /dev/md0

cat /proc/mdstat

--
--

    dd if=/dev/zero of=disk05.img bs=1k count=500k status=progress
    dd if=/dev/zero of=disk06.img bs=1k count=500k status=progress

    losetup /dev/loop5 disk05.img
    losetup /dev/loop6 disk06.img

    mdadm --create /dev/md/live --level=1 --raid-devices=2 /dev/loop4 /dev/loop5 --spare-devices=1 /dev/loop6

    cat /proc/mdstat

    mdadm --detail /dev/md/live

    mdadm --examine --scan
    mdadm --examine --scan > /etc/mdadm/mdadm.conf

    mdadm --stop /dev/md/live /dev/md127

    mdadm --assemble --scan
    mdadm --stop --scan
    mdadm --assemble --stop ¿?¿?
    mdadm --assemble --scan

    cp /etc/mdadm/mdadm.conf /etc/mdadm/mdadm.bk

    vim /etc/mdadm/mdadm.conf --> borramos live

    mdadm --assemble --scan

    mdadm --assemble /dev/md/live --run /dev/loop4 /dev/loop5

    cat /proc/mdstat

    --------------------------+
    mdadm --stop --scan       |---------> Siempre
    rm /etc/mdadm/mdadm.conf  |
    --------------------------+

--
## Practicando
--

## 2r --> 3r + 1s

    mdadm --create /dev/md/myraid --level=1 --raid-devices=2 /dev/loop1 /dev/loop2
    
    cat /proc/mdstat

    mdadm --grow /dev/md/myraid --raid-devices=3 --add /dev/loop3
            *--spare-devices --> no se puede

    cat /proc/mdstat

    mdadm /dev/md/myraid --add /dev/loop4 --> se añade como spare device

    mdadm --stop /dev/md/myraid

## level5 (3r + 1s) --> level5 ()

    mdadm --create /dev/md/myraid --level=5 --raid-devices=3 /dev/loop1 /dev/loop2 /dev/loop3 --spare-devices=1 /dev/loop4

    cat /prco/mdstat

    mdadm --grow /dev/md/myraid --raid-devies=5 --add /dev/loop5 /dev/loop6

    cat /proc/mdstat

    mdadm --stop /dev/md/myraid

## level1 --> level5

    mdadm --create /dev/md/myraid --level=1 --raid-devices=2 /dev/loop1 /dev/loop2 --spare-devices=1 /dev/loop3

    mdadm --grow /dev/md/myraid --level=5

    cat /proc/mdstat

    mdadm --detail /dev/md/myraid

    mdadm --stop /dev/md/myraid

--
--
    mdadm --create /dev/md/myraid --level=1 --raid-devices=2 /dev/loop1 /dev/loop2 --spare-devices=2 /dev/loop4 /dev/loop5

    mkfs -t ext4 /dev/md/myraid

    mount /dev/md/myraid /mnt

    cp /boot/vm* /mnt

    dd if=/dev/random of=/mnt/file bs=1k count=60k

    df -ht ext

    mdadm /dev/md/myraid --fail /dev/loop1

    cat /proc/mdstat

    mdadm /dev/md/myraid --fail /dev/loop1

    mdadm /dev/md/myraid --remove /dev/loop1 /dev/loop2

    cat /proc/mdstat

    mdadm --detail /dev/md/myraid

    mdadm --grow /dev/md/myraid --size=max

    mdadm --detail /dev/md/myraid
        +
        |
        +---------> Ha creacido al maximo del disco mas pequeño
    
    df -ht ext4 --> El sistema de ficheros no ha encontrado su ¿?size/area¿? para cambiarlo: el max

    resize2fs /dev/md/myraid --> ha aumentado /dev/md127 a ¿¿max??

    df -ht ext4

    umount /mnt --> primero

    mdadm --stop /dev/md/myraid --> segundo
--
--
    mdadm --examine --scan --> no crea nada

    mdadm --query /dev/loop1

    hexadump -C /dev/loop1 | head

**Eliminar marca de la particion, limpiar**
    mdadm-v --zero-superblock /dev/loop1
    mdadm-v --zero-superblock /dev/loop{2..6}

    mdadm --query /dev/loop1 --> .. is not an r¿?¿ array

    mdadm --examine --scan --> no creara nada

    losetup -d /dev/loop{1..6}