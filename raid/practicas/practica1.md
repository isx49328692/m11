# **Pràctica (1) RAID+LVM  en loops**
 Raid 1 dos dics de 500M.\
 Un segon Raid1 (dos discs de 500M).\
 Primer raid Raid1 crear PV i VG mydisc (no incloure el segon raid).\
 LV de 200M sistema, LV de 100M dades.\
 Mkfs i Muntar i posar-hi dades\
 Incorporar al VG el segon raid Raid2.\
 Incrementar sistema +100M.\
 Provocar un fail a un dels disc del Raid1\
 Eliminar completament el Raid1 del VG.\

## Creamos 4 discos de 500M

	dd if=/dev/zero of=disk01.img bs=1k count=500k
	dd if=/dev/zero of=disk02.img bs=1k count=500k
	dd if=/dev/zero of=disk03.img bs=1k count=500k
	dd if=/dev/zero of=disk04.img bs=1k count=500k

	ls -lh

## Las asignamos a un loopback (/dev/loop[1,2,3,4])

	losetup /dev/loop1 disk01.img
	losetup /dev/loop2 disk02.img
	losetup /dev/loop3 disk03.img
	losetup /dev/loop4 disk04.img

	losetup -a

## Creamos 2 RAID de level 1

	mdadm --create /dev/md0 --level=1 --raid-devices=2 /dev/loop1 /dev/loop2
	mdadm --create /dev/md1 --level=1 --raid-devices=2 /dev/loop3 /dev/loop4

	cat /proc/mdstat

## Primer RAID, crear PV --> physical volume

	pvcreate /dev/md0

		//... succesfully created

## Crear VG 'mydisc' / VG --> Volume Group

	vgcreate mydisc /dev/md0

		//... successfully created

## Creamos LV de 200M 'sistema' y otro LV de 100M 'dades' --> logical Volume

	lvcreate -L 200M -n sistema /dev/mydisc
	lvcreate -L 100M -n dades /dev/mydisc

## Fromateamos las particiones y las montamos

	mkfs -t ext4 /dev/mydisc/sistema
	mkfs -t ext4 /dev/mydisc/dades

### para montarlos debemos crear primero las carpetas

	mkdir /mnt/sistema
	mkdir /mnt/dades

### montamos

	mount /dev/mydisc/sistema /mnt/sistema
	mount /dev/mydisc/dades /mnt/dades

### metemos datos

	cp /boot/vm* /mnt/sistema
	cp /boot/vm* /mnt/dades

## Incorporamos al VG 'mydisc' el sgundo RAID

	vgextend mydisc /dev/md1

		//... successfully extended

## Incrementamos sistema +100M

	lvextend -L +100M /dev/mydisc/sistema

		//... successfully resized

## Provocar un fail a uno de los discos del RAID1

	mdadm /dev/md0 --fail /dev/loop1

	cat /proc/mdstat

## Eliminar completamente el RAID de VG

### primero tendremos que mover los datos a md1, si no, no dejara eliminar --> //... still in use

	pvmove /dev/md0 /dev/md1

### ahora si podemos eliminar

	vgreduce mydisc /dev/md0


## Pasos para cerrar y eliminar todo una vez finalizada la practica (comprobar orden)

	umount /mnt/sistema
	umount /mnt/dades

	lvremove /dev/mydisc/dades
	lvremove /dev/mydisc/sistema

	mdadm --stop /dev/md0
	mdadm --stop /dev/md1

	losetup -d /dev/loop{1..4}


