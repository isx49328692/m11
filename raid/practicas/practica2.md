# **Pràctica (2) REAL**

    Crear sda2: 10G, sda3: 4G, sda4: 4G

    Crear un raid1 amb sda3 i sda4

    generar un VG anomenat diskedt amb sda3 i sda4

    generar lv sistema de 3G, Dades de 1G

    muntar i posar-hi xixa.

    automatitzar el muntatge (/mnt/dades, /mnt/sistema)

    reboot i verificar que dades i sistema estan disponibles.


## Crear las particiones requeridas
    
    fdisk /dev/sda

    n   --> Para crear una nueva partición
    p   --> Para crear una partición primaria
    2   --> Para indicar que será la partición 2
    +10G   --> Para especificar el tamaño de la partición en 10 gigabytes

    n   --> Para crear una nueva partición
    p   --> Para crear una partición primaria
    3   --> Para indicar que será la partición 3
    +4G   --> Para especificar el tamaño de la partición en 4 gigabytes

    n   --> Para crear una nueva partición
    p   --> Para crear una partición primaria
    4   --> Para indicar que será la partición 4
    +4G  --> Para especificar el tamaño de la partición en 4 gigabytes
    
    w   # Para guardar los cambios y salir de fdisk


## Crear el RAID1

    mdadm --create /dev/md0 --level=1 --raid-devices=2 /dev/sda3 /dev/sda4

## Crear PV (¿Es necesario?)

    pvcreate /dev/md0

## Crear VG diskedt

    vgcreate diskedt /dev/md0

## Crear LV (sistema de 3G y datos de 1G)

    lvcreate -n sistema -L 3G diskedt
    lvcreate -n dades -L 1G diskedt


## Formatear y montar los LV

    mkfs -t ext4 /dev/diskedt/sistema
    mkfs -t ext4 /dev/diskedt/dades

### ntes del mount debemos crear los directorios

    mkdir /mnt/sistema
    mkdir /mnt/dades

### montamos
    mount /dev/diskedt/sistema /mnt/sistema
    mount /dev/diskedt/dades /mnt/dades


## Automatizar el montaje (/mnt/sistema y /mnt/dades)

### Editar file /etc/fstab y añadir:

    /dev/diskedt/sistema  /mnt/sistema  ext4  defaults  0  2
    /dev/diskedt/dades  /mnt/dades  ext4  defaults  0  2

### Crear /etc/mdadm/mdadm.conf

    mdadm --examine --scan > /etc/mdadm/mdadm.conf

    ------ARRAY /dev/md/0  metadata=1.2 UUID=863c67ee:3eb41ef5:a27471c9:0429aa6d name=g22:0

## Reiniciar y verificar

### Reiniciamos

    reboot

### Verificamos que esten

    lsblk   --> muestra info de bloques del sistema
    df -h   --> muestra el espacio en disco utilizado y disponible en cada sistema de archivos montado

## Desmontar y eliminar

### Desmontamos

    umount /mnt/dades
    umount /mnt/sistema

### Eliminamos o comentamos los montajes automaticos (mnt/sistema y /mnt/dades)
    vim /etc/fstab

### Eliminamos los logical volume sistema y dades
    lvremove /dev/diskedt/dades
        //... Logical volume "dades" successfully removed

    lvremove /dev/diskedt/sistema
        //... Logical volume "sistema" successfully removed

### paramos el RAID
    mdadm --stop /dev/md127
        //... mdadm: stopped /dev/md127

### ¿Eliminamos el vg?
    vgremove diskedt
        //... Volume group "diskedt" not found
        //... Cannot process volume group diskedt

### Llenamos de zero las particiones sda2, sda3, sda4

    mdadm -v --zero-superblock /dev/sda2
        //... mdadm: Unrecognised md component device - /dev/sda2

    mdadm -v --zero-superblock /dev/sda3
    mdadm -v --zero-superblock /dev/sda4

### Eliminamos mdadm.conf
    
    rm /etc/mdadm/mdadm.conf