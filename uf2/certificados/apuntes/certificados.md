# Certificados

    +----+                        +----+
    |  E | ---------------------->|  R |
    +----+          TLS/SSL       +----+

    https       PKI             asimetrica --> parell de claus
    POPs        CA .. CA                        privada
    SMTPs                                         +
    LDAPs                                     +-----------------+
                                              | publica         |
                           Certificado<------{| id subject      |
                                              | firma del issuer+--->CA emes, clau publica de la CA
                                              +-----------------+
    
    +-----------+
    |           |
    |         30= --> Texto plano <----+
    |           |                      |
    |        443= --> Seguro TLS/SSL   |
    +-----------+                      |
                      STARTTLS --------+--> Convierte la comunicacion en segura por el puerto de texto plano

## Practicando

Crea una clave privada (nos pedira passfrase)

    openssl genrsa -des3 -out ca.key 2048

Al  hacer

    cat ca.key

vemos que ni copiando digito a digito se obtendria la clave, seria siempre necesaria la passfrase

Sin embargo, si creamos la clave como

    openssl genrsa -out prova.key 1024

con un simple cat obtendriamos la clave.

eliminamos la clave prova.key

    rm prova.key

Creamos la clave server.key sin des3

    openssl genrsa -out server.key 2048





------------------------

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -new -x509 -nodes -out cacert.pem -keyout cakey.pem

    Generating a RSA private key
    ........+++++
    .............................................+++++
    writing new private key to 'cakey.pem'
    -----
    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:ca
    State or Province Name (full name) [Some-State]:ca
    Locality Name (eg, city) []:bcn
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
    Organizational Unit Name (eg, section) []:inf
    Common Name (e.g. server FQDN or YOUR name) []:VeritatAbsoluta
    Email Address []:veritat@edt.org

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -dates -in cacert.pem

    notBefore=Apr 14 07:52:39 2023 GMT
    notAfter=May 14 07:52:39 2023 GMT

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -text -dates -in cacert.pem

    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number:
                67:74:76:47:4c:8b:fc:b6:04:65:cc:f7:33:ee:98:b7:13:9f:12:44
            Signature Algorithm: sha256WithRSAEncryption
            Issuer: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
            Validity
                Not Before: Apr 14 07:52:39 2023 GMT
                Not After : May 14 07:52:39 2023 GMT
            Subject: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    RSA Public-Key: (2048 bit)
                    Modulus:
                        00:ca:f9:58:8f:ff:48:b3:fc:6a:fa:0c:9d:c7:0c:
                        97:b3:46:d3:79:ae:b9:ab:ad:fd:6c:60:66:84:9f:
                        b1:c9:f5:dc:02:6f:b9:85:44:7c:fc:14:8f:f1:a3:
                        d6:ce:3a:35:9d:22:13:f2:58:93:0c:ac:08:6a:33:
                        8b:63:4d:ec:9d:53:20:4d:c3:fa:27:53:77:ea:ce:
                        eb:58:ce:e7:26:a8:7f:09:5b:55:4e:10:4f:6a:93:
                        04:92:14:21:4e:59:74:7b:6a:41:50:0b:68:46:bb:
                        d0:4d:59:a0:e5:90:88:dd:a9:1e:22:b3:54:29:8a:
                        d1:97:a8:e9:89:62:70:08:89:1a:07:3a:08:9f:ad:
                        d3:1e:3e:d2:e2:23:e1:35:02:6b:e4:27:e3:f3:b7:
                        4d:6f:6f:e3:cf:2b:98:7e:22:df:e7:e1:87:e2:cd:
                        b7:c9:ad:3b:31:b5:52:43:a6:a9:b3:23:e6:8c:de:
                        1f:13:32:e0:15:7e:4b:64:f7:16:00:af:fa:6b:70:
                        43:73:7e:1d:2f:f1:37:3f:54:c8:5c:6d:d8:15:38:
                        ee:10:4a:99:f1:44:d4:44:58:27:3a:5b:93:2b:4a:
                        b0:e5:0e:04:9d:ee:f7:d7:bf:f2:e2:0c:0c:15:7d:
                        2b:8d:9f:6a:db:97:07:de:70:da:dc:55:0d:82:ab:
                        60:07
                    Exponent: 65537 (0x10001)
            X509v3 extensions:
                X509v3 Subject Key Identifier: 
                    B9:9B:90:63:0E:76:5D:E9:C8:9B:5D:94:AC:2D:A4:2D:5A:20:30:CB
                X509v3 Authority Key Identifier: 
                    keyid:B9:9B:90:63:0E:76:5D:E9:C8:9B:5D:94:AC:2D:A4:2D:5A:20:30:CB

                X509v3 Basic Constraints: critical
                    CA:TRUE
        Signature Algorithm: sha256WithRSAEncryption
            59:04:18:1e:3f:f2:5a:d0:fd:cd:e2:86:22:62:5f:a2:9e:44:
            24:29:6d:a3:5c:49:bd:e2:9b:94:96:60:d7:ec:13:2a:71:fe:
            8d:0b:40:e4:4f:fc:f6:4c:ff:7f:53:7d:2c:f1:44:9a:d9:2e:
            bd:ea:55:6f:fa:c7:e4:dd:9f:80:ba:45:db:64:73:04:d7:67:
            0b:95:e7:3a:02:99:a9:20:0a:2c:72:d8:03:c7:ce:66:e6:af:
            c5:90:8c:51:89:15:b8:a1:b7:ed:6e:60:59:09:3c:80:69:4e:
            fd:d5:48:ee:72:7c:90:62:79:96:7b:7e:7f:34:65:b5:0c:65:
            f3:7b:d7:5a:8b:71:f5:2e:dc:39:3f:77:a2:12:e2:fa:dc:5f:
            eb:62:dc:b7:b4:1f:38:00:8c:9e:bf:9f:3f:01:5f:e9:62:00:
            9c:d3:d2:4b:4d:ed:3f:2b:1a:1d:cd:2d:9b:69:82:10:e4:81:
            10:08:a0:2b:97:93:00:56:4c:7e:f8:5d:66:43:0b:d8:31:65:
            82:6e:d2:68:4b:91:94:c5:cc:f8:ca:ad:73:31:93:3d:58:2a:
            fd:4a:f6:43:03:37:d3:dc:4a:90:25:bb:0e:56:0d:21:97:35:
            c7:f4:c0:c9:a1:d8:8d:da:67:10:a2:26:39:99:dc:8f:00:2b:
            6a:6f:0f:9e
    notBefore=Apr 14 07:52:39 2023 GMT
    notAfter=May 14 07:52:39 2023 GMT
    -----BEGIN CERTIFICATE-----
    MIID3TCCAsWgAwIBAgIUZ3R2R0yL/LYEZcz3M+6YtxOfEkQwDQYJKoZIhvcNAQEL
    BQAwfjELMAkGA1UEBhMCY2ExCzAJBgNVBAgMAmNhMQwwCgYDVQQHDANiY24xDDAK
    BgNVBAoMA2VkdDEMMAoGA1UECwwDaW5mMRgwFgYDVQQDDA9WZXJpdGF0QWJzb2x1
    dGExHjAcBgkqhkiG9w0BCQEWD3Zlcml0YXRAZWR0Lm9yZzAeFw0yMzA0MTQwNzUy
    MzlaFw0yMzA1MTQwNzUyMzlaMH4xCzAJBgNVBAYTAmNhMQswCQYDVQQIDAJjYTEM
    MAoGA1UEBwwDYmNuMQwwCgYDVQQKDANlZHQxDDAKBgNVBAsMA2luZjEYMBYGA1UE
    AwwPVmVyaXRhdEFic29sdXRhMR4wHAYJKoZIhvcNAQkBFg92ZXJpdGF0QGVkdC5v
    cmcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDK+ViP/0iz/Gr6DJ3H
    DJezRtN5rrmrrf1sYGaEn7HJ9dwCb7mFRHz8FI/xo9bOOjWdIhPyWJMMrAhqM4tj
    TeydUyBNw/onU3fqzutYzucmqH8JW1VOEE9qkwSSFCFOWXR7akFQC2hGu9BNWaDl
    kIjdqR4is1QpitGXqOmJYnAIiRoHOgifrdMePtLiI+E1AmvkJ+Pzt01vb+PPK5h+
    It/n4YfizbfJrTsxtVJDpqmzI+aM3h8TMuAVfktk9xYAr/prcENzfh0v8Tc/VMhc
    bdgVOO4QSpnxRNREWCc6W5MrSrDlDgSd7vfXv/LiDAwVfSuNn2rblwfecNrcVQ2C
    q2AHAgMBAAGjUzBRMB0GA1UdDgQWBBS5m5BjDnZd6cibXZSsLaQtWiAwyzAfBgNV
    HSMEGDAWgBS5m5BjDnZd6cibXZSsLaQtWiAwyzAPBgNVHRMBAf8EBTADAQH/MA0G
    CSqGSIb3DQEBCwUAA4IBAQBZBBgeP/Ja0P3N4oYiYl+inkQkKW2jXEm94puUlmDX
    7BMqcf6NC0DkT/z2TP9/U30s8USa2S696lVv+sfk3Z+AukXbZHME12cLlec6Apmp
    IAosctgDx85m5q/FkIxRiRW4obftbmBZCTyAaU791UjucnyQYnmWe35/NGW1DGXz
    e9dai3H1Ltw5P3eiEuL63F/rYty3tB84AIyev58/AV/pYgCc09JLTe0/KxodzS2b
    aYIQ5IEQCKArl5MAVkx++F1mQwvYMWWCbtJoS5GUxcz4yq1zMZM9WCr9SvZDAzfT
    3EqQJbsOVg0hlzXH9MDJodiN2mcQoiY5mdyPACtqbw+e
    -----END CERTIFICATE-----

     
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl genrsa -out server.key.pem

    Generating RSA private key, 2048 bit long modulus (2 primes)
    .....................................................+++++
    .....+++++
    e is 65537 (0x010001)

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl genrsa -out serverkey.pem

    Generating RSA private key, 2048 bit long modulus (2 primes)
    ..............................................................................................+++++
    ..........+++++
    e is 65537 (0x010001)

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -new -key serverkey.pem -out serverreq.pem

    You are about to be asked to enter information that will be incorporated
    into your certificate request.
    What you are about to enter is what is called a Distinguished Name or a DN.
    There are quite a few fields but you can leave some blank
    For some fields there will be a default value,
    If you enter '.', the field will be left blank.
    -----
    Country Name (2 letter code) [AU]:ca
    State or Province Name (full name) [Some-State]:ca
    Locality Name (eg, city) []:bcn
    Organization Name (eg, company) [Internet Widgits Pty Ltd]:inf
    Organizational Unit Name (eg, section) []:inf
    Common Name (e.g. server FQDN or YOUR name) []:web.edt.org    
    Email Address []:email@edt.org

    Please enter the following 'extra' attributes
    to be sent with your certificate request
    A challenge password []:jupiter
    An optional company name []:jupiter

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ ls

    cacert.pem  ca.key  cakey.pem  pub.pem  serverkey.pem  serverreq.pem

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -noout -text -in serverreq.pem

    Certificate Request:
        Data:
            Version: 1 (0x0)
            Subject: C = ca, ST = ca, L = bcn, O = inf, OU = inf, CN = web.edt.org, emailAddress = email@edt.org
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    RSA Public-Key: (2048 bit)
                    Modulus:
                        00:d4:e2:27:bd:35:88:34:40:99:e7:20:2f:bc:00:
                        8c:3f:87:b5:ab:f7:e3:58:e0:c0:41:3f:01:23:e3:
                        46:e5:53:a3:b7:c1:1c:32:c5:53:7f:25:08:77:19:
                        8b:fa:41:c7:94:a8:da:b2:74:22:4a:17:12:99:58:
                        b3:f9:55:34:52:05:ae:d4:e5:e1:c1:af:27:7d:56:
                        b7:f9:0a:51:6b:b6:d6:fb:34:fb:3d:d4:fc:a6:90:
                        12:0e:ae:f2:24:07:7a:0c:14:a0:e8:91:e7:86:3c:
                        9f:ba:cb:31:85:5c:2a:3b:34:42:1d:87:1a:cd:21:
                        f6:32:69:fd:d2:55:5b:b3:d0:95:53:3c:6f:55:8c:
                        08:56:7c:2b:42:33:42:ee:bf:a9:48:07:d0:fe:af:
                        20:55:b3:30:7c:a6:9f:73:60:30:cb:02:69:0d:e8:
                        4c:ef:e1:1f:48:b4:7e:53:29:c4:90:6e:6c:d4:01:
                        24:4e:4f:e4:9f:42:e9:4e:c5:12:4a:5f:60:25:19:
                        ba:ce:47:31:ea:9b:1e:8c:d9:5f:42:75:fd:17:0c:
                        cc:d7:f6:71:d4:87:11:2a:36:d9:8d:93:a7:3d:f8:
                        da:99:fa:72:f9:dc:ae:bf:97:32:c9:5e:35:85:b7:
                        8d:d1:71:6a:82:fb:3b:dc:9e:c0:7c:de:3c:c0:e1:
                        02:7f
                    Exponent: 65537 (0x10001)
            Attributes:
                unstructuredName         :jupiter
                challengePassword        :jupiter
        Signature Algorithm: sha256WithRSAEncryption
            07:48:b7:d5:a9:47:48:41:17:88:49:1c:77:9d:45:29:fd:9c:
            64:4b:a4:d5:f5:6c:9a:27:4a:c7:b9:2a:3c:75:36:b1:30:8a:
            f7:e9:fc:b0:72:93:3e:3e:9d:e3:28:6e:45:98:5b:4e:ce:59:
            50:86:c4:b4:1f:73:ab:e5:b5:2d:62:39:c8:e3:33:96:bc:bb:
            d0:a0:e7:eb:af:da:15:4a:aa:a3:d3:d0:58:88:2d:fb:fc:2a:
            ef:b6:46:ba:61:bd:3e:10:2f:60:1f:4c:a8:f6:01:9f:23:f7:
            75:dc:0c:ce:99:2b:d0:9e:f6:5f:3b:e3:6a:44:fc:8f:b6:29:
            f2:48:95:45:44:c2:67:bb:eb:f5:11:b1:d9:ca:dc:d5:e0:7c:
            c9:ec:b0:3c:7a:f9:16:67:ea:ed:85:04:64:a5:ca:32:be:e7:
            8b:35:ab:cb:a7:99:e3:15:64:51:d8:96:57:cb:41:a3:cf:a2:
            4b:5a:a2:20:70:d3:c6:27:47:14:6c:68:fe:fc:8f:2b:84:bc:
            82:58:e0:0d:0f:65:ca:50:b8:c4:86:07:8c:36:da:3b:69:e5:
            84:63:39:7e:bd:60:e9:97:66:8c:d5:49:aa:d2:e1:bb:be:2c:
            5a:a2:cc:32:ef:9d:bd:46:e6:fc:53:21:97:ad:9e:ec:1e:f9:
            ad:3e:bf:3a

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ ls

    cacert.pem  cakey.pem  serverkey.pem  serverreq.pem

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in serverreq.pem -out servercert.pem

    Signature ok
    subject=C = ca, ST = ca, L = bcn, O = inf, OU = inf, CN = web.edt.org, emailAddress = email@edt.org
    Getting CA Private Key
    cacert.srl: No such file or directory
    140097900340544:error:06067099:digital envelope routines:EVP_PKEY_copy_parameters:different parameters:../crypto/evp/p_lib.c:93:
    140097900340544:error:02001002:system library:fopen:No such file or directory:../crypto/bio/bss_file.c:69:fopen('cacert.srl','r')
    140097900340544:error:2006D080:BIO routines:BIO_new_file:no such file:../crypto/bio/bss_file.c:76:

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in serverreq.pem -out servercert.pem -CAcreateserial

    Signature ok
    subject=C = ca, ST = ca, L = bcn, O = inf, OU = inf, CN = web.edt.org, emailAddress = email@edt.org
    Getting CA Private Key

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -text -in servercert.pem 

    Certificate:
        Data:
            Version: 1 (0x0)
            Serial Number:
                60:a3:d2:bc:42:3b:e7:0f:9b:79:b0:58:51:ae:53:f2:c8:7c:b8:c7
            Signature Algorithm: sha256WithRSAEncryption
            Issuer: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
            Validity
                Not Before: Apr 14 08:04:05 2023 GMT
                Not After : May 14 08:04:05 2023 GMT
            Subject: C = ca, ST = ca, L = bcn, O = inf, OU = inf, CN = web.edt.org, emailAddress = email@edt.org
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    RSA Public-Key: (2048 bit)
                    Modulus:
                        00:d4:e2:27:bd:35:88:34:40:99:e7:20:2f:bc:00:
                        8c:3f:87:b5:ab:f7:e3:58:e0:c0:41:3f:01:23:e3:
                        46:e5:53:a3:b7:c1:1c:32:c5:53:7f:25:08:77:19:
                        8b:fa:41:c7:94:a8:da:b2:74:22:4a:17:12:99:58:
                        b3:f9:55:34:52:05:ae:d4:e5:e1:c1:af:27:7d:56:
                        b7:f9:0a:51:6b:b6:d6:fb:34:fb:3d:d4:fc:a6:90:
                        12:0e:ae:f2:24:07:7a:0c:14:a0:e8:91:e7:86:3c:
                        9f:ba:cb:31:85:5c:2a:3b:34:42:1d:87:1a:cd:21:
                        f6:32:69:fd:d2:55:5b:b3:d0:95:53:3c:6f:55:8c:
                        08:56:7c:2b:42:33:42:ee:bf:a9:48:07:d0:fe:af:
                        20:55:b3:30:7c:a6:9f:73:60:30:cb:02:69:0d:e8:
                        4c:ef:e1:1f:48:b4:7e:53:29:c4:90:6e:6c:d4:01:
                        24:4e:4f:e4:9f:42:e9:4e:c5:12:4a:5f:60:25:19:
                        ba:ce:47:31:ea:9b:1e:8c:d9:5f:42:75:fd:17:0c:
                        cc:d7:f6:71:d4:87:11:2a:36:d9:8d:93:a7:3d:f8:
                        da:99:fa:72:f9:dc:ae:bf:97:32:c9:5e:35:85:b7:
                        8d:d1:71:6a:82:fb:3b:dc:9e:c0:7c:de:3c:c0:e1:
                        02:7f
                    Exponent: 65537 (0x10001)
        Signature Algorithm: sha256WithRSAEncryption
            70:48:23:4a:be:6c:bb:35:b8:25:b5:9a:fe:48:72:ac:84:07:
            08:f6:06:f9:71:fc:34:1b:9e:02:7a:c2:ee:df:58:79:a3:31:
            c6:cb:0a:41:35:50:3e:48:9d:bb:0b:bd:b4:89:30:5c:dc:dd:
            7a:06:fe:ef:e4:d3:c4:e1:c5:8f:ac:f6:aa:ec:12:09:4a:8c:
            47:40:f4:62:a0:42:8e:b6:8a:39:3c:d1:0a:0d:91:69:ea:59:
            47:9c:8d:14:3e:67:5c:4c:9d:66:ba:75:0d:8a:44:54:fd:6b:
            9b:78:e3:fa:1c:52:f4:9c:37:b2:3c:93:be:04:d8:0a:33:f2:
            47:00:f5:3b:35:a7:e9:c0:e2:0d:86:84:b3:de:6e:ce:35:6f:
            0e:10:75:b1:b1:a7:c5:2e:7d:cd:67:48:bc:9e:7f:1d:36:62:
            06:4b:b9:b4:8a:36:36:b9:ab:25:32:bd:81:ef:d9:7a:5f:e3:
            99:ba:2d:95:e6:2f:f7:dd:67:f7:72:a0:28:0a:c5:e7:53:7b:
            e2:34:19:99:22:3e:ea:e2:dd:93:7e:af:a6:ea:05:c4:7a:cb:
            6f:e3:aa:e4:47:75:71:5a:ec:55:ff:6a:fb:20:1f:b5:2b:15:
            a1:cf:86:a0:bd:2a:76:07:46:5a:9a:be:7c:a5:f4:1e:82:31:
            85:99:28:6d

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -text -purpose -in servercert.pem

    Certificate:
        Data:
            Version: 1 (0x0)
            Serial Number:
                60:a3:d2:bc:42:3b:e7:0f:9b:79:b0:58:51:ae:53:f2:c8:7c:b8:c7
            Signature Algorithm: sha256WithRSAEncryption
            Issuer: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
            Validity
                Not Before: Apr 14 08:04:05 2023 GMT
                Not After : May 14 08:04:05 2023 GMT
            Subject: C = ca, ST = ca, L = bcn, O = inf, OU = inf, CN = web.edt.org, emailAddress = email@edt.org
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    RSA Public-Key: (2048 bit)
                    Modulus:
                        00:d4:e2:27:bd:35:88:34:40:99:e7:20:2f:bc:00:
                        8c:3f:87:b5:ab:f7:e3:58:e0:c0:41:3f:01:23:e3:
                        46:e5:53:a3:b7:c1:1c:32:c5:53:7f:25:08:77:19:
                        8b:fa:41:c7:94:a8:da:b2:74:22:4a:17:12:99:58:
                        b3:f9:55:34:52:05:ae:d4:e5:e1:c1:af:27:7d:56:
                        b7:f9:0a:51:6b:b6:d6:fb:34:fb:3d:d4:fc:a6:90:
                        12:0e:ae:f2:24:07:7a:0c:14:a0:e8:91:e7:86:3c:
                        9f:ba:cb:31:85:5c:2a:3b:34:42:1d:87:1a:cd:21:
                        f6:32:69:fd:d2:55:5b:b3:d0:95:53:3c:6f:55:8c:
                        08:56:7c:2b:42:33:42:ee:bf:a9:48:07:d0:fe:af:
                        20:55:b3:30:7c:a6:9f:73:60:30:cb:02:69:0d:e8:
                        4c:ef:e1:1f:48:b4:7e:53:29:c4:90:6e:6c:d4:01:
                        24:4e:4f:e4:9f:42:e9:4e:c5:12:4a:5f:60:25:19:
                        ba:ce:47:31:ea:9b:1e:8c:d9:5f:42:75:fd:17:0c:
                        cc:d7:f6:71:d4:87:11:2a:36:d9:8d:93:a7:3d:f8:
                        da:99:fa:72:f9:dc:ae:bf:97:32:c9:5e:35:85:b7:
                        8d:d1:71:6a:82:fb:3b:dc:9e:c0:7c:de:3c:c0:e1:
                        02:7f
                    Exponent: 65537 (0x10001)
        Signature Algorithm: sha256WithRSAEncryption
            70:48:23:4a:be:6c:bb:35:b8:25:b5:9a:fe:48:72:ac:84:07:
            08:f6:06:f9:71:fc:34:1b:9e:02:7a:c2:ee:df:58:79:a3:31:
            c6:cb:0a:41:35:50:3e:48:9d:bb:0b:bd:b4:89:30:5c:dc:dd:
            7a:06:fe:ef:e4:d3:c4:e1:c5:8f:ac:f6:aa:ec:12:09:4a:8c:
            47:40:f4:62:a0:42:8e:b6:8a:39:3c:d1:0a:0d:91:69:ea:59:
            47:9c:8d:14:3e:67:5c:4c:9d:66:ba:75:0d:8a:44:54:fd:6b:
            9b:78:e3:fa:1c:52:f4:9c:37:b2:3c:93:be:04:d8:0a:33:f2:
            47:00:f5:3b:35:a7:e9:c0:e2:0d:86:84:b3:de:6e:ce:35:6f:
            0e:10:75:b1:b1:a7:c5:2e:7d:cd:67:48:bc:9e:7f:1d:36:62:
            06:4b:b9:b4:8a:36:36:b9:ab:25:32:bd:81:ef:d9:7a:5f:e3:
            99:ba:2d:95:e6:2f:f7:dd:67:f7:72:a0:28:0a:c5:e7:53:7b:
            e2:34:19:99:22:3e:ea:e2:dd:93:7e:af:a6:ea:05:c4:7a:cb:
            6f:e3:aa:e4:47:75:71:5a:ec:55:ff:6a:fb:20:1f:b5:2b:15:
            a1:cf:86:a0:bd:2a:76:07:46:5a:9a:be:7c:a5:f4:1e:82:31:
            85:99:28:6d
    Certificate purposes:
    SSL client : Yes
    SSL client CA : No
    SSL server : Yes
    SSL server CA : No
    Netscape SSL server : Yes
    Netscape SSL server CA : No
    S/MIME signing : Yes
    S/MIME signing CA : No
    S/MIME encryption : Yes
    S/MIME encryption CA : No
    CRL signing : Yes
    CRL signing CA : No
    Any Purpose : Yes
    Any Purpose CA : Yes
    OCSP helper : Yes
    OCSP helper CA : No
    Time Stamp signing : No
    Time Stamp signing CA : No

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -purpose -in servercert.pem

    Certificate purposes:
    SSL client : Yes
    SSL client CA : No
    SSL server : Yes
    SSL server CA : No
    Netscape SSL server : Yes
    Netscape SSL server CA : No
    S/MIME signing : Yes
    S/MIME signing CA : No
    S/MIME encryption : Yes
    S/MIME encryption CA : No
    CRL signing : Yes
    CRL signing CA : No
    Any Purpose : Yes
    Any Purpose CA : Yes
    OCSP helper : Yes
    OCSP helper CA : No
    Time Stamp signing : No
    Time Stamp signing CA : No

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ vim ca.conf

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ ls

    cacert.pem  cacert.srl  ca.conf  cakey.pem  servercert.pem  serverkey.pem  serverreq.pem

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in serverreq.pem -day 365 -extfile ca.conf -CAcreateserial -out servercert2.pem

    x509: Unrecognized flag day
    x509: Use -help for summary.

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -CA cacert.pem -CAkey cakey.pem -req -in serverreq.pem -days 365 -extfile ca.conf -CAcreateserial -out servercert2.pem

    Signature ok
    subject=C = ca, ST = ca, L = bcn, O = inf, OU = inf, CN = web.edt.org, emailAddress = email@edt.org
    Getting CA Private Key

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -text -purpose -in servercert2.pem

    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number:
                60:a3:d2:bc:42:3b:e7:0f:9b:79:b0:58:51:ae:53:f2:c8:7c:b8:c8
            Signature Algorithm: sha256WithRSAEncryption
            Issuer: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
            Validity
                Not Before: Apr 14 08:14:45 2023 GMT
                Not After : Apr 13 08:14:45 2024 GMT
            Subject: C = ca, ST = ca, L = bcn, O = inf, OU = inf, CN = web.edt.org, emailAddress = email@edt.org
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    RSA Public-Key: (2048 bit)
                    Modulus:
                        00:d4:e2:27:bd:35:88:34:40:99:e7:20:2f:bc:00:
                        8c:3f:87:b5:ab:f7:e3:58:e0:c0:41:3f:01:23:e3:
                        46:e5:53:a3:b7:c1:1c:32:c5:53:7f:25:08:77:19:
                        8b:fa:41:c7:94:a8:da:b2:74:22:4a:17:12:99:58:
                        b3:f9:55:34:52:05:ae:d4:e5:e1:c1:af:27:7d:56:
                        b7:f9:0a:51:6b:b6:d6:fb:34:fb:3d:d4:fc:a6:90:
                        12:0e:ae:f2:24:07:7a:0c:14:a0:e8:91:e7:86:3c:
                        9f:ba:cb:31:85:5c:2a:3b:34:42:1d:87:1a:cd:21:
                        f6:32:69:fd:d2:55:5b:b3:d0:95:53:3c:6f:55:8c:
                        08:56:7c:2b:42:33:42:ee:bf:a9:48:07:d0:fe:af:
                        20:55:b3:30:7c:a6:9f:73:60:30:cb:02:69:0d:e8:
                        4c:ef:e1:1f:48:b4:7e:53:29:c4:90:6e:6c:d4:01:
                        24:4e:4f:e4:9f:42:e9:4e:c5:12:4a:5f:60:25:19:
                        ba:ce:47:31:ea:9b:1e:8c:d9:5f:42:75:fd:17:0c:
                        cc:d7:f6:71:d4:87:11:2a:36:d9:8d:93:a7:3d:f8:
                        da:99:fa:72:f9:dc:ae:bf:97:32:c9:5e:35:85:b7:
                        8d:d1:71:6a:82:fb:3b:dc:9e:c0:7c:de:3c:c0:e1:
                        02:7f
                    Exponent: 65537 (0x10001)
            X509v3 extensions:
                X509v3 Basic Constraints: critical
                    CA:FALSE
                X509v3 Extended Key Usage: 
                    TLS Web Server Authentication, E-mail Protection
        Signature Algorithm: sha256WithRSAEncryption
            ac:c0:68:45:09:23:00:37:1b:f4:e5:e3:44:8c:5c:bd:49:a8:
            d1:33:c2:54:7d:29:a0:d5:0e:3a:03:2a:4d:a7:53:69:0c:05:
            5c:16:8a:fa:73:db:4d:c5:00:9f:6c:d9:37:a9:84:dd:3f:5f:
            a6:b7:f1:af:21:fb:58:aa:73:e8:cc:03:cb:81:2b:78:20:9a:
            49:18:97:09:5d:67:35:95:f7:18:8a:29:e8:e4:b4:da:ec:65:
            aa:7a:5c:4e:ee:b0:92:f3:87:d3:13:04:c0:94:11:90:d8:46:
            ea:8c:56:18:c0:26:b6:06:3f:55:a3:ab:2a:81:89:b2:c3:ef:
            4b:99:c0:ce:a7:16:ee:f9:fb:e8:89:ab:2f:8c:11:47:3d:c3:
            8a:6a:d7:4f:ab:f7:20:60:1d:1f:fe:a6:16:e5:70:d4:d0:4a:
            5d:94:9d:52:e8:12:2d:00:cf:39:81:b8:c6:a6:c8:eb:fc:c5:
            ff:06:04:78:ba:bc:9e:fd:92:ad:6e:49:5a:2c:d1:1e:51:b4:
            4c:dc:75:f1:2e:4b:96:b6:28:9b:73:9b:0b:85:c6:04:04:88:
            ea:01:b3:d9:7c:66:78:16:f4:fb:92:c6:c0:05:0e:f7:a3:68:
            86:46:88:7c:4f:f3:19:47:7a:b2:4f:58:40:43:c6:c8:46:b6:
            c0:ae:8f:7f
    Certificate purposes:
    SSL client : No
    SSL client CA : No
    SSL server : Yes
    SSL server CA : No
    Netscape SSL server : Yes
    Netscape SSL server CA : No
    S/MIME signing : Yes
    S/MIME signing CA : No
    S/MIME encryption : Yes
    S/MIME encryption CA : No
    CRL signing : Yes
    CRL signing CA : No
    Any Purpose : Yes
    Any Purpose CA : Yes
    OCSP helper : Yes
    OCSP helper CA : No
    Time Stamp signing : No
    Time Stamp signing CA : No


a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl s_client mail.google.com:443

    CONNECTED(00000003)
    depth=2 C = US, O = Google Trust Services LLC, CN = GTS Root R1
    verify return:1
    depth=1 C = US, O = Google Trust Services LLC, CN = GTS CA 1C3
    verify return:1
    depth=0 CN = mail.google.com
    verify return:1
    ---
    Certificate chain
    0 s:CN = mail.google.com
    i:C = US, O = Google Trust Services LLC, CN = GTS CA 1C3
    1 s:C = US, O = Google Trust Services LLC, CN = GTS CA 1C3
    i:C = US, O = Google Trust Services LLC, CN = GTS Root R1
    2 s:C = US, O = Google Trust Services LLC, CN = GTS Root R1
    i:C = BE, O = GlobalSign nv-sa, OU = Root CA, CN = GlobalSign Root CA
    ---
    Server certificate
    -----BEGIN CERTIFICATE-----
    MIIEmjCCA4KgAwIBAgIQMYw//8YH2y4KuAJcSEiQjDANBgkqhkiG9w0BAQsFADBG
    MQswCQYDVQQGEwJVUzEiMCAGA1UEChMZR29vZ2xlIFRydXN0IFNlcnZpY2VzIExM
    QzETMBEGA1UEAxMKR1RTIENBIDFDMzAeFw0yMzAzMjAwODI2MzNaFw0yMzA2MTIw
    ODI2MzJaMBoxGDAWBgNVBAMTD21haWwuZ29vZ2xlLmNvbTBZMBMGByqGSM49AgEG
    CCqGSM49AwEHA0IABI/PvD5OqkOkXqjfO5z9Xtux5yVYVNaBNMNGv3Tp0szgYZaH
    eVRVWRxcomnBjamxd/MBEHWThbdZOPrblQ/9wpKjggJ5MIICdTAOBgNVHQ8BAf8E
    BAMCB4AwEwYDVR0lBAwwCgYIKwYBBQUHAwEwDAYDVR0TAQH/BAIwADAdBgNVHQ4E
    FgQUpmUQRuZgh5iakLj7JIJ6x2zDdhowHwYDVR0jBBgwFoAUinR/r4XN7pXNPZzQ
    4kYU83E1HScwagYIKwYBBQUHAQEEXjBcMCcGCCsGAQUFBzABhhtodHRwOi8vb2Nz
    cC5wa2kuZ29vZy9ndHMxYzMwMQYIKwYBBQUHMAKGJWh0dHA6Ly9wa2kuZ29vZy9y
    ZXBvL2NlcnRzL2d0czFjMy5kZXIwLAYDVR0RBCUwI4IPbWFpbC5nb29nbGUuY29t
    ghBpbmJveC5nb29nbGUuY29tMCEGA1UdIAQaMBgwCAYGZ4EMAQIBMAwGCisGAQQB
    1nkCBQMwPAYDVR0fBDUwMzAxoC+gLYYraHR0cDovL2NybHMucGtpLmdvb2cvZ3Rz
    MWMzL2ZWSnhiVi1LdG1rLmNybDCCAQMGCisGAQQB1nkCBAIEgfQEgfEA7wB2AOg+
    0No+9QY1MudXKLyJa8kD08vREWvs62nhd31tBr1uAAABhv5XhLoAAAQDAEcwRQIg
    VYxQR8RHyKeLWHSoIlILvGkfW7dH318Hi9aox8NxVcsCIQCKTWALM+PllFqeYfps
    /KI5gkfG1W0Ij173aD6W4259pwB1AHoyjFTYty22IOo44FIe6YQWcDIThU070ivB
    OlejUutSAAABhv5XhO4AAAQDAEYwRAIgPkYmSSX7MbvSd7NnyfZYDdRaiun0MheJ
    qami2/WxvU8CIECSvYNjdBUcg/WcmlEquQxxq16hp5kOkuailnN8IhJ+MA0GCSqG
    SIb3DQEBCwUAA4IBAQADp8PqTZO8j4beSzL9FZHloSox1alI6DlQdqNGp4hT8Qd6
    qp12KFPfg8z1AaL+95P1Ip+oQCGjZAvWnas/NSwyE0/v8bz5vMlJqqfnIElawOSJ
    5quh+pH6m5k1/bR7F4xeoZSl3+IcBzfIlJnpFTYmPwZTX5Iwp4e0zPCNoSuZQI+B
    PMo7E4gmoFSC59V4T1DQnyuYpyS7Nbqfq3pG+sK9LNL4ki5x9PyqZ6gFO8ALVQtu
    pTzqVSPHbpGSUg6ctK6G7DzFI2+i+OvzJMwXMY8sJOAF3RM2V02C8n3pU1tbYQID
    kDBi5VYLyZKVjsm6vE5kHdlfYrkX19o5YtoY0w9M
    -----END CERTIFICATE-----
    subject=CN = mail.google.com

    issuer=C = US, O = Google Trust Services LLC, CN = GTS CA 1C3

    ---
    No client certificate CA names sent
    Peer signing digest: SHA256
    Peer signature type: ECDSA
    Server Temp Key: X25519, 253 bits
    ---
    SSL handshake has read 4314 bytes and written 387 bytes
    Verification: OK
    ---
    New, TLSv1.3, Cipher is TLS_AES_256_GCM_SHA384
    Server public key is 256 bit
    Secure Renegotiation IS NOT supported
    Compression: NONE
    Expansion: NONE
    No ALPN negotiated
    Early data was not sent
    Verify return code: 0 (ok)
    ---
    

    q
    ---
    Post-Handshake New Session Ticket arrived:
    SSL-Session:
        Protocol  : TLSv1.3
        Cipher    : TLS_AES_256_GCM_SHA384
        Session-ID: 72BAD935B1DF68F59B194A8EA65BBE8BED930FB02A7BBFB050BE6BC6B94EFACD
        Session-ID-ctx: 
        Resumption PSK: A37A94F6DCE50EAFFB96745DCCC4E4782455B7603DCFAE4CCF53B5D046404EEE3B578C0058C27BC8E8A91C7F5AD3A6CB
        PSK identity: None
        PSK identity hint: None
        SRP username: None
        TLS session ticket lifetime hint: 172800 (seconds)
        TLS session ticket:
        0000 - 02 b7 3b c0 9b d6 c9 b8-d5 32 16 50 82 1b 32 72   ..;......2.P..2r
        0010 - 14 eb 6d 74 51 1e 05 71-99 58 4f d3 d7 e6 84 f9   ..mtQ..q.XO.....
        0020 - 6b 6d 09 42 1f 95 b8 87-ed 68 43 37 ff ca b8 2a   km.B.....hC7...*
        0030 - e7 33 4a 24 eb 17 74 b3-11 53 6f 9a ad 80 61 ee   .3J$..t..So...a.
        0040 - 93 94 cd 21 29 d9 e2 a0-79 b0 29 2e e8 be bb 68   ...!)...y.)....h
        0050 - a8 0b 44 b4 93 07 c7 87-ed e7 ee fc 10 f7 17 6f   ..D............o
        0060 - 1f 3f a7 da 1d 46 41 66-a5 6c 47 6b 18 12 f8 fa   .?...FAf.lGk....
        0070 - c1 fd 87 cf 7e ce 68 11-29 f0 2d 9a 90 7b 4b 86   ....~.h.).-..{K.
        0080 - 4a 19 fc 08 fc e6 63 8d-68 a6 b9 75 1d c3 aa fc   J.....c.h..u....
        0090 - 40 6b ad b8 3c 2e 43 76-fc 69 e7 1a 2d cd 6f 0d   @k..<.Cv.i..-.o.
        00a0 - 14 f1 d2 3e 8c 7e e7 9a-da 34 3a 42 71 26 07 0e   ...>.~...4:Bq&..
        00b0 - 64 f1 84 44 dd 81 82 46-1b ca fe b3 1c 01 14 be   d..D...F........
        00c0 - 14 0c db 9b 73 d7 68 86-bf 92 dc 4b 55 03 58 33   ....s.h....KU.X3
        00d0 - fa 31 e2 1f 00 f1 3b c8-21 94 8f 47 98 90 a6 79   .1....;.!..G...y
        00e0 - d0 40 90 e1 5a 13 e2 e0-e1 1e 4d 41 44 54 d7 5e   .@..Z.....MADT.^
        00f0 - d5 68 03 f2 e2                                    .h...

        Start Time: 1681460744
        Timeout   : 7200 (sec)
        Verify return code: 0 (ok)
        Extended master secret: no
        Max Early Data: 14336
    ---
    read R BLOCK
    ---
    Post-Handshake New Session Ticket arrived:
    SSL-Session:
        Protocol  : TLSv1.3
        Cipher    : TLS_AES_256_GCM_SHA384
        Session-ID: C49B201C33F2EBF104D888E464DA0D00CF11C0D5602A2D1AE731348B992840A3
        Session-ID-ctx: 
        Resumption PSK: F6E1FFB0C9AC1BD7A0DCA2AEACFD3E67C66C35C5F2D73B806DE3578442E065B2BF4288293BAD1412CD28C624FC7899BD
        PSK identity: None
        PSK identity hint: None
        SRP username: None
        TLS session ticket lifetime hint: 172800 (seconds)
        TLS session ticket:
        0000 - 02 b7 3b c0 9b d6 c9 b8-d5 32 16 50 82 1b 32 72   ..;......2.P..2r
        0010 - f0 e2 36 f8 52 6d 5d ae-97 6c b3 7a 74 00 bd 45   ..6.Rm]..l.zt..E
        0020 - f0 6e 76 97 dc 93 7d 2d-4e b3 91 27 66 ff 50 22   .nv...}-N..'f.P"
        0030 - 04 dc 61 3f ee 48 de 15-db 8f 6f f7 0f ca 4c 16   ..a?.H....o...L.
        0040 - e9 cb 2c 28 07 6c 18 2e-0d 33 bf e8 79 57 44 57   ..,(.l...3..yWDW
        0050 - eb 79 5e 4c 2a d8 21 97-43 5d 3f 97 76 20 87 9f   .y^L*.!.C]?.v ..
        0060 - 2e ef f2 b3 4f 41 31 92-87 30 75 4b 1f 24 cc 55   ....OA1..0uK.$.U
        0070 - d2 2e ea 68 fc 90 a4 7a-c3 5b 67 41 05 ed 63 f8   ...h...z.[gA..c.
        0080 - 17 48 8e 27 a1 5c 0c fe-b6 1b 07 c8 33 0e c8 3c   .H.'.\......3..<
        0090 - 1a d5 f2 c0 c8 91 dd 2f-57 5e 60 c1 70 60 9b f1   ......./W^`.p`..
        00a0 - 61 d3 ea 35 2b 43 9b a4-21 68 cd 9f 40 d6 20 32   a..5+C..!h..@. 2
        00b0 - 71 5d 89 21 e2 b3 6e 73-57 71 52 1b 2b 26 fd 6d   q].!..nsWqR.+&.m
        00c0 - 56 fa d8 81 29 89 28 71-e2 e5 86 30 55 e7 4e 82   V...).(q...0U.N.
        00d0 - 70 45 09 c1 49 46 6d 04-dd bc ce b9 61 bc 20 8f   pE..IFm.....a. .
        00e0 - 6d f9 76 c0 24 8a ac 71-b4 89 4d 41 44 50 73 ba   m.v.$..q..MADPs.
        00f0 - 32 74 fa f4 c9                                    2t...

        Start Time: 1681460744
        Timeout   : 7200 (sec)
        Verify return code: 0 (ok)
        Extended master secret: no
        Max Early Data: 14336
    ---
    read R BLOCK
    HTTP/1.0 400 Bad Request
    Content-Type: text/html; charset=UTF-8
    Referrer-Policy: no-referrer
    Content-Length: 1555
    Date: Fri, 14 Apr 2023 08:25:44 GMT

    <!DOCTYPE html>
    <html lang=en>
    <meta charset=utf-8>
    <meta name=viewport content="initial-scale=1, minimum-scale=1, width=device-width">
    <title>Error 400 (Bad Request)!!1</title>
    <style>
        *{margin:0;padding:0}html,code{font:15px/22px arial,sans-serif}html{background:#fff;color:#222;padding:15px}body{margin:7% auto 0;max-width:390px;min-height:180px;padding:30px 0 15px}* > body{background:url(//www.google.com/images/errors/robot.png) 100% 5px no-repeat;padding-right:205px}p{margin:11px 0 22px;overflow:hidden}ins{color:#777;text-decoration:none}a img{border:0}@media screen and (max-width:772px){body{background:none;margin-top:0;max-width:none;padding-right:0}}#logo{background:url(//www.google.com/images/branding/googlelogo/1x/googlelogo_color_150x54dp.png) no-repeat;margin-left:-5px}@media only screen and (min-resolution:192dpi){#logo{background:url(//www.google.com/images/branding/googlelogo/2x/googlelogo_color_150x54dp.png) no-repeat 0% 0%/100% 100%;-moz-border-image:url(//www.google.com/images/branding/googlelogo/2x/googlelogo_color_150x54dp.png) 0}}@media only screen and (-webkit-min-device-pixel-ratio:2){#logo{background:url(//www.google.com/images/branding/googlelogo/2x/googlelogo_color_150x54dp.png) no-repeat;-webkit-background-size:100% 100%}}#logo{display:inline-block;height:54px;width:150px}
    </style>
    <a href=//www.google.com/><span id=logo aria-label=Google></span></a>
    <p><b>400.</b> <ins>That’s an error.</ins>
    <p>Your client has issued a malformed or illegal request.  <ins>That’s all we know.</ins>
    read:errno=0

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ vim google.pem

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -text google.pem 

    x509: Unknown parameter google.pem

    x509: Use -help for summary.

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -text -in google.pem

    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number:
                31:8c:3f:ff:c6:07:db:2e:0a:b8:02:5c:48:48:90:8c
            Signature Algorithm: sha256WithRSAEncryption
            Issuer: C = US, O = Google Trust Services LLC, CN = GTS CA 1C3
            Validity
                Not Before: Mar 20 08:26:33 2023 GMT
                Not After : Jun 12 08:26:32 2023 GMT
            Subject: CN = mail.google.com
            Subject Public Key Info:
                Public Key Algorithm: id-ecPublicKey
                    Public-Key: (256 bit)
                    pub:
                        04:8f:cf:bc:3e:4e:aa:43:a4:5e:a8:df:3b:9c:fd:
                        5e:db:b1:e7:25:58:54:d6:81:34:c3:46:bf:74:e9:
                        d2:cc:e0:61:96:87:79:54:55:59:1c:5c:a2:69:c1:
                        8d:a9:b1:77:f3:01:10:75:93:85:b7:59:38:fa:db:
                        95:0f:fd:c2:92
                    ASN1 OID: prime256v1
                    NIST CURVE: P-256
            X509v3 extensions:
                X509v3 Key Usage: critical
                    Digital Signature
                X509v3 Extended Key Usage: 
                    TLS Web Server Authentication
                X509v3 Basic Constraints: critical
                    CA:FALSE
                X509v3 Subject Key Identifier: 
                    A6:65:10:46:E6:60:87:98:9A:90:B8:FB:24:82:7A:C7:6C:C3:76:1A
                X509v3 Authority Key Identifier: 
                    keyid:8A:74:7F:AF:85:CD:EE:95:CD:3D:9C:D0:E2:46:14:F3:71:35:1D:27

                Authority Information Access: 
                    OCSP - URI:http://ocsp.pki.goog/gts1c3
                    CA Issuers - URI:http://pki.goog/repo/certs/gts1c3.der

                X509v3 Subject Alternative Name: 
                    DNS:mail.google.com, DNS:inbox.google.com
                X509v3 Certificate Policies: 
                    Policy: 2.23.140.1.2.1
                    Policy: 1.3.6.1.4.1.11129.2.5.3

                X509v3 CRL Distribution Points: 

                    Full Name:
                    URI:http://crls.pki.goog/gts1c3/fVJxbV-Ktmk.crl

                CT Precertificate SCTs: 
                    Signed Certificate Timestamp:
                        Version   : v1 (0x0)
                        Log ID    : E8:3E:D0:DA:3E:F5:06:35:32:E7:57:28:BC:89:6B:C9:
                                    03:D3:CB:D1:11:6B:EC:EB:69:E1:77:7D:6D:06:BD:6E
                        Timestamp : Mar 20 09:26:33.914 2023 GMT
                        Extensions: none
                        Signature : ecdsa-with-SHA256
                                    30:45:02:20:55:8C:50:47:C4:47:C8:A7:8B:58:74:A8:
                                    22:52:0B:BC:69:1F:5B:B7:47:DF:5F:07:8B:D6:A8:C7:
                                    C3:71:55:CB:02:21:00:8A:4D:60:0B:33:E3:E5:94:5A:
                                    9E:61:FA:6C:FC:A2:39:82:47:C6:D5:6D:08:8F:5E:F7:
                                    68:3E:96:E3:6E:7D:A7
                    Signed Certificate Timestamp:
                        Version   : v1 (0x0)
                        Log ID    : 7A:32:8C:54:D8:B7:2D:B6:20:EA:38:E0:52:1E:E9:84:
                                    16:70:32:13:85:4D:3B:D2:2B:C1:3A:57:A3:52:EB:52
                        Timestamp : Mar 20 09:26:33.966 2023 GMT
                        Extensions: none
                        Signature : ecdsa-with-SHA256
                                    30:44:02:20:3E:46:26:49:25:FB:31:BB:D2:77:B3:67:
                                    C9:F6:58:0D:D4:5A:8A:E9:F4:32:17:89:A9:A9:A2:DB:
                                    F5:B1:BD:4F:02:20:40:92:BD:83:63:74:15:1C:83:F5:
                                    9C:9A:51:2A:B9:0C:71:AB:5E:A1:A7:99:0E:92:E6:A2:
                                    96:73:7C:22:12:7E
        Signature Algorithm: sha256WithRSAEncryption
            03:a7:c3:ea:4d:93:bc:8f:86:de:4b:32:fd:15:91:e5:a1:2a:
            31:d5:a9:48:e8:39:50:76:a3:46:a7:88:53:f1:07:7a:aa:9d:
            76:28:53:df:83:cc:f5:01:a2:fe:f7:93:f5:22:9f:a8:40:21:
            a3:64:0b:d6:9d:ab:3f:35:2c:32:13:4f:ef:f1:bc:f9:bc:c9:
            49:aa:a7:e7:20:49:5a:c0:e4:89:e6:ab:a1:fa:91:fa:9b:99:
            35:fd:b4:7b:17:8c:5e:a1:94:a5:df:e2:1c:07:37:c8:94:99:
            e9:15:36:26:3f:06:53:5f:92:30:a7:87:b4:cc:f0:8d:a1:2b:
            99:40:8f:81:3c:ca:3b:13:88:26:a0:54:82:e7:d5:78:4f:50:
            d0:9f:2b:98:a7:24:bb:35:ba:9f:ab:7a:46:fa:c2:bd:2c:d2:
            f8:92:2e:71:f4:fc:aa:67:a8:05:3b:c0:0b:55:0b:6e:a5:3c:
            ea:55:23:c7:6e:91:92:52:0e:9c:b4:ae:86:ec:3c:c5:23:6f:
            a2:f8:eb:f3:24:cc:17:31:8f:2c:24:e0:05:dd:13:36:57:4d:
            82:f2:7d:e9:53:5b:5b:61:02:03:90:30:62:e5:56:0b:c9:92:
            95:8e:c9:ba:bc:4e:64:1d:d9:5f:62:b9:17:d7:da:39:62:da:
            18:d3:0f:4c

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ docker run --rm --name web.edt.org -h web.edt.org --net 2hisx -d edtasixm11/tls18:https

    Unable to find image 'edtasixm11/tls18:https' locally
    https: Pulling from edtasixm11/tls18
    c8ae8b35783e: Pull complete 
    4fb3e0984ffa: Pull complete 
    a26ed1db3034: Pull complete 
    c4b89fc71810: Pull complete 
    12f547bb1763: Pull complete 
    0775602c9095: Pull complete 
    Digest: sha256:b5ca1bb26fd779e5ac36501317ef9d78ad46450efeccf2c04d7f76bc520ea4da
    Status: Downloaded newer image for edtasixm11/tls18:https
    e7808777be4d34373e42b1f27e75aabdf0b8997df26e66559cbcabc09ca56904

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ docker exec -it web.edt.org /bin/bash

## Doocker
[root@web docker]# ps ax

    PID TTY      STAT   TIME COMMAND
      1 ?        Ss     0:00 /bin/bash /opt/docker/startup.sh
     33 ?        S      0:00 /usr/sbin/httpd -DFOREGROUND
     34 ?        S      0:00 /usr/sbin/httpd -DFOREGROUND
     35 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
     36 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
     43 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
    249 pts/0    Ss     0:00 /bin/bash
    267 pts/0    R+     0:00 ps ax

[root@web docker]# httpd -S

    [Fri Apr 14 08:34:51.191217 2023] [core:error] [pid 268:tid 139853607075904] (EAI 2)Name or service not known: AH00547: Could not resolve host name www.auto1.cat -- ignoring!
    [Fri Apr 14 08:34:51.192404 2023] [core:error] [pid 268:tid 139853607075904] (EAI 2)Name or service not known: AH00547: Could not resolve host name www.auto2.cat -- ignoring!
    VirtualHost configuration:
    217.70.184.56:443      www.web2.org (/etc/httpd/conf.d/webs.conf:34)
    205.185.116.171:443    www.web1.org (/etc/httpd/conf.d/webs.conf:23)
    *:443                  web.edt.org (/etc/httpd/conf.d/ssl.conf:56)
    ServerRoot: "/etc/httpd"
    Main DocumentRoot: "/var/www/html"
    Main ErrorLog: "/etc/httpd/logs/error_log"
    Mutex cache-socache: using_defaults
    Mutex authdigest-opaque: using_defaults
    Mutex watchdog-callback: using_defaults
    Mutex proxy-balancer-shm: using_defaults
    Mutex rewrite-map: using_defaults
    Mutex ssl-stapling-refresh: using_defaults
    Mutex authdigest-client: using_defaults
    Mutex lua-ivm-shm: using_defaults
    Mutex ssl-stapling: using_defaults
    Mutex proxy: using_defaults
    Mutex authn-socache: using_defaults
    Mutex ssl-cache: using_defaults
    Mutex default: dir="/run/httpd/" mechanism=default 
    PidFile: "/run/httpd/httpd.pid"
    Define: DUMP_VHOSTS
    Define: DUMP_RUN_CFG
    User: name="apache" id=48
    Group: name="apache" id=48

[root@web docker]# vim /etc/hosts

[root@web docker]# cat /etc/hosts

    127.0.0.1	localhost
    ::1	localhost ip6-localhost ip6-loopback
    fe00::0	ip6-localnet
    ff00::0	ip6-mcastprefix
    ff02::1	ip6-allnodes
    ff02::2	ip6-allrouters
    172.18.0.2	web.edt.org web www.auto1.cat www.auto2.cat www.web1.org www.web2.org

[root@web docker]# nmap www.auto1.cat                                                   

    Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-14 08:37 UTC
    Nmap scan report for www.auto1.cat (172.18.0.2)
    Host is up (0.000014s latency).
    rDNS record for 172.18.0.2: web.edt.org
    Not shown: 998 closed ports
    PORT    STATE SERVICE
    80/tcp  open  http
    443/tcp open  https

Nmap done: 1 IP address (1 host up) scanned in 1.65 seconds

[root@web docker]# nmap www.web1.org 

    Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-14 08:37 UTC
    Nmap scan report for www.web1.org (172.18.0.2)
    Host is up (0.000015s latency).
    rDNS record for 172.18.0.2: web.edt.org
    Not shown: 998 closed ports
    PORT    STATE SERVICE
    80/tcp  open  http
    443/tcp open  https

    Nmap done: 1 IP address (1 host up) scanned in 1.65 seconds

[root@web docker]# nmap www.web2.org

    Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-14 08:37 UTC
    Nmap scan report for www.web2.org (172.18.0.2)
    Host is up (0.000014s latency).
    rDNS record for 172.18.0.2: web.edt.org
    Not shown: 998 closed ports
    PORT    STATE SERVICE
    80/tcp  open  http
    443/tcp open  https

    Nmap done: 1 IP address (1 host up) scanned in 1.65 seconds

[root@web docker]# nmap www.auto2.cat

    Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-14 08:37 UTC
    Nmap scan report for www.auto2.cat (172.18.0.2)
    Host is up (0.000015s latency).
    rDNS record for 172.18.0.2: web.edt.org
    Not shown: 998 closed ports
    PORT    STATE SERVICE
    80/tcp  open  http
    443/tcp open  https

    Nmap done: 1 IP address (1 host up) scanned in 1.65 seconds

[root@web docker]# ps ax

        PID TTY      STAT   TIME COMMAND
        1 ?        Ss     0:00 /bin/bash /opt/docker/startup.sh
        33 ?        S      0:00 /usr/sbin/httpd -DFOREGROUND
        34 ?        S      0:00 /usr/sbin/httpd -DFOREGROUND
        35 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
        36 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
        43 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
        249 pts/0    Ss     0:00 /bin/bash
        277 pts/0    R+     0:00 ps ax

[root@web docker]# kill -1 33

[root@web docker]# httpd -S

    VirtualHost configuration:
    172.18.0.2:443         is a NameVirtualHost
            default server www.auto1.cat (/etc/httpd/conf.d/webs.conf:1)
            port 443 namevhost www.auto1.cat (/etc/httpd/conf.d/webs.conf:1)
            port 443 namevhost www.auto2.cat (/etc/httpd/conf.d/webs.conf:13)
            port 443 namevhost www.web1.org (/etc/httpd/conf.d/webs.conf:23)
            port 443 namevhost www.web2.org (/etc/httpd/conf.d/webs.conf:34)
    *:443                  web.edt.org (/etc/httpd/conf.d/ssl.conf:56)
    ServerRoot: "/etc/httpd"
    Main DocumentRoot: "/var/www/html"
    Main ErrorLog: "/etc/httpd/logs/error_log"
    Mutex authdigest-opaque: using_defaults
    Mutex watchdog-callback: using_defaults
    Mutex proxy-balancer-shm: using_defaults
    Mutex rewrite-map: using_defaults
    Mutex ssl-stapling-refresh: using_defaults
    Mutex authdigest-client: using_defaults
    Mutex lua-ivm-shm: using_defaults
    Mutex ssl-stapling: using_defaults
    Mutex proxy: using_defaults
    Mutex authn-socache: using_defaults
    Mutex ssl-cache: using_defaults
    Mutex default: dir="/run/httpd/" mechanism=default 
    Mutex cache-socache: using_defaults
    PidFile: "/run/httpd/httpd.pid"
    Define: DUMP_VHOSTS
    Define: DUMP_RUN_CFG
    User: name="apache" id=48
    Group: name="apache" id=48


--------------

## localhost

----------
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ sudo vim /etc/hosts

    [sudo] password for a191466js: 

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ nmap www.auto1.cat

    Starting Nmap 7.80 ( https://nmap.org ) at 2023-04-14 10:40 CEST
        Nmap scan report for www.auto1.cat (172.18.0.2)
        Host is up (0.00012s latency).
        rDNS record for 172.18.0.2: www.auto2.cat
        Not shown: 998 closed ports
        PORT    STATE SERVICE
        80/tcp  open  http
        443/tcp open  https

        Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ nmap www.auto2.cat

    Starting Nmap 7.80 ( https://nmap.org ) at 2023-04-14 10:40 CEST
    Nmap scan report for www.auto2.cat (172.18.0.2)
    Host is up (0.00012s latency).
    Not shown: 998 closed ports
    PORT    STATE SERVICE
    80/tcp  open  http
    443/tcp open  https

    Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ nmap www.web1.org

    Starting Nmap 7.80 ( https://nmap.org ) at 2023-04-14 10:40 CEST
    Nmap scan report for www.web1.org (172.18.0.2)
    Host is up (0.00011s latency).
    rDNS record for 172.18.0.2: www.auto2.cat
    Not shown: 998 closed ports
    PORT    STATE SERVICE
    80/tcp  open  http
    443/tcp open  https

    Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ nmap www.web2.org

    Starting Nmap 7.80 ( https://nmap.org ) at 2023-04-14 10:40 CEST
    Nmap scan report for www.web2.org (172.18.0.2)
    Host is up (0.00012s latency).
    rDNS record for 172.18.0.2: www.auto2.cat
    Not shown: 998 closed ports
    PORT    STATE SERVICE
    80/tcp  open  http
    443/tcp open  https

    Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ sudo cat /etc/hosts

    127.0.0.1	localhost
    127.0.1.1	g22.informatica.escoladeltreball.org	g22

    # The following lines are desirable for IPv6 capable hosts
    ::1     localhost ip6-localhost ip6-loopback
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters

    10.200.247.246 server profeF2G.informatica.escoladeltreball.org

    10.200.249.170 www.edt.edu inf.edt.edu xtec.edt.edu

    172.18.0.2 www.auto2.cat www.auto1.cat www.web1.org www.web2.org

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ 

==================

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ docker run --rm --name web.edt.org -h web.edt.org --net 2hisx -d edtasixm11/tls18:https
1521cdef49461fa45e40039096dad76f0f857e2891a1c9f140087c7eb24eb1b7
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ docker exec -it web.edt.org /bin/bash
[root@web docker]# vim /etc/hosts
hosts        hosts.allow  hosts.deny   
[root@web docker]# vim /etc/hosts
hosts        hosts.allow  hosts.deny   
[root@web docker]# vim /etc/hosts
[root@web docker]# cat /etc/hosts
127.0.0.1	localhost
::1	localhost ip6-localhost ip6-loopback
fe00::0	ip6-localnet
ff00::0	ip6-mcastprefix
ff02::1	ip6-allnodes
ff02::2	ip6-allrouters
172.18.0.2	web.edt.org web www.auto2.cat www.auto1.cat www.web1.org www.web2.org
[root@web docker]# nmap www.auto2.cat

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-17 08:02 UTC
Nmap scan report for www.auto2.cat (172.18.0.2)
Host is up (0.000014s latency).
rDNS record for 172.18.0.2: web.edt.org
Not shown: 998 closed ports
PORT    STATE SERVICE
80/tcp  open  http
443/tcp open  https

Nmap done: 1 IP address (1 host up) scanned in 1.67 seconds
[root@web docker]# nmap www.auto1.cat

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-17 08:02 UTC
Nmap scan report for www.auto1.cat (172.18.0.2)
Host is up (0.000016s latency).
rDNS record for 172.18.0.2: web.edt.org
Not shown: 998 closed ports
PORT    STATE SERVICE
80/tcp  open  http
443/tcp open  https

Nmap done: 1 IP address (1 host up) scanned in 1.66 seconds
[root@web docker]# nmap www.web1.org 

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-17 08:02 UTC
Nmap scan report for www.web1.org (172.18.0.2)
Host is up (0.000015s latency).
rDNS record for 172.18.0.2: web.edt.org
Not shown: 998 closed ports
PORT    STATE SERVICE
80/tcp  open  http
443/tcp open  https

Nmap done: 1 IP address (1 host up) scanned in 1.65 seconds
[root@web docker]# nmap www.web2.org

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-17 08:02 UTC
Nmap scan report for www.web2.org (172.18.0.2)
Host is up (0.000014s latency).
rDNS record for 172.18.0.2: web.edt.org
Not shown: 998 closed ports
PORT    STATE SERVICE
80/tcp  open  http
443/tcp open  https

Nmap done: 1 IP address (1 host up) scanned in 1.65 seconds
[root@web docker]# ps ax
    PID TTY      STAT   TIME COMMAND
      1 ?        Ss     0:00 /bin/bash /opt/docker/startup.sh
     33 ?        S      0:00 /usr/sbin/httpd -DFOREGROUND
     34 ?        S      0:00 /usr/sbin/httpd -DFOREGROUND
     35 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
     36 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
     42 ?        Sl     0:00 /usr/sbin/httpd -DFOREGROUND
    249 pts/0    Ss     0:00 /bin/bash
    274 pts/0    R+     0:00 ps ax
[root@web docker]# kill -1 33
[root@web docker]# httpd -S
VirtualHost configuration:
172.18.0.2:443         is a NameVirtualHost
         default server www.auto1.cat (/etc/httpd/conf.d/webs.conf:1)
         port 443 namevhost www.auto1.cat (/etc/httpd/conf.d/webs.conf:1)
         port 443 namevhost www.auto2.cat (/etc/httpd/conf.d/webs.conf:13)
         port 443 namevhost www.web1.org (/etc/httpd/conf.d/webs.conf:23)
         port 443 namevhost www.web2.org (/etc/httpd/conf.d/webs.conf:34)
*:443                  web.edt.org (/etc/httpd/conf.d/ssl.conf:56)
ServerRoot: "/etc/httpd"
Main DocumentRoot: "/var/www/html"
Main ErrorLog: "/etc/httpd/logs/error_log"
Mutex authdigest-opaque: using_defaults
Mutex watchdog-callback: using_defaults
Mutex proxy-balancer-shm: using_defaults
Mutex rewrite-map: using_defaults
Mutex ssl-stapling-refresh: using_defaults
Mutex authdigest-client: using_defaults
Mutex lua-ivm-shm: using_defaults
Mutex ssl-stapling: using_defaults
Mutex proxy: using_defaults
Mutex authn-socache: using_defaults
Mutex ssl-cache: using_defaults
Mutex default: dir="/run/httpd/" mechanism=default 
Mutex cache-socache: using_defaults
PidFile: "/run/httpd/httpd.pid"
Define: DUMP_VHOSTS
Define: DUMP_RUN_CFG
User: name="apache" id=48
Group: name="apache" id=48
[root@web docker]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
7: eth0@if8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:12:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.18.0.2/16 brd 172.18.255.255 scope global eth0
       valid_lft forever preferred_lft forever
[root@web docker]# ls
Dockerfile  cacert.pem  httpd.conf        index.auto2.html  index.web2.html       servercert.auto2.pem  serverkey.auto1.pem  serverkey.web2.pem  ssl.conf
README.md   cacert.srl  httpd.old.conf    index.plain.html  install.sh            servercert.web1.pem   serverkey.auto2.pem  serverreq.web1.pem  startup.sh
ca.conf     cakey.pem   index.auto1.html  index.web1.html   servercert.auto1.pem  servercert.web2.pem   serverkey.web1.pem   serverreq.web2.pem  webs.conf
[root@web docker]# pwd       
/opt/docker
[root@web docker]# exit
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ docker st
stack  start  stats  stop   
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ docker stop web.edt.org
web.edt.org
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d edtasixm11/tls18:ldaps
Unable to find image 'edtasixm11/tls18:ldaps' locally
ldaps: Pulling from edtasixm11/tls18
c8ae8b35783e: Already exists 
1e9a7caefe0e: Pull complete 
1193bcb7f2a2: Pull complete 
a2fe81643212: Pull complete 
35e8c5a23909: Pull complete 
Digest: sha256:698984bfb6fa5c140c487b51a882e923051937f8294fd1b05f38bea56ae1bea9
Status: Downloaded newer image for edtasixm11/tls18:ldaps
966e62ef59b55a403652f8a21912a86f54aad1e4de85b5b602760ca00a3e52e9
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ docker exec -it ldap.edt.org /bin/bash
[root@ldap docker]# cat /etc/os-release 
NAME=Fedora
VERSION="27 (Twenty Seven)"
ID=fedora
VERSION_ID=27
PRETTY_NAME="Fedora 27 (Twenty Seven)"
ANSI_COLOR="0;34"
CPE_NAME="cpe:/o:fedoraproject:fedora:27"
HOME_URL="https://fedoraproject.org/"
SUPPORT_URL="https://fedoraproject.org/wiki/Communicating_and_getting_help"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_BUGZILLA_PRODUCT="Fedora"
REDHAT_BUGZILLA_PRODUCT_VERSION=27
REDHAT_SUPPORT_PRODUCT="Fedora"
REDHAT_SUPPORT_PRODUCT_VERSION=27
PRIVACY_POLICY_URL="https://fedoraproject.org/wiki/Legal:PrivacyPolicy"
[root@ldap docker]# yum install nmap
Last metadata expiration check: 0:00:00 ago on Mon Apr 17 08:31:11 2023.
Dependencies resolved.
=========================================================================================================
 Package                 Arch                 Version                        Repository             Size
=========================================================================================================
Installing:
 nmap                    x86_64               2:7.60-8.fc27                  updates               5.7 M
Installing dependencies:
 libstdc++               x86_64               7.3.1-6.fc27                   updates               486 k
 nmap-ncat               x86_64               2:7.60-8.fc27                  updates               232 k

Transaction Summary
=========================================================================================================
Install  3 Packages

Total download size: 6.4 M
Installed size: 25 M
Is this ok [y/N]: y
Downloading Packages:
(1/3): nmap-ncat-7.60-8.fc27.x86_64.rpm                                  375 kB/s | 232 kB     00:00    
(2/3): libstdc++-7.3.1-6.fc27.x86_64.rpm                                 699 kB/s | 486 kB     00:00    
(3/3): nmap-7.60-8.fc27.x86_64.rpm                                       5.9 MB/s | 5.7 MB     00:00    
---------------------------------------------------------------------------------------------------------
Total                                                                    5.6 MB/s | 6.4 MB     00:01     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                 1/1 
  Installing       : libstdc++-7.3.1-6.fc27.x86_64                                                   1/3 
  Running scriptlet: libstdc++-7.3.1-6.fc27.x86_64                                                   1/3 
  Installing       : nmap-ncat-2:7.60-8.fc27.x86_64                                                  2/3 
  Installing       : nmap-2:7.60-8.fc27.x86_64                                                       3/3 
  Verifying        : nmap-2:7.60-8.fc27.x86_64                                                       1/3 
  Verifying        : nmap-ncat-2:7.60-8.fc27.x86_64                                                  2/3 
  Verifying        : libstdc++-7.3.1-6.fc27.x86_64                                                   3/3 

Installed:
  nmap.x86_64 2:7.60-8.fc27      libstdc++.x86_64 7.3.1-6.fc27      nmap-ncat.x86_64 2:7.60-8.fc27     

Complete!
[root@ldap docker]# nmap localhost

Starting Nmap 7.60 ( https://nmap.org ) at 2023-04-17 08:31 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000014s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 998 closed ports
PORT    STATE SERVICE
389/tcp open  ldap
636/tcp open  ldapssl

Nmap done: 1 IP address (1 host up) scanned in 1.66 seconds
[root@ldap docker]# cat startup.sh 
#! /bin/bash
# @edt ASIX M06 2018-2019
# Crear i engegar slapd amb edt.org
# -------------------------------------

/opt/docker/install.sh && echo "Install Ok"
/sbin/slapd -d0 -u ldap -h "ldap:/// ldaps:/// ldapi:///" && echo "slapd Ok"

[root@ldap docker]# ldapsearch -x | grep dn
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
dn: o=africa,ou=clients,dc=edt,dc=org
dn: o=europa,ou=clients,dc=edt,dc=org
dn: o=eu,o=europa,ou=clients,dc=edt,dc=org
dn: o=noneu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=pau,ou=usuaris,dc=edt,dc=org
dn: uid=pere,ou=usuaris,dc=edt,dc=org
dn: uid=anna,ou=usuaris,dc=edt,dc=org
dn: uid=marta,ou=usuaris,dc=edt,dc=org
dn: uid=Jordi,ou=usuaris,dc=edt,dc=org
dn: uid=admin,ou=usuaris,dc=edt,dc=org
dn: uid=user01,ou=usuaris,dc=edt,dc=org
dn: uid=user02,ou=usuaris,dc=edt,dc=org
dn: uid=user03,ou=usuaris,dc=edt,dc=org
dn: uid=user04,ou=usuaris,dc=edt,dc=org
dn: uid=user05,ou=usuaris,dc=edt,dc=org
dn: uid=user06,ou=usuaris,dc=edt,dc=org
dn: uid=user07,ou=usuaris,dc=edt,dc=org
dn: uid=user08,ou=usuaris,dc=edt,dc=org
dn: uid=user09,ou=usuaris,dc=edt,dc=org
dn: uid=user10,ou=usuaris,dc=edt,dc=org
dn: uid=mao,o=asia,ou=clients,dc=edt,dc=org
dn: uid=ho,o=asia,ou=clients,dc=edt,dc=org
dn: uid=hirohito,o=asia,ou=clients,dc=edt,dc=org
dn: uid=nelson,o=africa,ou=clients,dc=edt,dc=org
dn: uid=robert,o=africa,ou=clients,dc=edt,dc=org
dn: uid=ali,o=africa,ou=clients,dc=edt,dc=org
dn: uid=konrad,o=eu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=humphrey,o=eu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=carles,o=eu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=francisco,o=noneu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=vladimir,o=noneu,o=europa,ou=clients,dc=edt,dc=org
userPassword:: e1NIQX0zSkdHb0dCNGN6a1Zwdnk2czA1WkVndml0SVE9
dn: uid=jorge,o=noneu,o=europa,ou=clients,dc=edt,dc=org
dn: ou=grups,dc=edt,dc=org
dn: cn=cup,ou=grups,dc=edt,dc=org
dn: cn=admin,ou=grups,dc=edt,dc=org
dn: cn=alumnes,ou=grups,dc=edt,dc=org
dn: cn=profes,ou=grups,dc=edt,dc=org
dn: cn=1wiaw,ou=grups,dc=edt,dc=org
dn: cn=2wiaw,ou=grups,dc=edt,dc=org
dn: cn=1asix,ou=grups,dc=edt,dc=org
dn: cn=2asix,ou=grups,dc=edt,dc=org
[root@ldap docker]# ldapsearch -x | grep dn
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
dn: o=africa,ou=clients,dc=edt,dc=org
dn: o=europa,ou=clients,dc=edt,dc=org
dn: o=eu,o=europa,ou=clients,dc=edt,dc=org
dn: o=noneu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=pau,ou=usuaris,dc=edt,dc=org
dn: uid=pere,ou=usuaris,dc=edt,dc=org
dn: uid=anna,ou=usuaris,dc=edt,dc=org
dn: uid=marta,ou=usuaris,dc=edt,dc=org
dn: uid=Jordi,ou=usuaris,dc=edt,dc=org
dn: uid=admin,ou=usuaris,dc=edt,dc=org
dn: uid=user01,ou=usuaris,dc=edt,dc=org
dn: uid=user02,ou=usuaris,dc=edt,dc=org
dn: uid=user03,ou=usuaris,dc=edt,dc=org
dn: uid=user04,ou=usuaris,dc=edt,dc=org
dn: uid=user05,ou=usuaris,dc=edt,dc=org
dn: uid=user06,ou=usuaris,dc=edt,dc=org
dn: uid=user07,ou=usuaris,dc=edt,dc=org
dn: uid=user08,ou=usuaris,dc=edt,dc=org
dn: uid=user09,ou=usuaris,dc=edt,dc=org
dn: uid=user10,ou=usuaris,dc=edt,dc=org
dn: uid=mao,o=asia,ou=clients,dc=edt,dc=org
dn: uid=ho,o=asia,ou=clients,dc=edt,dc=org
dn: uid=hirohito,o=asia,ou=clients,dc=edt,dc=org
dn: uid=nelson,o=africa,ou=clients,dc=edt,dc=org
dn: uid=robert,o=africa,ou=clients,dc=edt,dc=org
dn: uid=ali,o=africa,ou=clients,dc=edt,dc=org
dn: uid=konrad,o=eu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=humphrey,o=eu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=carles,o=eu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=francisco,o=noneu,o=europa,ou=clients,dc=edt,dc=org
dn: uid=vladimir,o=noneu,o=europa,ou=clients,dc=edt,dc=org
userPassword:: e1NIQX0zSkdHb0dCNGN6a1Zwdnk2czA1WkVndml0SVE9
dn: uid=jorge,o=noneu,o=europa,ou=clients,dc=edt,dc=org
dn: ou=grups,dc=edt,dc=org
dn: cn=cup,ou=grups,dc=edt,dc=org
dn: cn=admin,ou=grups,dc=edt,dc=org
dn: cn=alumnes,ou=grups,dc=edt,dc=org
dn: cn=profes,ou=grups,dc=edt,dc=org
dn: cn=1wiaw,ou=grups,dc=edt,dc=org
dn: cn=2wiaw,ou=grups,dc=edt,dc=org
dn: cn=1asix,ou=grups,dc=edt,dc=org
dn: cn=2asix,ou=grups,dc=edt,dc=org
[root@ldap docker]# cat ldap.conf 
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

BASE	dc=edt,dc=org
URI	ldap://localhost

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

#TLS_CACERTDIR /etc/openldap/certs
TLS_CACERT /opt/docker/cacert.pem

# Turning this off breaks GSSAPI used with krb5 when rdns = false
SASL_NOCANON	on

[root@ldap docker]# cat slapd.conf 
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#

#include	/etc/openldap/schema/corba.schema
include		/etc/openldap/schema/core.schema
include		/etc/openldap/schema/cosine.schema
#include	/etc/openldap/schema/duaconf.schema
#include	/etc/openldap/schema/dyngroup.schema
include		/etc/openldap/schema/inetorgperson.schema
#include	/etc/openldap/schema/java.schema
#include	/etc/openldap/schema/misc.schema
include		/etc/openldap/schema/nis.schema
include		/etc/openldap/schema/openldap.schema
#include	/etc/openldap/schema/ppolicy.schema
include		/etc/openldap/schema/collective.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2
pidfile		/var/run/openldap/slapd.pid
TLSCACertificateFile        /etc/openldap/certs/cacert.pem
TLSCertificateFile          /etc/openldap/certs/servercert.ldap.pem
TLSCertificateKeyFile       /etc/openldap/certs/serverkey.ldap.pem
TLSVerifyClient       never
TLSCipherSuite        HIGH:MEDIUM:LOW:+SSLv2
#argsfile	/var/run/openldap/slapd.args

#-------------------------------------------------
database config
rootdn "cn=Sysadmin,cn=config"
rootpw {SSHA}JGzCfrm+TvKfHtbpjPdz3YCVYpqUbTVY
#passwd syskey
# -------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to * by * none

[root@ldap docker]# ldapsearch -x -H ldap://ldap.edt.org | dn | head
bash: dn: command not found
[root@ldap docker]# ldapsearch -x -H ldap://ldap.edt.org | grep dn | head
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
[root@ldap docker]# ldapsearch -x -H ldaps://ldap.edt.org | grep dn | head
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
[root@ldap docker]# #ldapsearch -x - Z -H ldaps://ldap.edt.org | grep dn | head --> StartTLS
[root@ldap docker]# #ldapsearch -x - Z -H ldaps://ldap.edt.org | grep dn | head --> -Z StartTLS
[root@ldap docker]# #ldapsearch -x -Z -H ldap://ldap.edt.org | grep dn | head                
[root@ldap docker]# ldapsearch -x -Z -H ldap://ldap.edt.org | grep dn | head
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
[root@ldap docker]# #ldapsearch -x -ZZ -H ldap://ldap.edt.org | grep dn | head --> -ZZ garantizamos que si se puede hacer se hara y si no, no la hara
[root@ldap docker]# ldapsearch -x -ZZ -H ldap://ldap.edt.org | grep dn | head                          
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
[root@ldap docker]# 
[root@ldap docker]# ldapsearch -x -ZZ -H ldap://172.18.0.2 | grep dn | head
ldap_start_tls: Connect error (-11)
	additional info: TLS: hostname does not match CN in peer certificate
[root@ldap docker]# ldapsearch -x -H ldap://172.18.0.2 | grep dn | head
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
[root@ldap docker]# ldapsearch -x -H ldap://localhost | grep dn | head
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
[root@ldap docker]# ldapsearch -x -H ldap://172.18.0.1 | grep dn | head
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
[root@ldap docker]# vim /etc/hosts
bash: vim: command not found
[root@ldap docker]# yum install vim iputils
Last metadata expiration check: 1:02:53 ago on Mon Apr 17 08:31:11 2023.
Dependencies resolved.
=========================================================================================================
 Package                    Arch               Version                         Repository           Size
=========================================================================================================
Installing:
 iputils                    x86_64             20161105-7.fc27                 fedora              157 k
 vim-enhanced               x86_64             2:8.1.513-2.fc27                updates             1.4 M
Installing dependencies:
 gpm-libs                   x86_64             1.20.7-10.fc26                  fedora               36 k
 vim-common                 x86_64             2:8.1.513-2.fc27                updates             6.5 M
 vim-filesystem             noarch             2:8.1.513-2.fc27                updates              52 k
 which                      x86_64             2.21-4.fc27                     fedora               46 k

Transaction Summary
=========================================================================================================
Install  6 Packages

Total download size: 8.2 M
Installed size: 31 M
Is this ok [y/N]: y
Downloading Packages:
(1/6): iputils-20161105-7.fc27.x86_64.rpm                                292 kB/s | 157 kB     00:00    
(2/6): gpm-libs-1.20.7-10.fc26.x86_64.rpm                                472 kB/s |  36 kB     00:00    
(3/6): which-2.21-4.fc27.x86_64.rpm                                      622 kB/s |  46 kB     00:00    
(4/6): vim-enhanced-8.1.513-2.fc27.x86_64.rpm                            1.8 MB/s | 1.4 MB     00:00    
(5/6): vim-filesystem-8.1.513-2.fc27.noarch.rpm                          565 kB/s |  52 kB     00:00    
(6/6): vim-common-8.1.513-2.fc27.x86_64.rpm                              6.4 MB/s | 6.5 MB     00:01    
---------------------------------------------------------------------------------------------------------
Total                                                                    4.6 MB/s | 8.2 MB     00:01     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                 1/1 
  Installing       : vim-filesystem-2:8.1.513-2.fc27.noarch                                          1/6 
  Installing       : vim-common-2:8.1.513-2.fc27.x86_64                                              2/6 
  Installing       : which-2.21-4.fc27.x86_64                                                        3/6 
  Running scriptlet: which-2.21-4.fc27.x86_64                                                        3/6 
install-info: No such file or directory for /usr/share/info/which.info.gz
  Installing       : gpm-libs-1.20.7-10.fc26.x86_64                                                  4/6 
  Running scriptlet: gpm-libs-1.20.7-10.fc26.x86_64                                                  4/6 
  Installing       : vim-enhanced-2:8.1.513-2.fc27.x86_64                                            5/6 
  Installing       : iputils-20161105-7.fc27.x86_64                                                  6/6 
  Running scriptlet: iputils-20161105-7.fc27.x86_64                                                  6/6 
Failed to connect to bus: No such file or directory
  Running scriptlet: vim-common-2:8.1.513-2.fc27.x86_64                                              6/6 
  Verifying        : iputils-20161105-7.fc27.x86_64                                                  1/6 
  Verifying        : vim-enhanced-2:8.1.513-2.fc27.x86_64                                            2/6 
  Verifying        : vim-common-2:8.1.513-2.fc27.x86_64                                              3/6 
  Verifying        : gpm-libs-1.20.7-10.fc26.x86_64                                                  4/6 
  Verifying        : which-2.21-4.fc27.x86_64                                                        5/6 
  Verifying        : vim-filesystem-2:8.1.513-2.fc27.noarch                                          6/6 

Installed:
  iputils.x86_64 20161105-7.fc27                       vim-enhanced.x86_64 2:8.1.513-2.fc27              
  gpm-libs.x86_64 1.20.7-10.fc26                       vim-common.x86_64 2:8.1.513-2.fc27                
  vim-filesystem.noarch 2:8.1.513-2.fc27               which.x86_64 2.21-4.fc27                          

Complete!
[root@ldap docker]# ip a 
bash: ip: command not found
[root@ldap docker]# yum install iproute
Last metadata expiration check: 1:03:39 ago on Mon Apr 17 08:31:11 2023.
Dependencies resolved.
=========================================================================================================
 Package                     Arch                Version                      Repository            Size
=========================================================================================================
Installing:
 iproute                     x86_64              4.15.0-1.fc27                updates              534 k
Installing dependencies:
 libmnl                      x86_64              1.0.4-4.fc27                 fedora                28 k
 linux-atm-libs              x86_64              2.5.1-19.fc27                fedora                40 k
Installing weak dependencies:
 iproute-tc                  x86_64              4.15.0-1.fc27                updates              389 k

Transaction Summary
=========================================================================================================
Install  4 Packages

Total download size: 992 k
Installed size: 2.1 M
Is this ok [y/N]: y
Downloading Packages:
(1/4): libmnl-1.0.4-4.fc27.x86_64.rpm                                     86 kB/s |  28 kB     00:00    
(2/4): linux-atm-libs-2.5.1-19.fc27.x86_64.rpm                           318 kB/s |  40 kB     00:00    
(3/4): iproute-4.15.0-1.fc27.x86_64.rpm                                  1.0 MB/s | 534 kB     00:00    
(4/4): iproute-tc-4.15.0-1.fc27.x86_64.rpm                               688 kB/s | 389 kB     00:00    
---------------------------------------------------------------------------------------------------------
Total                                                                    650 kB/s | 992 kB     00:01     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                 1/1 
  Installing       : libmnl-1.0.4-4.fc27.x86_64                                                      1/4 
  Running scriptlet: libmnl-1.0.4-4.fc27.x86_64                                                      1/4 
  Installing       : iproute-4.15.0-1.fc27.x86_64                                                    2/4 
  Installing       : linux-atm-libs-2.5.1-19.fc27.x86_64                                             3/4 
  Running scriptlet: linux-atm-libs-2.5.1-19.fc27.x86_64                                             3/4 
  Installing       : iproute-tc-4.15.0-1.fc27.x86_64                                                 4/4 
  Verifying        : iproute-4.15.0-1.fc27.x86_64                                                    1/4 
  Verifying        : libmnl-1.0.4-4.fc27.x86_64                                                      2/4 
  Verifying        : iproute-tc-4.15.0-1.fc27.x86_64                                                 3/4 
  Verifying        : linux-atm-libs-2.5.1-19.fc27.x86_64                                             4/4 

Installed:
  iproute.x86_64 4.15.0-1.fc27           iproute-tc.x86_64 4.15.0-1.fc27    libmnl.x86_64 1.0.4-4.fc27   
  linux-atm-libs.x86_64 2.5.1-19.fc27   

Complete!
[root@ldap docker]# ldapsearch -x -H ldap://172.18.0.2 | grep dn | head
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
[root@ldap docker]# vim /etc/hosts
[root@ldap docker]# ldapsearch -x -H ldap://patata.edt.org | grep dn | head
dn: dc=edt,dc=org
dn: ou=maquines,dc=edt,dc=org
dn: ou=clients,dc=edt,dc=org
dn: ou=productes,dc=edt,dc=org
dn: ou=usuaris,dc=edt,dc=org
dn: ou=hosts,dc=edt,dc=org
dn: ou=keys,dc=edt,dc=org
dn: ou=certs,dc=edt,dc=org
dn: ou=domains,dc=edt,dc=org
dn: o=asia,ou=clients,dc=edt,dc=org
[root@ldap docker]# ldapsearch -x -H ldap://patata | grep dn | head
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
[root@ldap docker]# ldapsearch -x -H ldaps://patata.edt.org | grep dn | head
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
[root@ldap docker]# ldapsearch -x -d1 -H ldaps://patata.edt.org | grep dn | head
ldap_url_parse_ext(ldaps://patata.edt.org)
ldap_create
ldap_url_parse_ext(ldaps://patata.edt.org:636/??base)
ldap_sasl_bind
ldap_send_initial_request
ldap_new_connection 1 1 0
ldap_int_open_connection
ldap_connect_to_host: TCP patata.edt.org:636
ldap_new_socket: 3
ldap_prepare_socket: 3
ldap_connect_to_host: Trying 172.18.0.2:636
ldap_pvt_connect: fd: 3 tm: -1 async: 0
attempting to connect: 
connect success
TLS: loaded CA certificate file /opt/docker/cacert.pem.
TLS: certificate [E=ldap@edt.org,CN=ldap.edt.org,OU=informatica,O=edt,L=barcelona,ST=barcelona,C=ca] is valid
TLS certificate verification: subject: E=ldap@edt.org,CN=ldap.edt.org,OU=informatica,O=edt,L=barcelona,ST=barcelona,C=ca, issuer: E=veritat@edt.org,CN=VeritatAbsoluta,OU=informatica,O=edt,L=barcelona,ST=barcelona,C=ca, cipher: AES-128-GCM, security level: high, secret key bits: 128, total key bits: 128, cache hits: 0, cache misses: 0, cache not reusable: 0
TLS: hostname (patata.edt.org) does not match common name in certificate (ldap.edt.org).
TLS: can't connect: TLS: hostname does not match CN in peer certificate.
ldap_err2string
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
[root@ldap docker]# yum install openssl
Last metadata expiration check: 1:09:41 ago on Mon Apr 17 08:31:11 2023.
Dependencies resolved.
=========================================================================================================
 Package                    Arch                Version                       Repository            Size
=========================================================================================================
Installing:
 openssl                    x86_64              1:1.1.0i-1.fc27               updates              575 k
Upgrading:
 openssl-libs               x86_64              1:1.1.0i-1.fc27               updates              1.3 M
Installing dependencies:
 gc                         x86_64              7.6.0-7.fc27                  fedora               110 k
 guile                      x86_64              5:2.0.14-3.fc27               fedora               3.5 M
 libatomic_ops              x86_64              7.4.6-3.fc27                  fedora                33 k
 make                       x86_64              1:4.2.1-4.fc27                fedora               494 k

Transaction Summary
=========================================================================================================
Install  5 Packages
Upgrade  1 Package

Total download size: 6.0 M
Is this ok [y/N]: y
Downloading Packages:
(1/6): openssl-1.1.0i-1.fc27.x86_64.rpm                                  1.1 MB/s | 575 kB     00:00    
(2/6): gc-7.6.0-7.fc27.x86_64.rpm                                        194 kB/s | 110 kB     00:00    
(3/6): libatomic_ops-7.4.6-3.fc27.x86_64.rpm                             453 kB/s |  33 kB     00:00    
(4/6): make-4.2.1-4.fc27.x86_64.rpm                                      749 kB/s | 494 kB     00:00    
(5/6): openssl-libs-1.1.0i-1.fc27.x86_64.rpm                             7.5 MB/s | 1.3 MB     00:00    
(6/6): guile-2.0.14-3.fc27.x86_64.rpm                                    4.1 MB/s | 3.5 MB     00:00    
---------------------------------------------------------------------------------------------------------
Total                                                                    2.1 MB/s | 6.0 MB     00:02     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                 1/1 
  Upgrading        : openssl-libs-1:1.1.0i-1.fc27.x86_64                                             1/7 
  Running scriptlet: openssl-libs-1:1.1.0i-1.fc27.x86_64                                             1/7 
  Installing       : libatomic_ops-7.4.6-3.fc27.x86_64                                               2/7 
  Running scriptlet: libatomic_ops-7.4.6-3.fc27.x86_64                                               2/7 
  Installing       : gc-7.6.0-7.fc27.x86_64                                                          3/7 
  Running scriptlet: gc-7.6.0-7.fc27.x86_64                                                          3/7 
  Installing       : guile-5:2.0.14-3.fc27.x86_64                                                    4/7 
  Running scriptlet: guile-5:2.0.14-3.fc27.x86_64                                                    4/7 
  Installing       : make-1:4.2.1-4.fc27.x86_64                                                      5/7 
  Running scriptlet: make-1:4.2.1-4.fc27.x86_64                                                      5/7 
  Installing       : openssl-1:1.1.0i-1.fc27.x86_64                                                  6/7 
  Cleanup          : openssl-libs-1:1.1.0h-3.fc27.x86_64                                             7/7 
  Running scriptlet: openssl-libs-1:1.1.0h-3.fc27.x86_64                                             7/7 
  Running scriptlet: guile-5:2.0.14-3.fc27.x86_64                                                    7/7 
  Verifying        : openssl-1:1.1.0i-1.fc27.x86_64                                                  1/7 
  Verifying        : make-1:4.2.1-4.fc27.x86_64                                                      2/7 
  Verifying        : gc-7.6.0-7.fc27.x86_64                                                          3/7 
  Verifying        : guile-5:2.0.14-3.fc27.x86_64                                                    4/7 
  Verifying        : libatomic_ops-7.4.6-3.fc27.x86_64                                               5/7 
  Verifying        : openssl-libs-1:1.1.0i-1.fc27.x86_64                                             6/7 
  Verifying        : openssl-libs-1:1.1.0h-3.fc27.x86_64                                             7/7 

Installed:
  openssl.x86_64 1:1.1.0i-1.fc27        gc.x86_64 7.6.0-7.fc27         guile.x86_64 5:2.0.14-3.fc27    
  libatomic_ops.x86_64 7.4.6-3.fc27     make.x86_64 1:4.2.1-4.fc27    

Upgraded:
  openssl-libs.x86_64 1:1.1.0i-1.fc27                                                                    

Complete!
[root@ldap docker]# openssl x509 -noout -text -in servercert.ldap.pem 
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            c1:5d:a3:8e:7b:1f:62:d4
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = barcelona, L = barcelona, O = edt, OU = informatica, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
        Validity
            Not Before: Mar 28 17:41:40 2019 GMT
            Not After : Mar 25 17:41:40 2029 GMT
        Subject: C = ca, ST = barcelona, L = barcelona, O = edt, OU = informatica, CN = ldap.edt.org, emailAddress = ldap@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:92:de:74:b9:5c:0f:c8:5d:ff:d1:a3:66:ef:49:
                    42:e4:4f:37:d7:5c:c0:72:e2:b8:90:a2:38:85:df:
                    47:24:cd:a9:8e:24:76:3b:60:98:9d:6e:b5:73:f0:
                    6a:ae:2b:90:02:fb:4d:36:0b:bb:b7:cd:ca:64:d6:
                    c5:3b:f4:af:77:7f:9f:0c:97:8f:ed:00:c6:32:9c:
                    6c:90:1d:c7:6b:fd:87:78:15:ea:4e:dd:28:f0:b3:
                    53:f0:c8:c2:c3:44:02:69:91:37:fa:4d:af:da:67:
                    42:0a:af:4a:a1:c2:74:33:e1:9b:b1:3f:d1:0c:0b:
                    3b:35:9c:98:14:fd:d1:f2:76:e8:1d:86:b8:a2:da:
                    ca:69:11:04:72:7d:74:4f:b3:45:d6:44:0e:5c:5d:
                    25:e6:51:27:5d:b4:9b:7f:8e:4c:c2:13:5f:11:53:
                    7d:2f:44:ed:64:e1:e7:28:0a:b9:48:62:3d:42:28:
                    0e:58:3b:5d:a9:8e:61:69:2f:2b:c4:0d:19:4c:33:
                    bd:aa:38:e4:0b:bf:60:58:04:eb:d0:51:a4:8a:e9:
                    c1:eb:0a:4a:93:f6:aa:15:ee:af:ed:68:10:78:94:
                    19:ff:4b:2c:6e:5b:96:74:41:78:6e:ca:68:4c:7b:
                    fb:1d:10:6f:4b:8b:d0:0c:10:d4:1e:2d:43:a1:e6:
                    30:5d
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            X509v3 Extended Key Usage: 
                TLS Web Server Authentication
            X509v3 Subject Alternative Name: 
                IP Address:172.17.0.2, IP Address:127.0.0.1, email:ldap@edt.org, URI:ldaps://mysecureldapserver.org
    Signature Algorithm: sha256WithRSAEncryption
         15:bd:e6:be:6c:97:74:fb:9d:9a:1b:4f:68:be:af:36:bd:0d:
         46:62:13:a2:51:6b:1f:91:20:e2:fc:98:61:43:4d:72:d1:fc:
         59:cb:b2:ef:ad:4b:31:cf:d2:73:56:3b:de:e6:ba:8f:0e:92:
         a0:aa:17:d6:02:f6:6d:75:42:79:07:e0:04:8e:5e:e2:bb:02:
         9e:bc:12:b9:77:ed:1b:05:56:94:ae:50:0f:df:ab:19:c6:36:
         c9:b5:28:50:e2:bc:5e:ef:91:cd:db:55:36:1a:b8:e3:b3:8c:
         10:28:0a:62:a1:ad:3a:6d:4c:e2:5c:a6:e6:83:1f:b5:e0:76:
         89:37:a2:6f:8a:f7:ea:71:ed:b0:a2:f0:b6:d5:bc:bd:18:e9:
         0f:6e:d5:c7:a2:e8:46:c6:50:34:a6:1d:91:d0:20:38:7f:99:
         99:48:0c:fa:41:36:ef:a8:da:96:c0:0b:bb:e0:6a:27:e5:8e:
         8b:ad:2e:02:dc:ad:c4:f8:89:99:94:23:c8:19:25:3b:72:d9:
         32:06:5c:57:43:11:4e:17:0f:17:8f:74:f0:8a:0c:fb:09:1c:
         ef:45:be:a7:25:27:9b:d0:2e:3c:0c:11:ea:5e:32:7d:51:c7:
         3d:5d:f3:e9:2a:7c:ae:e0:0c:7c:3c:c3:e1:26:91:22:bb:93:
         d6:c2:55:78
[root@ldap docker]# vim /etc/hosts
[root@ldap docker]# ldapsearch -x -d1 -H ldaps://mysecureldapserver.org | grep dn | head
ldap_url_parse_ext(ldaps://mysecureldapserver.org)
ldap_create
ldap_url_parse_ext(ldaps://mysecureldapserver.org:636/??base)
ldap_sasl_bind
ldap_send_initial_request
ldap_new_connection 1 1 0
ldap_int_open_connection
ldap_connect_to_host: TCP mysecureldapserver.org:636
ldap_new_socket: 3
ldap_prepare_socket: 3
ldap_connect_to_host: Trying 172.18.0.2:636
ldap_pvt_connect: fd: 3 tm: -1 async: 0
attempting to connect: 
connect success
TLS: loaded CA certificate file /opt/docker/cacert.pem.
TLS: certificate [E=ldap@edt.org,CN=ldap.edt.org,OU=informatica,O=edt,L=barcelona,ST=barcelona,C=ca] is valid
TLS certificate verification: subject: E=ldap@edt.org,CN=ldap.edt.org,OU=informatica,O=edt,L=barcelona,ST=barcelona,C=ca, issuer: E=veritat@edt.org,CN=VeritatAbsoluta,OU=informatica,O=edt,L=barcelona,ST=barcelona,C=ca, cipher: AES-128-GCM, security level: high, secret key bits: 128, total key bits: 128, cache hits: 0, cache misses: 0, cache not reusable: 0
TLS: hostname (mysecureldapserver.org) does not match common name in certificate (ldap.edt.org).
TLS: can't connect: TLS: hostname does not match CN in peer certificate.
ldap_err2string
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
[root@ldap docker]# ldapsearch -x -H ldaps://mysecureldapserver.org | grep dn | head
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
[root@ldap docker]# vim /etc/hosts
[root@ldap docker]# ldapsearch -x -H ldaps://mysecureldapserver.org | grep dn | head
ldap_sasl_bind(SIMPLE): Can't contact LDAP server (-1)
[root@ldap docker]# 
[root@ldap docker]# cat ext.alternate.conf 
basicConstraints=CA:FALSE
extendedKeyUsage=serverAuth
subjectAltName=IP:172.17.0.2,IP:127.0.0.1,email:copy,URI:ldaps://mysecureldapserver.org

[root@ldap docker]# 
[root@ldap docker]# # DNS: DNS:ldap.example.com, DNS:*.inf.example.com,
[root@ldap docker]# 

# Certificado autosignat (repaso)



# Extensions

a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ cat ca.conf 
basicConstraints = critical,CA:FALSE
extendedKeyUsage = serverAuth,emailProtection
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -new -x509 -nodes -out provacert.pem -keyout provakey.pem
Generating a RSA private key
.....................................................................................................................................+++++
.....................+++++
writing new private key to 'provakey.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:ca
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:inf
Common Name (e.g. server FQDN or YOUR name) []:web.edt.org
Email Address []:web@edt.org
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -text -in provacert.pem 
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            70:5c:d2:10:f1:a1:84:95:e0:72:7c:8d:4a:6e:39:5d:25:73:a1:c3
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = web.edt.org, emailAddress = web@edt.org
        Validity
            Not Before: Apr 19 07:38:17 2023 GMT
            Not After : May 19 07:38:17 2023 GMT
        Subject: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = web.edt.org, emailAddress = web@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:c5:59:48:fe:6d:02:74:02:b6:1a:8e:6b:2c:69:
                    97:2a:6e:96:47:e7:d4:8d:76:e8:b3:8e:79:21:87:
                    ce:dc:80:42:93:de:12:6a:13:4e:65:42:00:c1:1a:
                    06:f8:31:c2:5c:54:99:38:2a:2a:a0:07:62:9f:89:
                    06:d7:8d:16:0a:6f:2e:0f:3b:29:44:d4:3b:28:14:
                    9b:48:b2:5d:c2:0a:db:82:bf:5c:3e:4e:f8:f1:1d:
                    da:3c:ff:06:fe:67:8e:2e:3f:04:33:b4:db:a9:20:
                    88:33:13:67:60:1b:9f:a2:97:88:ce:a9:69:a4:e7:
                    11:25:91:24:3a:c5:bb:7a:d2:49:d9:29:2b:2e:b6:
                    0e:d8:32:48:04:32:92:bc:69:1e:6f:2d:63:e3:55:
                    13:96:b5:2c:fa:a4:85:df:ec:45:38:8e:68:ea:30:
                    ba:d2:f0:40:fa:ab:8d:7d:7e:28:78:ef:1a:68:4f:
                    39:2f:3b:91:0e:52:48:39:b9:b7:5c:24:34:f2:84:
                    42:63:34:23:20:59:18:0d:e9:14:bd:f5:34:7c:03:
                    82:e3:b2:f4:a3:7e:4a:55:eb:e6:00:c0:b2:d6:41:
                    a0:1c:ff:eb:71:3c:c2:c7:56:7b:87:71:9a:5f:36:
                    00:69:e1:8d:51:d4:be:02:c8:81:ef:8a:3f:b5:70:
                    8d:d1
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                2F:63:56:7D:32:10:B7:06:F3:2F:9D:CF:FB:91:09:93:9E:86:86:CB
            X509v3 Authority Key Identifier: 
                keyid:2F:63:56:7D:32:10:B7:06:F3:2F:9D:CF:FB:91:09:93:9E:86:86:CB

            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         b4:71:25:93:69:26:b0:e9:66:b3:68:8a:5b:53:80:9d:28:fc:
         0d:b4:ce:a0:e8:53:5b:4d:3f:44:d8:63:35:f3:1c:49:2f:7b:
         55:47:e2:9c:4a:78:41:b0:20:f5:05:4f:65:69:ec:c6:e9:d7:
         46:f9:85:42:ca:84:ed:6e:b1:e4:b3:20:ff:af:4a:ae:2f:43:
         30:ad:fc:a7:f1:da:da:87:eb:d4:12:4c:a6:3d:fe:47:a0:73:
         bf:da:d2:bf:7f:61:a1:b6:c4:ed:d5:75:85:cc:06:9e:80:90:
         a8:91:a9:91:83:af:56:24:75:af:8f:1d:7b:1b:6c:50:da:16:
         52:0b:f0:52:0d:0a:9d:8d:eb:6a:c8:aa:45:aa:bf:eb:98:d4:
         51:4e:0f:05:11:2a:c7:c5:87:27:48:77:7d:ac:42:d0:c3:62:
         5a:15:88:93:70:6d:6b:6a:19:dc:05:7a:e4:44:03:05:84:03:
         fb:c0:c4:9d:c3:2d:2a:94:42:82:3d:b4:25:28:6e:84:88:2c:
         44:bd:54:66:02:65:c9:a6:ce:cf:9c:68:4c:e7:ef:11:43:64:
         98:2c:0c:42:dc:51:9d:cf:be:24:9e:33:49:2a:f5:91:51:55:
         84:05:66:ba:00:13:4b:37:bd:90:f5:f8:83:5b:50:b3:29:45:
         d0:56:8e:b6
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ locate openssl.conf
/var/lib/dpkg/info/openssl.conffiles
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ tree /etc/pki/
/etc/pki/
├── fwupd
│   ├── GPG-KEY-Hughski-Limited
│   ├── GPG-KEY-Linux-Foundation-Firmware
│   ├── GPG-KEY-Linux-Vendor-Firmware-Service
│   └── LVFS-CA.pem
└── fwupd-metadata
    ├── GPG-KEY-Linux-Foundation-Metadata
    ├── GPG-KEY-Linux-Vendor-Firmware-Service
    └── LVFS-CA.pem

2 directories, 7 files
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ locate openssl.cnf
/etc/ssl/openssl.cnf
/usr/lib/ssl/openssl.cnf
/usr/share/doc/openvpn/examples/sample-keys/openssl.cnf
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ cp /etc/ssl/openssl.cnf .
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ tree /etc/ssl
/etc/ssl
├── certs
│   ├── 02265526.0 -> Entrust_Root_Certification_Authority_-_G2.pem
│   ├── 03179a64.0 -> Staat_der_Nederlanden_EV_Root_CA.pem
│   ├── 062cdee6.0 -> GlobalSign_Root_CA_-_R3.pem
│   ├── 064e0aa9.0 -> QuoVadis_Root_CA_2_G3.pem
│   ├── 06dc52d5.0 -> SSL.com_EV_Root_Certification_Authority_RSA_R2.pem
│   ├── 080911ac.0 -> QuoVadis_Root_CA.pem
│   ├── 09789157.0 -> Starfield_Services_Root_Certificate_Authority_-_G2.pem
│   ├── 0a775a30.0 -> GTS_Root_R3.pem
│   ├── 0b1b94ef.0 -> CFCA_EV_ROOT.pem
│   ├── 0bf05006.0 -> SSL.com_Root_Certification_Authority_ECC.pem
│   ├── 0c4c9b6c.0 -> Global_Chambersign_Root_-_2008.pem
│   ├── 0f5dc4f3.0 -> UCA_Extended_Validation_Root.pem
│   ├── 0f6fa695.0 -> GDCA_TrustAUTH_R5_ROOT.pem
│   ├── 1001acf7.0 -> GTS_Root_R1.pem
│   ├── 106f3e4d.0 -> Entrust_Root_Certification_Authority_-_EC1.pem
│   ├── 116bf586.0 -> GeoTrust_Primary_Certification_Authority_-_G2.pem
│   ├── 14bc7599.0 -> emSign_ECC_Root_CA_-_G3.pem
│   ├── 1636090b.0 -> Hellenic_Academic_and_Research_Institutions_RootCA_2011.pem
│   ├── 18856ac4.0 -> SecureSign_RootCA11.pem
│   ├── 1d3472b9.0 -> GlobalSign_ECC_Root_CA_-_R5.pem
│   ├── 1e08bfd1.0 -> IdenTrust_Public_Sector_Root_CA_1.pem
│   ├── 1e09d511.0 -> T-TeleSec_GlobalRoot_Class_2.pem
│   ├── 244b5494.0 -> DigiCert_High_Assurance_EV_Root_CA.pem
│   ├── 2923b3f9.0 -> emSign_Root_CA_-_G1.pem
│   ├── 2ae6433e.0 -> CA_Disig_Root_R2.pem
│   ├── 2b349938.0 -> AffirmTrust_Commercial.pem
│   ├── 2e5ac55d.0 -> DST_Root_CA_X3.pem
│   ├── 32888f65.0 -> Hellenic_Academic_and_Research_Institutions_RootCA_2015.pem
│   ├── 349f2832.0 -> EC-ACC.pem
│   ├── 3513523f.0 -> DigiCert_Global_Root_CA.pem
│   ├── 3bde41ac.0 -> Autoridad_de_Certificacion_Firmaprofesional_CIF_A62634068.pem
│   ├── 3e44d2f7.0 -> TrustCor_RootCert_CA-2.pem
│   ├── 3e45d192.0 -> Hongkong_Post_Root_CA_1.pem
│   ├── 3fb36b73.0 -> NAVER_Global_Root_Certification_Authority.pem
│   ├── 40193066.0 -> Certum_Trusted_Network_CA_2.pem
│   ├── 4042bcee.0 -> ISRG_Root_X1.pem
│   ├── 40547a79.0 -> COMODO_Certification_Authority.pem
│   ├── 406c9bb1.0 -> emSign_Root_CA_-_C1.pem
│   ├── 4304c5e5.0 -> Network_Solutions_Certificate_Authority.pem
│   ├── 48bec511.0 -> Certum_Trusted_Network_CA.pem
│   ├── 4a6481c9.0 -> GlobalSign_Root_CA_-_R2.pem
│   ├── 4b718d9b.0 -> emSign_ECC_Root_CA_-_C3.pem
│   ├── 4bfab552.0 -> Starfield_Root_Certificate_Authority_-_G2.pem
│   ├── 4f316efb.0 -> SwissSign_Gold_CA_-_G2.pem
│   ├── 5273a94c.0 -> E-Tugra_Certification_Authority.pem
│   ├── 5443e9e3.0 -> T-TeleSec_GlobalRoot_Class_3.pem
│   ├── 54657681.0 -> Buypass_Class_2_Root_CA.pem
│   ├── 57bcb2da.0 -> SwissSign_Silver_CA_-_G2.pem
│   ├── 5a4d6896.0 -> Staat_der_Nederlanden_Root_CA_-_G3.pem
│   ├── 5ad8a5d6.0 -> GlobalSign_Root_CA.pem
│   ├── 5cd81ad7.0 -> TeliaSonera_Root_CA_v1.pem
│   ├── 5d3033c5.0 -> TrustCor_RootCert_CA-1.pem
│   ├── 5e98733a.0 -> Entrust_Root_Certification_Authority_-_G4.pem
│   ├── 5f15c80c.0 -> TWCA_Global_Root_CA.pem
│   ├── 5f618aec.0 -> certSIGN_Root_CA_G2.pem
│   ├── 607986c7.0 -> DigiCert_Global_Root_G2.pem
│   ├── 626dceaf.0 -> GTS_Root_R2.pem
│   ├── 653b494a.0 -> Baltimore_CyberTrust_Root.pem
│   ├── 68dd7389.0 -> Hongkong_Post_Root_CA_3.pem
│   ├── 6b99d060.0 -> Entrust_Root_Certification_Authority.pem
│   ├── 6d41d539.0 -> Amazon_Root_CA_2.pem
│   ├── 6fa5da56.0 -> SSL.com_Root_Certification_Authority_RSA.pem
│   ├── 706f604c.0 -> XRamp_Global_CA_Root.pem
│   ├── 749e9e03.0 -> QuoVadis_Root_CA_1_G3.pem
│   ├── 75d1b2ed.0 -> DigiCert_Trusted_Root_G4.pem
│   ├── 76cb8f92.0 -> Cybertrust_Global_Root.pem
│   ├── 76faf6c0.0 -> QuoVadis_Root_CA_3.pem
│   ├── 7719f463.0 -> Hellenic_Academic_and_Research_Institutions_ECC_RootCA_2015.pem
│   ├── 773e07ad.0 -> OISTE_WISeKey_Global_Root_GC_CA.pem
│   ├── 7aaf71c0.0 -> TrustCor_ECA-1.pem
│   ├── 7f3d5d1d.0 -> DigiCert_Assured_ID_Root_G3.pem
│   ├── 8160b96c.0 -> Microsec_e-Szigno_Root_CA_2009.pem
│   ├── 8cb5ee0f.0 -> Amazon_Root_CA_3.pem
│   ├── 8d86cdd1.0 -> certSIGN_ROOT_CA.pem
│   ├── 8d89cda1.0 -> Microsoft_ECC_Root_Certificate_Authority_2017.pem
│   ├── 930ac5d2.0 -> Actalis_Authentication_Root_CA.pem
│   ├── 93bc0acc.0 -> AffirmTrust_Networking.pem
│   ├── 988a38cb.0 -> NetLock_Arany_=Class_Gold=_Főtanúsítvány.pem
│   ├── 9b5697b0.0 -> Trustwave_Global_ECC_P256_Certification_Authority.pem
│   ├── 9c2e7d30.0 -> Sonera_Class_2_Root_CA.pem
│   ├── 9c8dfbd4.0 -> AffirmTrust_Premium_ECC.pem
│   ├── 9d04f354.0 -> DigiCert_Assured_ID_Root_G2.pem
│   ├── a3418fda.0 -> GTS_Root_R4.pem
│   ├── a94d09e5.0 -> ACCVRAIZ1.pem
│   ├── ACCVRAIZ1.pem -> /usr/share/ca-certificates/mozilla/ACCVRAIZ1.crt
│   ├── AC_RAIZ_FNMT-RCM.pem -> /usr/share/ca-certificates/mozilla/AC_RAIZ_FNMT-RCM.crt
│   ├── Actalis_Authentication_Root_CA.pem -> /usr/share/ca-certificates/mozilla/Actalis_Authentication_Root_CA.crt
│   ├── aee5f10d.0 -> Entrust.net_Premium_2048_Secure_Server_CA.pem
│   ├── AffirmTrust_Commercial.pem -> /usr/share/ca-certificates/mozilla/AffirmTrust_Commercial.crt
│   ├── AffirmTrust_Networking.pem -> /usr/share/ca-certificates/mozilla/AffirmTrust_Networking.crt
│   ├── AffirmTrust_Premium_ECC.pem -> /usr/share/ca-certificates/mozilla/AffirmTrust_Premium_ECC.crt
│   ├── AffirmTrust_Premium.pem -> /usr/share/ca-certificates/mozilla/AffirmTrust_Premium.crt
│   ├── Amazon_Root_CA_1.pem -> /usr/share/ca-certificates/mozilla/Amazon_Root_CA_1.crt
│   ├── Amazon_Root_CA_2.pem -> /usr/share/ca-certificates/mozilla/Amazon_Root_CA_2.crt
│   ├── Amazon_Root_CA_3.pem -> /usr/share/ca-certificates/mozilla/Amazon_Root_CA_3.crt
│   ├── Amazon_Root_CA_4.pem -> /usr/share/ca-certificates/mozilla/Amazon_Root_CA_4.crt
│   ├── Atos_TrustedRoot_2011.pem -> /usr/share/ca-certificates/mozilla/Atos_TrustedRoot_2011.crt
│   ├── Autoridad_de_Certificacion_Firmaprofesional_CIF_A62634068.pem -> /usr/share/ca-certificates/mozilla/Autoridad_de_Certificacion_Firmaprofesional_CIF_A62634068.crt
│   ├── b0e59380.0 -> GlobalSign_ECC_Root_CA_-_R4.pem
│   ├── b1159c4c.0 -> DigiCert_Assured_ID_Root_CA.pem
│   ├── b306b114.0 -> ssl-cert-snakeoil.pem
│   ├── b66938e9.0 -> Secure_Global_CA.pem
│   ├── b727005e.0 -> AffirmTrust_Premium.pem
│   ├── b7a5b843.0 -> TWCA_Root_Certification_Authority.pem
│   ├── Baltimore_CyberTrust_Root.pem -> /usr/share/ca-certificates/mozilla/Baltimore_CyberTrust_Root.crt
│   ├── bf53fb88.0 -> Microsoft_RSA_Root_Certificate_Authority_2017.pem
│   ├── Buypass_Class_2_Root_CA.pem -> /usr/share/ca-certificates/mozilla/Buypass_Class_2_Root_CA.crt
│   ├── Buypass_Class_3_Root_CA.pem -> /usr/share/ca-certificates/mozilla/Buypass_Class_3_Root_CA.crt
│   ├── c01cdfa2.0 -> VeriSign_Universal_Root_Certification_Authority.pem
│   ├── c01eb047.0 -> UCA_Global_G2_Root.pem
│   ├── c28a8a30.0 -> D-TRUST_Root_Class_3_CA_2_2009.pem
│   ├── c47d9980.0 -> Chambers_of_Commerce_Root_-_2008.pem
│   ├── ca6e4ad9.0 -> ePKI_Root_Certification_Authority.pem
│   ├── ca-certificates.crt
│   ├── CA_Disig_Root_R2.pem -> /usr/share/ca-certificates/mozilla/CA_Disig_Root_R2.crt
│   ├── cbf06781.0 -> Go_Daddy_Root_Certificate_Authority_-_G2.pem
│   ├── cc450945.0 -> Izenpe.com.pem
│   ├── cd58d51e.0 -> Security_Communication_RootCA2.pem
│   ├── cd8c0d63.0 -> AC_RAIZ_FNMT-RCM.pem
│   ├── ce5e74ef.0 -> Amazon_Root_CA_1.pem
│   ├── Certigna.pem -> /usr/share/ca-certificates/mozilla/Certigna.crt
│   ├── Certigna_Root_CA.pem -> /usr/share/ca-certificates/mozilla/Certigna_Root_CA.crt
│   ├── certSIGN_Root_CA_G2.pem -> /usr/share/ca-certificates/mozilla/certSIGN_Root_CA_G2.crt
│   ├── certSIGN_ROOT_CA.pem -> /usr/share/ca-certificates/mozilla/certSIGN_ROOT_CA.crt
│   ├── Certum_Trusted_Network_CA_2.pem -> /usr/share/ca-certificates/mozilla/Certum_Trusted_Network_CA_2.crt
│   ├── Certum_Trusted_Network_CA.pem -> /usr/share/ca-certificates/mozilla/Certum_Trusted_Network_CA.crt
│   ├── CFCA_EV_ROOT.pem -> /usr/share/ca-certificates/mozilla/CFCA_EV_ROOT.crt
│   ├── Chambers_of_Commerce_Root_-_2008.pem -> /usr/share/ca-certificates/mozilla/Chambers_of_Commerce_Root_-_2008.crt
│   ├── Comodo_AAA_Services_root.pem -> /usr/share/ca-certificates/mozilla/Comodo_AAA_Services_root.crt
│   ├── COMODO_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/COMODO_Certification_Authority.crt
│   ├── COMODO_ECC_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/COMODO_ECC_Certification_Authority.crt
│   ├── COMODO_RSA_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/COMODO_RSA_Certification_Authority.crt
│   ├── Cybertrust_Global_Root.pem -> /usr/share/ca-certificates/mozilla/Cybertrust_Global_Root.crt
│   ├── d4dae3dd.0 -> D-TRUST_Root_Class_3_CA_2_EV_2009.pem
│   ├── d6325660.0 -> COMODO_RSA_Certification_Authority.pem
│   ├── d7e8dc79.0 -> QuoVadis_Root_CA_2.pem
│   ├── d853d49e.0 -> Trustis_FPS_Root_CA.pem
│   ├── d887a5bb.0 -> Trustwave_Global_ECC_P384_Certification_Authority.pem
│   ├── dc4d6a89.0 -> GlobalSign_Root_CA_-_R6.pem
│   ├── dd8e9d41.0 -> DigiCert_Global_Root_G3.pem
│   ├── de6d66f3.0 -> Amazon_Root_CA_4.pem
│   ├── DigiCert_Assured_ID_Root_CA.pem -> /usr/share/ca-certificates/mozilla/DigiCert_Assured_ID_Root_CA.crt
│   ├── DigiCert_Assured_ID_Root_G2.pem -> /usr/share/ca-certificates/mozilla/DigiCert_Assured_ID_Root_G2.crt
│   ├── DigiCert_Assured_ID_Root_G3.pem -> /usr/share/ca-certificates/mozilla/DigiCert_Assured_ID_Root_G3.crt
│   ├── DigiCert_Global_Root_CA.pem -> /usr/share/ca-certificates/mozilla/DigiCert_Global_Root_CA.crt
│   ├── DigiCert_Global_Root_G2.pem -> /usr/share/ca-certificates/mozilla/DigiCert_Global_Root_G2.crt
│   ├── DigiCert_Global_Root_G3.pem -> /usr/share/ca-certificates/mozilla/DigiCert_Global_Root_G3.crt
│   ├── DigiCert_High_Assurance_EV_Root_CA.pem -> /usr/share/ca-certificates/mozilla/DigiCert_High_Assurance_EV_Root_CA.crt
│   ├── DigiCert_Trusted_Root_G4.pem -> /usr/share/ca-certificates/mozilla/DigiCert_Trusted_Root_G4.crt
│   ├── DST_Root_CA_X3.pem -> /usr/share/ca-certificates/mozilla/DST_Root_CA_X3.crt
│   ├── D-TRUST_Root_Class_3_CA_2_2009.pem -> /usr/share/ca-certificates/mozilla/D-TRUST_Root_Class_3_CA_2_2009.crt
│   ├── D-TRUST_Root_Class_3_CA_2_EV_2009.pem -> /usr/share/ca-certificates/mozilla/D-TRUST_Root_Class_3_CA_2_EV_2009.crt
│   ├── e113c810.0 -> Certigna.pem
│   ├── e18bfb83.0 -> QuoVadis_Root_CA_3_G3.pem
│   ├── e36a6752.0 -> Atos_TrustedRoot_2011.pem
│   ├── e73d606e.0 -> OISTE_WISeKey_Global_Root_GB_CA.pem
│   ├── e868b802.0 -> e-Szigno_Root_CA_2017.pem
│   ├── e8de2f56.0 -> Buypass_Class_3_Root_CA.pem
│   ├── EC-ACC.pem -> /usr/share/ca-certificates/mozilla/EC-ACC.crt
│   ├── ee64a828.0 -> Comodo_AAA_Services_root.pem
│   ├── eed8c118.0 -> COMODO_ECC_Certification_Authority.pem
│   ├── ef954a4e.0 -> IdenTrust_Commercial_Root_CA_1.pem
│   ├── emSign_ECC_Root_CA_-_C3.pem -> /usr/share/ca-certificates/mozilla/emSign_ECC_Root_CA_-_C3.crt
│   ├── emSign_ECC_Root_CA_-_G3.pem -> /usr/share/ca-certificates/mozilla/emSign_ECC_Root_CA_-_G3.crt
│   ├── emSign_Root_CA_-_C1.pem -> /usr/share/ca-certificates/mozilla/emSign_Root_CA_-_C1.crt
│   ├── emSign_Root_CA_-_G1.pem -> /usr/share/ca-certificates/mozilla/emSign_Root_CA_-_G1.crt
│   ├── Entrust.net_Premium_2048_Secure_Server_CA.pem -> /usr/share/ca-certificates/mozilla/Entrust.net_Premium_2048_Secure_Server_CA.crt
│   ├── Entrust_Root_Certification_Authority_-_EC1.pem -> /usr/share/ca-certificates/mozilla/Entrust_Root_Certification_Authority_-_EC1.crt
│   ├── Entrust_Root_Certification_Authority_-_G2.pem -> /usr/share/ca-certificates/mozilla/Entrust_Root_Certification_Authority_-_G2.crt
│   ├── Entrust_Root_Certification_Authority_-_G4.pem -> /usr/share/ca-certificates/mozilla/Entrust_Root_Certification_Authority_-_G4.crt
│   ├── Entrust_Root_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/Entrust_Root_Certification_Authority.crt
│   ├── ePKI_Root_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/ePKI_Root_Certification_Authority.crt
│   ├── e-Szigno_Root_CA_2017.pem -> /usr/share/ca-certificates/mozilla/e-Szigno_Root_CA_2017.crt
│   ├── E-Tugra_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/E-Tugra_Certification_Authority.crt
│   ├── f081611a.0 -> Go_Daddy_Class_2_CA.pem
│   ├── f0c70a8d.0 -> SSL.com_EV_Root_Certification_Authority_ECC.pem
│   ├── f249de83.0 -> Trustwave_Global_Certification_Authority.pem
│   ├── f30dd6ad.0 -> USERTrust_ECC_Certification_Authority.pem
│   ├── f3377b1b.0 -> Security_Communication_Root_CA.pem
│   ├── f387163d.0 -> Starfield_Class_2_CA.pem
│   ├── f39fc864.0 -> SecureTrust_CA.pem
│   ├── f51bb24c.0 -> Certigna_Root_CA.pem
│   ├── fc5a8f99.0 -> USERTrust_RSA_Certification_Authority.pem
│   ├── fe8a2cd8.0 -> SZAFIR_ROOT_CA2.pem
│   ├── ff34af3f.0 -> TUBITAK_Kamu_SM_SSL_Kok_Sertifikasi_-_Surum_1.pem
│   ├── GDCA_TrustAUTH_R5_ROOT.pem -> /usr/share/ca-certificates/mozilla/GDCA_TrustAUTH_R5_ROOT.crt
│   ├── GeoTrust_Primary_Certification_Authority_-_G2.pem -> /usr/share/ca-certificates/mozilla/GeoTrust_Primary_Certification_Authority_-_G2.crt
│   ├── Global_Chambersign_Root_-_2008.pem -> /usr/share/ca-certificates/mozilla/Global_Chambersign_Root_-_2008.crt
│   ├── GlobalSign_ECC_Root_CA_-_R4.pem -> /usr/share/ca-certificates/mozilla/GlobalSign_ECC_Root_CA_-_R4.crt
│   ├── GlobalSign_ECC_Root_CA_-_R5.pem -> /usr/share/ca-certificates/mozilla/GlobalSign_ECC_Root_CA_-_R5.crt
│   ├── GlobalSign_Root_CA.pem -> /usr/share/ca-certificates/mozilla/GlobalSign_Root_CA.crt
│   ├── GlobalSign_Root_CA_-_R2.pem -> /usr/share/ca-certificates/mozilla/GlobalSign_Root_CA_-_R2.crt
│   ├── GlobalSign_Root_CA_-_R3.pem -> /usr/share/ca-certificates/mozilla/GlobalSign_Root_CA_-_R3.crt
│   ├── GlobalSign_Root_CA_-_R6.pem -> /usr/share/ca-certificates/mozilla/GlobalSign_Root_CA_-_R6.crt
│   ├── Go_Daddy_Class_2_CA.pem -> /usr/share/ca-certificates/mozilla/Go_Daddy_Class_2_CA.crt
│   ├── Go_Daddy_Root_Certificate_Authority_-_G2.pem -> /usr/share/ca-certificates/mozilla/Go_Daddy_Root_Certificate_Authority_-_G2.crt
│   ├── GTS_Root_R1.pem -> /usr/share/ca-certificates/mozilla/GTS_Root_R1.crt
│   ├── GTS_Root_R2.pem -> /usr/share/ca-certificates/mozilla/GTS_Root_R2.crt
│   ├── GTS_Root_R3.pem -> /usr/share/ca-certificates/mozilla/GTS_Root_R3.crt
│   ├── GTS_Root_R4.pem -> /usr/share/ca-certificates/mozilla/GTS_Root_R4.crt
│   ├── Hellenic_Academic_and_Research_Institutions_ECC_RootCA_2015.pem -> /usr/share/ca-certificates/mozilla/Hellenic_Academic_and_Research_Institutions_ECC_RootCA_2015.crt
│   ├── Hellenic_Academic_and_Research_Institutions_RootCA_2011.pem -> /usr/share/ca-certificates/mozilla/Hellenic_Academic_and_Research_Institutions_RootCA_2011.crt
│   ├── Hellenic_Academic_and_Research_Institutions_RootCA_2015.pem -> /usr/share/ca-certificates/mozilla/Hellenic_Academic_and_Research_Institutions_RootCA_2015.crt
│   ├── Hongkong_Post_Root_CA_1.pem -> /usr/share/ca-certificates/mozilla/Hongkong_Post_Root_CA_1.crt
│   ├── Hongkong_Post_Root_CA_3.pem -> /usr/share/ca-certificates/mozilla/Hongkong_Post_Root_CA_3.crt
│   ├── IdenTrust_Commercial_Root_CA_1.pem -> /usr/share/ca-certificates/mozilla/IdenTrust_Commercial_Root_CA_1.crt
│   ├── IdenTrust_Public_Sector_Root_CA_1.pem -> /usr/share/ca-certificates/mozilla/IdenTrust_Public_Sector_Root_CA_1.crt
│   ├── ISRG_Root_X1.pem -> /usr/share/ca-certificates/mozilla/ISRG_Root_X1.crt
│   ├── Izenpe.com.pem -> /usr/share/ca-certificates/mozilla/Izenpe.com.crt
│   ├── java
│   │   └── cacerts
│   ├── Microsec_e-Szigno_Root_CA_2009.pem -> /usr/share/ca-certificates/mozilla/Microsec_e-Szigno_Root_CA_2009.crt
│   ├── Microsoft_ECC_Root_Certificate_Authority_2017.pem -> /usr/share/ca-certificates/mozilla/Microsoft_ECC_Root_Certificate_Authority_2017.crt
│   ├── Microsoft_RSA_Root_Certificate_Authority_2017.pem -> /usr/share/ca-certificates/mozilla/Microsoft_RSA_Root_Certificate_Authority_2017.crt
│   ├── NAVER_Global_Root_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/NAVER_Global_Root_Certification_Authority.crt
│   ├── NetLock_Arany_=Class_Gold=_Főtanúsítvány.pem -> /usr/share/ca-certificates/mozilla/NetLock_Arany_=Class_Gold=_Főtanúsítvány.crt
│   ├── Network_Solutions_Certificate_Authority.pem -> /usr/share/ca-certificates/mozilla/Network_Solutions_Certificate_Authority.crt
│   ├── OISTE_WISeKey_Global_Root_GB_CA.pem -> /usr/share/ca-certificates/mozilla/OISTE_WISeKey_Global_Root_GB_CA.crt
│   ├── OISTE_WISeKey_Global_Root_GC_CA.pem -> /usr/share/ca-certificates/mozilla/OISTE_WISeKey_Global_Root_GC_CA.crt
│   ├── QuoVadis_Root_CA_1_G3.pem -> /usr/share/ca-certificates/mozilla/QuoVadis_Root_CA_1_G3.crt
│   ├── QuoVadis_Root_CA_2_G3.pem -> /usr/share/ca-certificates/mozilla/QuoVadis_Root_CA_2_G3.crt
│   ├── QuoVadis_Root_CA_2.pem -> /usr/share/ca-certificates/mozilla/QuoVadis_Root_CA_2.crt
│   ├── QuoVadis_Root_CA_3_G3.pem -> /usr/share/ca-certificates/mozilla/QuoVadis_Root_CA_3_G3.crt
│   ├── QuoVadis_Root_CA_3.pem -> /usr/share/ca-certificates/mozilla/QuoVadis_Root_CA_3.crt
│   ├── QuoVadis_Root_CA.pem -> /usr/share/ca-certificates/mozilla/QuoVadis_Root_CA.crt
│   ├── Secure_Global_CA.pem -> /usr/share/ca-certificates/mozilla/Secure_Global_CA.crt
│   ├── SecureSign_RootCA11.pem -> /usr/share/ca-certificates/mozilla/SecureSign_RootCA11.crt
│   ├── SecureTrust_CA.pem -> /usr/share/ca-certificates/mozilla/SecureTrust_CA.crt
│   ├── Security_Communication_RootCA2.pem -> /usr/share/ca-certificates/mozilla/Security_Communication_RootCA2.crt
│   ├── Security_Communication_Root_CA.pem -> /usr/share/ca-certificates/mozilla/Security_Communication_Root_CA.crt
│   ├── Sonera_Class_2_Root_CA.pem -> /usr/share/ca-certificates/mozilla/Sonera_Class_2_Root_CA.crt
│   ├── ssl-cert-snakeoil.pem
│   ├── SSL.com_EV_Root_Certification_Authority_ECC.pem -> /usr/share/ca-certificates/mozilla/SSL.com_EV_Root_Certification_Authority_ECC.crt
│   ├── SSL.com_EV_Root_Certification_Authority_RSA_R2.pem -> /usr/share/ca-certificates/mozilla/SSL.com_EV_Root_Certification_Authority_RSA_R2.crt
│   ├── SSL.com_Root_Certification_Authority_ECC.pem -> /usr/share/ca-certificates/mozilla/SSL.com_Root_Certification_Authority_ECC.crt
│   ├── SSL.com_Root_Certification_Authority_RSA.pem -> /usr/share/ca-certificates/mozilla/SSL.com_Root_Certification_Authority_RSA.crt
│   ├── Staat_der_Nederlanden_EV_Root_CA.pem -> /usr/share/ca-certificates/mozilla/Staat_der_Nederlanden_EV_Root_CA.crt
│   ├── Staat_der_Nederlanden_Root_CA_-_G3.pem -> /usr/share/ca-certificates/mozilla/Staat_der_Nederlanden_Root_CA_-_G3.crt
│   ├── Starfield_Class_2_CA.pem -> /usr/share/ca-certificates/mozilla/Starfield_Class_2_CA.crt
│   ├── Starfield_Root_Certificate_Authority_-_G2.pem -> /usr/share/ca-certificates/mozilla/Starfield_Root_Certificate_Authority_-_G2.crt
│   ├── Starfield_Services_Root_Certificate_Authority_-_G2.pem -> /usr/share/ca-certificates/mozilla/Starfield_Services_Root_Certificate_Authority_-_G2.crt
│   ├── SwissSign_Gold_CA_-_G2.pem -> /usr/share/ca-certificates/mozilla/SwissSign_Gold_CA_-_G2.crt
│   ├── SwissSign_Silver_CA_-_G2.pem -> /usr/share/ca-certificates/mozilla/SwissSign_Silver_CA_-_G2.crt
│   ├── SZAFIR_ROOT_CA2.pem -> /usr/share/ca-certificates/mozilla/SZAFIR_ROOT_CA2.crt
│   ├── TeliaSonera_Root_CA_v1.pem -> /usr/share/ca-certificates/mozilla/TeliaSonera_Root_CA_v1.crt
│   ├── TrustCor_ECA-1.pem -> /usr/share/ca-certificates/mozilla/TrustCor_ECA-1.crt
│   ├── TrustCor_RootCert_CA-1.pem -> /usr/share/ca-certificates/mozilla/TrustCor_RootCert_CA-1.crt
│   ├── TrustCor_RootCert_CA-2.pem -> /usr/share/ca-certificates/mozilla/TrustCor_RootCert_CA-2.crt
│   ├── Trustis_FPS_Root_CA.pem -> /usr/share/ca-certificates/mozilla/Trustis_FPS_Root_CA.crt
│   ├── Trustwave_Global_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/Trustwave_Global_Certification_Authority.crt
│   ├── Trustwave_Global_ECC_P256_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/Trustwave_Global_ECC_P256_Certification_Authority.crt
│   ├── Trustwave_Global_ECC_P384_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/Trustwave_Global_ECC_P384_Certification_Authority.crt
│   ├── T-TeleSec_GlobalRoot_Class_2.pem -> /usr/share/ca-certificates/mozilla/T-TeleSec_GlobalRoot_Class_2.crt
│   ├── T-TeleSec_GlobalRoot_Class_3.pem -> /usr/share/ca-certificates/mozilla/T-TeleSec_GlobalRoot_Class_3.crt
│   ├── TUBITAK_Kamu_SM_SSL_Kok_Sertifikasi_-_Surum_1.pem -> /usr/share/ca-certificates/mozilla/TUBITAK_Kamu_SM_SSL_Kok_Sertifikasi_-_Surum_1.crt
│   ├── TWCA_Global_Root_CA.pem -> /usr/share/ca-certificates/mozilla/TWCA_Global_Root_CA.crt
│   ├── TWCA_Root_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/TWCA_Root_Certification_Authority.crt
│   ├── UCA_Extended_Validation_Root.pem -> /usr/share/ca-certificates/mozilla/UCA_Extended_Validation_Root.crt
│   ├── UCA_Global_G2_Root.pem -> /usr/share/ca-certificates/mozilla/UCA_Global_G2_Root.crt
│   ├── USERTrust_ECC_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/USERTrust_ECC_Certification_Authority.crt
│   ├── USERTrust_RSA_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/USERTrust_RSA_Certification_Authority.crt
│   ├── VeriSign_Universal_Root_Certification_Authority.pem -> /usr/share/ca-certificates/mozilla/VeriSign_Universal_Root_Certification_Authority.crt
│   └── XRamp_Global_CA_Root.pem -> /usr/share/ca-certificates/mozilla/XRamp_Global_CA_Root.crt
├── openssl.cnf
└── private [error opening dir]

3 directories, 263 files
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -text -in provacert.pem 
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            70:5c:d2:10:f1:a1:84:95:e0:72:7c:8d:4a:6e:39:5d:25:73:a1:c3
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = web.edt.org, emailAddress = web@edt.org
        Validity
            Not Before: Apr 19 07:38:17 2023 GMT
            Not After : May 19 07:38:17 2023 GMT
        Subject: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = web.edt.org, emailAddress = web@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:c5:59:48:fe:6d:02:74:02:b6:1a:8e:6b:2c:69:
                    97:2a:6e:96:47:e7:d4:8d:76:e8:b3:8e:79:21:87:
                    ce:dc:80:42:93:de:12:6a:13:4e:65:42:00:c1:1a:
                    06:f8:31:c2:5c:54:99:38:2a:2a:a0:07:62:9f:89:
                    06:d7:8d:16:0a:6f:2e:0f:3b:29:44:d4:3b:28:14:
                    9b:48:b2:5d:c2:0a:db:82:bf:5c:3e:4e:f8:f1:1d:
                    da:3c:ff:06:fe:67:8e:2e:3f:04:33:b4:db:a9:20:
                    88:33:13:67:60:1b:9f:a2:97:88:ce:a9:69:a4:e7:
                    11:25:91:24:3a:c5:bb:7a:d2:49:d9:29:2b:2e:b6:
                    0e:d8:32:48:04:32:92:bc:69:1e:6f:2d:63:e3:55:
                    13:96:b5:2c:fa:a4:85:df:ec:45:38:8e:68:ea:30:
                    ba:d2:f0:40:fa:ab:8d:7d:7e:28:78:ef:1a:68:4f:
                    39:2f:3b:91:0e:52:48:39:b9:b7:5c:24:34:f2:84:
                    42:63:34:23:20:59:18:0d:e9:14:bd:f5:34:7c:03:
                    82:e3:b2:f4:a3:7e:4a:55:eb:e6:00:c0:b2:d6:41:
                    a0:1c:ff:eb:71:3c:c2:c7:56:7b:87:71:9a:5f:36:
                    00:69:e1:8d:51:d4:be:02:c8:81:ef:8a:3f:b5:70:
                    8d:d1
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier: 
                2F:63:56:7D:32:10:B7:06:F3:2F:9D:CF:FB:91:09:93:9E:86:86:CB
            X509v3 Authority Key Identifier: 
                keyid:2F:63:56:7D:32:10:B7:06:F3:2F:9D:CF:FB:91:09:93:9E:86:86:CB

            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         b4:71:25:93:69:26:b0:e9:66:b3:68:8a:5b:53:80:9d:28:fc:
         0d:b4:ce:a0:e8:53:5b:4d:3f:44:d8:63:35:f3:1c:49:2f:7b:
         55:47:e2:9c:4a:78:41:b0:20:f5:05:4f:65:69:ec:c6:e9:d7:
         46:f9:85:42:ca:84:ed:6e:b1:e4:b3:20:ff:af:4a:ae:2f:43:
         30:ad:fc:a7:f1:da:da:87:eb:d4:12:4c:a6:3d:fe:47:a0:73:
         bf:da:d2:bf:7f:61:a1:b6:c4:ed:d5:75:85:cc:06:9e:80:90:
         a8:91:a9:91:83:af:56:24:75:af:8f:1d:7b:1b:6c:50:da:16:
         52:0b:f0:52:0d:0a:9d:8d:eb:6a:c8:aa:45:aa:bf:eb:98:d4:
         51:4e:0f:05:11:2a:c7:c5:87:27:48:77:7d:ac:42:d0:c3:62:
         5a:15:88:93:70:6d:6b:6a:19:dc:05:7a:e4:44:03:05:84:03:
         fb:c0:c4:9d:c3:2d:2a:94:42:82:3d:b4:25:28:6e:84:88:2c:
         44:bd:54:66:02:65:c9:a6:ce:cf:9c:68:4c:e7:ef:11:43:64:
         98:2c:0c:42:dc:51:9d:cf:be:24:9e:33:49:2a:f5:91:51:55:
         84:05:66:ba:00:13:4b:37:bd:90:f5:f8:83:5b:50:b3:29:45:
         d0:56:8e:b6
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ #openssl req -new -x509 -nodes -out p2cert.pem -key provakey.pem -config openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ #issuer = subject --> autosignat
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -new -x509 -nodes -out p2cert.pem -key provakey.pem -config openssl.cnf 
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:^C
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -new -x509 -nodes -out p2cert.pem -key provakey.pem -config openssl.cnf 
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [CA]:
State or Province Name (full name) [ca]:
Locality Name (eg, city) [bcn]:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:inf
Common Name (e.g. server FQDN or YOUR name) []:web.edt^C  
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -new -x509 -nodes -out p2cert.pem -key provakey.pem -config openssl.cnf -extension usr_cert
req: Unrecognized flag extension
req: Use -help for summary.
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -new -x509 -nodes -out p2cert.pem -key provakey.pem -config openssl.cnf -extensions usr_cert
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [CA]:^C
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl req -new -x509 -nodes -out p2cert.pem -key provakey.pem -config openssl.cnf -extensions my_client 
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [CA]:
State or Province Name (full name) [ca]:
Locality Name (eg, city) [bcn]:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:web.edt.org
Email Address []:web@edt.org
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ openssl x509 -noout -text -in p2cert.pem 
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            3a:75:1e:21:ad:9c:83:a7:ca:e0:20:ea:28:d1:01:15:88:17:2e:19
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = CA, ST = ca, L = bcn, O = Internet Widgits Pty Ltd, CN = web.edt.org, emailAddress = web@edt.org
        Validity
            Not Before: Apr 19 08:05:35 2023 GMT
            Not After : May 19 08:05:35 2023 GMT
        Subject: C = CA, ST = ca, L = bcn, O = Internet Widgits Pty Ltd, CN = web.edt.org, emailAddress = web@edt.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:c5:59:48:fe:6d:02:74:02:b6:1a:8e:6b:2c:69:
                    97:2a:6e:96:47:e7:d4:8d:76:e8:b3:8e:79:21:87:
                    ce:dc:80:42:93:de:12:6a:13:4e:65:42:00:c1:1a:
                    06:f8:31:c2:5c:54:99:38:2a:2a:a0:07:62:9f:89:
                    06:d7:8d:16:0a:6f:2e:0f:3b:29:44:d4:3b:28:14:
                    9b:48:b2:5d:c2:0a:db:82:bf:5c:3e:4e:f8:f1:1d:
                    da:3c:ff:06:fe:67:8e:2e:3f:04:33:b4:db:a9:20:
                    88:33:13:67:60:1b:9f:a2:97:88:ce:a9:69:a4:e7:
                    11:25:91:24:3a:c5:bb:7a:d2:49:d9:29:2b:2e:b6:
                    0e:d8:32:48:04:32:92:bc:69:1e:6f:2d:63:e3:55:
                    13:96:b5:2c:fa:a4:85:df:ec:45:38:8e:68:ea:30:
                    ba:d2:f0:40:fa:ab:8d:7d:7e:28:78:ef:1a:68:4f:
                    39:2f:3b:91:0e:52:48:39:b9:b7:5c:24:34:f2:84:
                    42:63:34:23:20:59:18:0d:e9:14:bd:f5:34:7c:03:
                    82:e3:b2:f4:a3:7e:4a:55:eb:e6:00:c0:b2:d6:41:
                    a0:1c:ff:eb:71:3c:c2:c7:56:7b:87:71:9a:5f:36:
                    00:69:e1:8d:51:d4:be:02:c8:81:ef:8a:3f:b5:70:
                    8d:d1
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            X509v3 Subject Key Identifier: 
                2F:63:56:7D:32:10:B7:06:F3:2F:9D:CF:FB:91:09:93:9E:86:86:CB
            X509v3 Authority Key Identifier: 
                keyid:2F:63:56:7D:32:10:B7:06:F3:2F:9D:CF:FB:91:09:93:9E:86:86:CB
                DirName:/C=CA/ST=ca/L=bcn/O=Internet Widgits Pty Ltd/CN=web.edt.org/emailAddress=web@edt.org
                serial:3A:75:1E:21:AD:9C:83:A7:CA:E0:20:EA:28:D1:01:15:88:17:2E:19

    Signature Algorithm: sha256WithRSAEncryption
         ae:e6:b0:96:04:4f:c6:d2:96:91:d4:b8:c4:cd:89:8b:1c:03:
         52:88:95:21:f5:af:16:ff:55:ea:70:4d:b0:8e:b8:b1:5d:b6:
         f7:d2:83:dc:fe:46:9b:29:bf:c4:ab:06:7c:ab:fc:72:47:cd:
         f2:a4:05:9e:e8:3b:d5:c2:a8:ea:af:96:fe:63:b1:fb:5f:4b:
         45:9c:99:0e:2a:c2:1c:d2:a7:6f:7f:61:4b:ce:9d:29:8d:10:
         a2:3f:f5:99:d3:49:59:18:8e:da:8c:32:1f:3b:3d:95:4c:49:
         59:5b:98:24:51:ca:7c:45:89:18:bc:da:8d:fa:26:26:54:91:
         4a:41:88:cb:17:52:a9:0e:86:25:24:14:9c:bd:d5:a9:70:f2:
         8b:01:6e:17:1f:7f:ec:50:ed:fb:c6:11:d7:e8:66:2b:8f:15:
         6c:fd:84:3f:da:1b:80:99:17:5d:8d:03:66:93:10:1b:1d:82:
         ae:8c:b9:b7:9a:3a:87:4b:d6:98:96:e2:e1:de:07:04:17:c4:
         7c:0b:ad:ae:18:f0:02:a3:c4:2a:f3:04:c5:e1:a4:c4:d9:36:
         2c:d7:ed:85:a3:ea:ef:87:d4:eb:a7:5b:10:6a:a7:3d:94:43:
         05:0d:25:3e:55:ac:f6:95:c1:a9:17:47:e5:62:0c:83:68:d0:
         0c:52:5b:d0
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ mkdir ca
a191466js@g22:~/Documents/m11/uf2/certificados/ssl$ cd ca
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ ls
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ pwd
/home/users/inf/hisx2/a191466js/Documents/m11/uf2/certificados/ssl/ca
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ cp ../openssl.cnf .
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ mkdir certs newcerts private crl
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ openssl req -new -x509 -nodes -out cacert.pem -keyout cakey.pem
Generating a RSA private key
................+++++
.........+++++
writing new private key to 'cakey.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:ca
State or Province Name (full name) [Some-State]:ca
Locality Name (eg, city) []:bcn
Organization Name (eg, company) [Internet Widgits Pty Ltd]:edt
Organizational Unit Name (eg, section) []:inf
Common Name (e.g. server FQDN or YOUR name) []:VeritatAbsoluta
Email Address []:veritat@edt.org
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ ls
cacert.pem  cakey.pem  certs  crl  newcerts  openssl.cnf  private
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ mv cakey.pem private/
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ ls
cacert.pem  certs  crl  newcerts  openssl.cnf  private
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ echo "01" > serial
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ touch index.txt
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ ls
cacert.pem  certs  crl  index.txt  newcerts  openssl.cnf  private  serial
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ openssl req -new -nodes -keyout client.pem -out clientreq.pem
Generating a RSA private key
.....................................+++++
..................................+++++
writing new private key to 'client.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:
State or Province Name (full name) [Some-State]:albany
Locality Name (eg, city) []:adelaida
Organization Name (eg, company) [Internet Widgits Pty Ltd]:ossi
Organizational Unit Name (eg, section) []:
Common Name (e.g. server FQDN or YOUR name) []:web.edt.org
Email Address []:web@au.au

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ openssl ca -in clientreq.pem -out clientcert.pem -config openssl.cnf 
Using configuration from openssl.cnf
Check that the request matches the signature
Signature ok
The countryName field is different between
CA certificate (ca) and the request (AU)
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ openssl ca -in clientreq.pem -out clientcert.pem -config openssl.cnf 
Using configuration from openssl.cnf
Check that the request matches the signature
Signature ok
The countryName field is different between
CA certificate (ca) and the request (AU)
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ openssl ca -in clientreq.pem -out clientcert.pem -config openssl.cnf 
Using configuration from openssl.cnf
Check that the request matches the signature
Signature ok
Certificate Details:
        Serial Number: 1 (0x1)
        Validity
            Not Before: Apr 19 08:30:20 2023 GMT
            Not After : Apr 18 08:30:20 2024 GMT
        Subject:
            countryName               = AU
            stateOrProvinceName       = albany
            localityName              = adelaida
            organizationName          = ossi
            commonName                = web.edt.org
            emailAddress              = web@au.au
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Comment: 
                OpenSSL Generated Certificate
            X509v3 Subject Key Identifier: 
                04:67:9D:08:E7:30:FC:B6:67:D5:F9:BB:56:33:A7:98:F5:1E:6D:01
            X509v3 Authority Key Identifier: 
                keyid:B2:FE:A2:E2:DC:41:AB:CB:DF:0E:A4:1E:60:5D:67:8F:F3:9F:07:AC

Certificate is to be certified until Apr 18 08:30:20 2024 GMT (365 days)
Sign the certificate? [y/n]:y


1 out of 1 certificate requests certified, commit? [y/n]y
Write out database with 1 new entries
Data Base Updated
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ tre
bash: tre: command not found
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ tree
.
├── cacert.pem
├── certs
├── clientcert.pem
├── client.pem
├── clientreq.pem
├── crl
├── index.txt
├── index.txt.attr
├── index.txt.old
├── newcerts
│   └── 01.pem
├── openssl.cnf
├── private
│   └── cakey.pem
├── serial
└── serial.old

4 directories, 12 files
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ cat serial
02
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ cat index.txt
V	240418083020Z		01	unknown	/C=AU/ST=albany/L=adelaida/O=ossi/CN=web.edt.org/emailAddress=web@au.au
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ openssl x509 -noout -text -in clientcert.pem 
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 1 (0x1)
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = ca, ST = ca, L = bcn, O = edt, OU = inf, CN = VeritatAbsoluta, emailAddress = veritat@edt.org
        Validity
            Not Before: Apr 19 08:30:20 2023 GMT
            Not After : Apr 18 08:30:20 2024 GMT
        Subject: C = AU, ST = albany, L = adelaida, O = ossi, CN = web.edt.org, emailAddress = web@au.au
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:bd:2f:c0:32:64:68:05:3b:4f:88:9b:74:88:4a:
                    1e:3e:ed:71:21:42:e9:c3:23:9e:a4:5e:88:68:cd:
                    f7:75:31:5c:4c:35:db:ce:64:a8:af:f4:bf:5f:07:
                    a2:5d:3f:4d:17:3f:40:00:9c:85:9f:e1:a0:fa:df:
                    6e:79:23:1e:96:00:b7:20:8d:1c:bc:d0:18:f1:25:
                    e5:6f:39:4f:79:32:10:05:e5:45:47:20:ce:5c:5c:
                    bf:9c:0e:43:f2:65:21:0b:32:d7:9a:52:e8:54:3f:
                    92:35:87:b5:21:30:fe:44:e7:85:ef:96:cd:55:47:
                    44:d3:9c:19:bf:b7:9a:db:14:1a:9a:90:78:6c:55:
                    9f:c9:2f:57:03:79:0e:3c:ed:51:23:09:a1:23:6d:
                    cb:3f:3c:b9:94:47:9b:c2:7d:d5:86:c4:e8:85:f2:
                    58:fe:43:0e:01:3d:72:5c:2d:c8:ed:be:94:85:67:
                    21:9c:c2:bd:f9:78:47:99:fc:5e:e4:06:6a:ee:f5:
                    40:99:79:75:68:fd:dd:10:44:40:ec:b8:39:7c:c3:
                    d4:c5:30:13:e7:76:8b:8d:b4:e1:c1:88:c6:71:cd:
                    c7:c1:04:1e:dc:0e:0e:76:07:ec:da:6a:5f:4b:bd:
                    c0:e5:76:43:5c:03:fe:3d:59:8a:d9:67:da:aa:db:
                    6c:2d
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints: 
                CA:FALSE
            Netscape Comment: 
                OpenSSL Generated Certificate
            X509v3 Subject Key Identifier: 
                04:67:9D:08:E7:30:FC:B6:67:D5:F9:BB:56:33:A7:98:F5:1E:6D:01
            X509v3 Authority Key Identifier: 
                keyid:B2:FE:A2:E2:DC:41:AB:CB:DF:0E:A4:1E:60:5D:67:8F:F3:9F:07:AC

    Signature Algorithm: sha256WithRSAEncryption
         17:07:58:cb:d7:ff:cc:e2:e5:9d:50:a1:b0:01:ab:7f:17:4a:
         8e:52:8e:3a:ed:20:b6:4f:3e:b9:36:f2:8e:d0:81:28:bd:0f:
         26:1d:28:80:6b:17:7e:df:44:5e:ae:52:09:d2:4f:30:0c:ea:
         82:be:a9:e8:8d:63:45:b7:4a:68:bb:66:d0:fb:65:bb:09:8c:
         0f:d3:15:69:af:ee:95:55:22:d4:a4:d6:f9:74:b0:53:40:b9:
         9c:ed:ba:12:d7:ae:19:d3:c3:e5:64:2b:f0:81:c9:45:7e:bb:
         c1:dd:4a:83:78:2a:43:dd:bf:53:b5:ae:d7:ae:dd:75:f1:b6:
         66:71:14:90:16:bf:1a:6b:5d:63:34:87:97:9e:a8:4d:91:a9:
         66:52:ad:e7:0d:72:82:16:c1:6b:0a:9b:73:8b:bf:51:75:10:
         d7:b0:f5:81:a9:6c:fd:b6:07:42:3b:e9:4f:d8:e9:3c:d7:9a:
         02:6a:d4:ab:c1:8f:3c:00:f4:87:8b:02:d1:6d:7a:ea:1a:da:
         a2:b6:12:76:41:c1:18:86:a5:9b:a0:c1:e1:18:9e:4d:9e:33:
         70:84:d2:3f:c8:2f:4a:61:ed:70:05:5c:f0:c7:18:5f:cc:4c:
         5f:fb:c3:1c:c8:10:16:9e:10:1d:a5:96:02:43:ae:df:48:1e:
         e9:c3:c2:15
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ cat openssl.cnf 
#
# OpenSSL example configuration file.
# This is mostly being used for generation of certificate requests.
#

# Note that you can include other files from the main configuration
# file using the .include directive.
#.include filename

# This definition stops the following lines choking if HOME isn't
# defined.
HOME			= .

# Extra OBJECT IDENTIFIER info:
#oid_file		= $ENV::HOME/.oid
oid_section		= new_oids

# System default
openssl_conf = default_conf

# To use this configuration file with the "-extfile" option of the
# "openssl x509" utility, name here the section containing the
# X.509v3 extensions to use:
# extensions		=
# (Alternatively, use a configuration file that has only
# X.509v3 extensions in its main [= default] section.)

[ new_oids ]

# We can add new OIDs in here for use by 'ca', 'req' and 'ts'.
# Add a simple OID like this:
# testoid1=1.2.3.4
# Or use config file substitution like this:
# testoid2=${testoid1}.5.6

# Policies used by the TSA examples.
tsa_policy1 = 1.2.3.4.1
tsa_policy2 = 1.2.3.4.5.6
tsa_policy3 = 1.2.3.4.5.7

####################################################################
[ ca ]
default_ca	= CA_default		# The default ca section

####################################################################
[ CA_default ]

dir		= .		# Where everything is kept
certs		= $dir/certs		# Where the issued certs are kept
crl_dir		= $dir/crl		# Where the issued crl are kept
database	= $dir/index.txt	# database index file.
#unique_subject	= no			# Set to 'no' to allow creation of
					# several certs with same subject.
new_certs_dir	= $dir/newcerts		# default place for new certs.

certificate	= $dir/cacert.pem 	# The CA certificate
serial		= $dir/serial 		# The current serial number
crlnumber	= $dir/crlnumber	# the current crl number
					# must be commented out to leave a V1 CRL
crl		= $dir/crl.pem 		# The current CRL
private_key	= $dir/private/cakey.pem# The private key

x509_extensions	= usr_cert		# The extensions to add to the cert

# Comment out the following two lines for the "traditional"
# (and highly broken) format.
name_opt 	= ca_default		# Subject Name options
cert_opt 	= ca_default		# Certificate field options

# Extension copying option: use with caution.
# copy_extensions = copy

# Extensions to add to a CRL. Note: Netscape communicator chokes on V2 CRLs
# so this is commented out by default to leave a V1 CRL.
# crlnumber must also be commented out to leave a V1 CRL.
# crl_extensions	= crl_ext

default_days	= 365			# how long to certify for
default_crl_days= 30			# how long before next CRL
default_md	= default		# use public key default MD
preserve	= no			# keep passed DN ordering

# A few difference way of specifying how similar the request should look
# For type CA, the listed attributes must be the same, and the optional
# and supplied fields are just that :-)
policy		= policy_anything

# For the CA policy
[ policy_match ]
countryName		= match
stateOrProvinceName	= match
organizationName	= match
organizationalUnitName	= optional
commonName		= supplied
emailAddress		= optional

# For the 'anything' policy
# At this point in time, you must list all acceptable 'object'
# types.
[ policy_anything ]
countryName		= optional
stateOrProvinceName	= optional
localityName		= optional
organizationName	= optional
organizationalUnitName	= optional
commonName		= supplied
emailAddress		= optional

####################################################################
[ req ]
default_bits		= 2048
default_keyfile 	= privkey.pem
distinguished_name	= req_distinguished_name
attributes		= req_attributes
x509_extensions	= v3_ca	# The extensions to add to the self signed cert

# Passwords for private keys if not present they will be prompted for
# input_password = secret
# output_password = secret

# This sets a mask for permitted string types. There are several options.
# default: PrintableString, T61String, BMPString.
# pkix	 : PrintableString, BMPString (PKIX recommendation before 2004)
# utf8only: only UTF8Strings (PKIX recommendation after 2004).
# nombstr : PrintableString, T61String (no BMPStrings or UTF8Strings).
# MASK:XXXX a literal mask value.
# WARNING: ancient versions of Netscape crash on BMPStrings or UTF8Strings.
string_mask = utf8only

# req_extensions = v3_req # The extensions to add to a certificate request

[ req_distinguished_name ]
countryName			= Country Name (2 letter code)
countryName_default		= CA
countryName_min			= 2
countryName_max			= 2

stateOrProvinceName		= State or Province Name (full name)
stateOrProvinceName_default	= ca

localityName			= Locality Name (eg, city)
localityName_default		= bcn
0.organizationName		= Organization Name (eg, company)
0.organizationName_default	= edt

# we can do this but it is not needed normally :-)
#1.organizationName		= Second Organization Name (eg, company)
#1.organizationName_default	= World Wide Web Pty Ltd

organizationalUnitName		= Organizational Unit Name (eg, section)
#organizationalUnitName_default	=

commonName			= Common Name (e.g. server FQDN or YOUR name)
commonName_max			= 64

emailAddress			= Email Address
emailAddress_max		= 64

# SET-ex3			= SET extension number 3

[ req_attributes ]
challengePassword		= A challenge password
challengePassword_min		= 4
challengePassword_max		= 20

unstructuredName		= An optional company name

[ usr_cert ]

# These extensions are added when 'ca' signs a request.

# This goes against PKIX guidelines but some CAs do it and some software
# requires this to avoid interpreting an end user certificate as a CA.

basicConstraints=CA:FALSE

# Here are some examples of the usage of nsCertType. If it is omitted
# the certificate can be used for anything *except* object signing.

# This is OK for an SSL server.
# nsCertType			= server

# For an object signing certificate this would be used.
# nsCertType = objsign

# For normal client use this is typical
# nsCertType = client, email

# and for everything including object signing:
# nsCertType = client, email, objsign

# This is typical in keyUsage for a client certificate.
# keyUsage = nonRepudiation, digitalSignature, keyEncipherment

# This will be displayed in Netscape's comment listbox.
nsComment			= "OpenSSL Generated Certificate"

# PKIX recommendations harmless if included in all certificates.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer

# This stuff is for subjectAltName and issuerAltname.
# Import the email address.
# subjectAltName=email:copy
# An alternative to produce certificates that aren't
# deprecated according to PKIX.
# subjectAltName=email:move

# Copy subject details
# issuerAltName=issuer:copy

#nsCaRevocationUrl		= http://www.domain.dom/ca-crl.pem
#nsBaseUrl
#nsRevocationUrl
#nsRenewalUrl
#nsCaPolicyUrl
#nsSslServerName

# This is required for TSA certificates.
# extendedKeyUsage = critical,timeStamping

[ v3_req ]

# Extensions to add to a certificate request

basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment

[ v3_ca ]


# Extensions for a typical CA


# PKIX recommendation.

subjectKeyIdentifier=hash

authorityKeyIdentifier=keyid:always,issuer

basicConstraints = critical,CA:true

# Key usage: this is typical for a CA certificate. However since it will
# prevent it being used as an test self-signed certificate it is best
# left out by default.
# keyUsage = cRLSign, keyCertSign

# Some might want this also
# nsCertType = sslCA, emailCA

# Include email address in subject alt name: another PKIX recommendation
# subjectAltName=email:copy
# Copy issuer details
# issuerAltName=issuer:copy

# DER hex encoding of an extension: beware experts only!
# obj=DER:02:03
# Where 'obj' is a standard or added object
# You can even override a supported extension:
# basicConstraints= critical, DER:30:03:01:01:FF

[ crl_ext ]

# CRL extensions.
# Only issuerAltName and authorityKeyIdentifier make any sense in a CRL.

# issuerAltName=issuer:copy
authorityKeyIdentifier=keyid:always

[ proxy_cert_ext ]
# These extensions should be added when creating a proxy certificate

# This goes against PKIX guidelines but some CAs do it and some software
# requires this to avoid interpreting an end user certificate as a CA.

basicConstraints=CA:FALSE

# Here are some examples of the usage of nsCertType. If it is omitted
# the certificate can be used for anything *except* object signing.

# This is OK for an SSL server.
# nsCertType			= server

# For an object signing certificate this would be used.
# nsCertType = objsign

# For normal client use this is typical
# nsCertType = client, email

# and for everything including object signing:
# nsCertType = client, email, objsign

# This is typical in keyUsage for a client certificate.
# keyUsage = nonRepudiation, digitalSignature, keyEncipherment

# This will be displayed in Netscape's comment listbox.
nsComment			= "OpenSSL Generated Certificate"

# PKIX recommendations harmless if included in all certificates.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer

# This stuff is for subjectAltName and issuerAltname.
# Import the email address.
# subjectAltName=email:copy
# An alternative to produce certificates that aren't
# deprecated according to PKIX.
# subjectAltName=email:move

# Copy subject details
# issuerAltName=issuer:copy

#nsCaRevocationUrl		= http://www.domain.dom/ca-crl.pem
#nsBaseUrl
#nsRevocationUrl
#nsRenewalUrl
#nsCaPolicyUrl
#nsSslServerName

# This really needs to be in place for it to be a proxy certificate.
proxyCertInfo=critical,language:id-ppl-anyLanguage,pathlen:3,policy:foo

####################################################################
[ tsa ]

default_tsa = tsa_config1	# the default TSA section

[ tsa_config1 ]

# These are used by the TSA reply generation only.
dir		= ./demoCA		# TSA root directory
serial		= $dir/tsaserial	# The current serial number (mandatory)
crypto_device	= builtin		# OpenSSL engine to use for signing
signer_cert	= $dir/tsacert.pem 	# The TSA signing certificate
					# (optional)
certs		= $dir/cacert.pem	# Certificate chain to include in reply
					# (optional)
signer_key	= $dir/private/tsakey.pem # The TSA private key (optional)
signer_digest  = sha256			# Signing digest to use. (Optional)
default_policy	= tsa_policy1		# Policy if request did not specify it
					# (optional)
other_policies	= tsa_policy2, tsa_policy3	# acceptable policies (optional)
digests     = sha1, sha256, sha384, sha512  # Acceptable message digests (mandatory)
accuracy	= secs:1, millisecs:500, microsecs:100	# (optional)
clock_precision_digits  = 0	# number of digits after dot. (optional)
ordering		= yes	# Is ordering defined for timestamps?
				# (optional, default: no)
tsa_name		= yes	# Must the TSA name be included in the reply?
				# (optional, default: no)
ess_cert_id_chain	= no	# Must the ESS cert id chain be included?
				# (optional, default: no)
ess_cert_id_alg		= sha1	# algorithm to compute certificate
				# identifier (optional, default: sha1)
[default_conf]
ssl_conf = ssl_sect

[ssl_sect]
system_default = system_default_sect

[system_default_sect]
MinProtocol = TLSv1.2
CipherString = DEFAULT@SECLEVEL=2

[ my_client ]
basicConstraints	= CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always

[ my_server ]
basicConstraints	= CA:FALSE
nsCertType	= server
nsComment	= "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
extendedKeyUsage	= serverAuth
keyUsage	= digitalSignature, keyEncipherment
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ openssl req -new -key clientkey.pem -out client2req.pem
Can't open clientkey.pem for reading, No such file or directory
139745584653632:error:02001002:system library:fopen:No such file or directory:../crypto/bio/bss_file.c:69:fopen('clientkey.pem','r')
139745584653632:error:2006D080:BIO routines:BIO_new_file:no such file:../crypto/bio/bss_file.c:76:
unable to load Private Key
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ openssl req -new -key clientkey.pem -out client2req.pem
Can't open clientkey.pem for reading, No such file or directory
140271277532480:error:02001002:system library:fopen:No such file or directory:../crypto/bio/bss_file.c:69:fopen('clientkey.pem','r')
140271277532480:error:2006D080:BIO routines:BIO_new_file:no such file:../crypto/bio/bss_file.c:76:
unable to load Private Key
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$ vim openssl.cnf 
a191466js@g22:~/Documents/m11/uf2/certificados/ssl/ca$  
