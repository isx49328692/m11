# root vim
a191466js@g22:~/Documents/m11/uf3/iptable$ ls
apuntes
a191466js@g22:~/Documents/m11/uf3/iptable$ mkdir iptables
a191466js@g22:~/Documents/m11/uf3/iptable$ ls
apuntes  iptables
a191466js@g22:~/Documents/m11/uf3/iptable$ cd iptables/
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ su -
Password: 
root@g22:~# 
logout
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ pwd
/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ su -
Password: 
root@g22:~# cd /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# ls
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# pwd
/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
LIBVIRT_INP  all  --  anywhere             anywhere            

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
DOCKER-USER  all  --  anywhere             anywhere            
DOCKER-ISOLATION-STAGE-1  all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
LIBVIRT_FWX  all  --  anywhere             anywhere            
LIBVIRT_FWI  all  --  anywhere             anywhere            
LIBVIRT_FWO  all  --  anywhere             anywhere            

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
LIBVIRT_OUT  all  --  anywhere             anywhere            

Chain DOCKER (3 references)
target     prot opt source               destination         

Chain DOCKER-ISOLATION-STAGE-1 (1 references)
target     prot opt source               destination         
DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            

Chain DOCKER-ISOLATION-STAGE-2 (3 references)
target     prot opt source               destination         
DROP       all  --  anywhere             anywhere            
DROP       all  --  anywhere             anywhere            
DROP       all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            

Chain DOCKER-USER (1 references)
target     prot opt source               destination         
RETURN     all  --  anywhere             anywhere            

Chain LIBVIRT_FWI (1 references)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             192.168.122.0/24     ctstate RELATED,ESTABLISHED
REJECT     all  --  anywhere             anywhere             reject-with icmp-port-unreachable

Chain LIBVIRT_FWO (1 references)
target     prot opt source               destination         
ACCEPT     all  --  192.168.122.0/24     anywhere            
REJECT     all  --  anywhere             anywhere             reject-with icmp-port-unreachable

Chain LIBVIRT_FWX (1 references)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            

Chain LIBVIRT_INP (1 references)
target     prot opt source               destination         
ACCEPT     udp  --  anywhere             anywhere             udp dpt:domain
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:domain
ACCEPT     udp  --  anywhere             anywhere             udp dpt:bootps
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:67

Chain LIBVIRT_OUT (1 references)
target     prot opt source               destination         
ACCEPT     udp  --  anywhere             anywhere             udp dpt:domain
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:domain
ACCEPT     udp  --  anywhere             anywhere             udp dpt:bootpc
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:68
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# systemctl stop docker
Warning: Stopping docker.service, but it can still be activated by:
  docker.socket
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
     Active: inactive (dead) since Mon 2023-04-24 10:25:12 CEST; 6s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
    Process: 1431 ExecStart=/usr/sbin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock $DOC>
   Main PID: 1431 (code=exited, status=0/SUCCESS)
        CPU: 2.126s

Apr 24 08:03:45 g22 dockerd[1431]: time="2023-04-24T08:03:45.350646291+02:00" level=info msg="Docker dae>
Apr 24 08:03:45 g22 dockerd[1431]: time="2023-04-24T08:03:45.351278934+02:00" level=info msg="Daemon has>
Apr 24 08:03:45 g22 systemd[1]: Started Docker Application Container Engine.
Apr 24 08:03:45 g22 dockerd[1431]: time="2023-04-24T08:03:45.385709858+02:00" level=info msg="API listen>
Apr 24 10:25:12 g22 systemd[1]: Stopping Docker Application Container Engine...
Apr 24 10:25:12 g22 dockerd[1431]: time="2023-04-24T10:25:12.527270286+02:00" level=info msg="Processing>
Apr 24 10:25:12 g22 dockerd[1431]: time="2023-04-24T10:25:12.530251163+02:00" level=info msg="Daemon shu>
Apr 24 10:25:12 g22 systemd[1]: docker.service: Succeeded.
Apr 24 10:25:12 g22 systemd[1]: Stopped Docker Application Container Engine.
Apr 24 10:25:12 g22 systemd[1]: docker.service: Consumed 2.126s CPU time.
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
LIBVIRT_INP  all  --  anywhere             anywhere            

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         
DOCKER-USER  all  --  anywhere             anywhere            
DOCKER-ISOLATION-STAGE-1  all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
DOCKER     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             anywhere            
LIBVIRT_FWX  all  --  anywhere             anywhere            
LIBVIRT_FWI  all  --  anywhere             anywhere            
LIBVIRT_FWO  all  --  anywhere             anywhere            

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
LIBVIRT_OUT  all  --  anywhere             anywhere            

Chain DOCKER (3 references)
target     prot opt source               destination         

Chain DOCKER-ISOLATION-STAGE-1 (1 references)
target     prot opt source               destination         
DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
DOCKER-ISOLATION-STAGE-2  all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            

Chain DOCKER-ISOLATION-STAGE-2 (3 references)
target     prot opt source               destination         
DROP       all  --  anywhere             anywhere            
DROP       all  --  anywhere             anywhere            
DROP       all  --  anywhere             anywhere            
RETURN     all  --  anywhere             anywhere            

Chain DOCKER-USER (1 references)
target     prot opt source               destination         
RETURN     all  --  anywhere             anywhere            

Chain LIBVIRT_FWI (1 references)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             192.168.122.0/24     ctstate RELATED,ESTABLISHED
REJECT     all  --  anywhere             anywhere             reject-with icmp-port-unreachable

Chain LIBVIRT_FWO (1 references)
target     prot opt source               destination         
ACCEPT     all  --  192.168.122.0/24     anywhere            
REJECT     all  --  anywhere             anywhere             reject-with icmp-port-unreachable

Chain LIBVIRT_FWX (1 references)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            

Chain LIBVIRT_INP (1 references)
target     prot opt source               destination         
ACCEPT     udp  --  anywhere             anywhere             udp dpt:domain
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:domain
ACCEPT     udp  --  anywhere             anywhere             udp dpt:bootps
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:67

Chain LIBVIRT_OUT (1 references)
target     prot opt source               destination         
ACCEPT     udp  --  anywhere             anywhere             udp dpt:domain
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:domain
ACCEPT     udp  --  anywhere             anywhere             udp dpt:bootpc
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:68
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# vim ipdefault
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# vim ipdefault
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# mv ipdefault ipdefault.sh
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# ls
ipdefault.sh
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# vim ipdefault.sh 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# vim ipdefault.sh  ip_default.sh
2 files to edit
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# mv ipdefault.sh  ip_default.sh
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# ls
ip_default.sh
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# vim ip_default.sh 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# cp ip_default.sh ip-01-input.sh
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# systemctl status apache2
● apache2.service - The Apache HTTP Server
     Loaded: loaded (/lib/systemd/system/apache2.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2023-04-24 09:21:51 CEST; 2h 2min ago
       Docs: https://httpd.apache.org/docs/2.4/
    Process: 14376 ExecReload=/usr/sbin/apachectl graceful (code=exited, status=0/SUCCESS)
   Main PID: 11885 (apache2)
      Tasks: 11 (limit: 9383)
     Memory: 57.6M
        CPU: 1.256s
     CGroup: /system.slice/apache2.service
             ├─11885 /usr/sbin/apache2 -k start
             ├─14380 /usr/sbin/apache2 -k start
             ├─14381 /usr/sbin/apache2 -k start
             ├─14382 /usr/sbin/apache2 -k start
             ├─14383 /usr/sbin/apache2 -k start
             ├─14526 /usr/sbin/apache2 -k start
             ├─14532 /usr/sbin/apache2 -k start
             ├─14534 /usr/sbin/apache2 -k start
             ├─14546 /usr/sbin/apache2 -k start
             ├─14550 /usr/sbin/apache2 -k start
             └─14556 /usr/sbin/apache2 -k start

Apr 24 09:21:51 g22 systemd[1]: Starting The Apache HTTP Server...
Apr 24 09:21:51 g22 systemd[1]: Started The Apache HTTP Server.
Apr 24 09:38:25 g22 systemd[1]: Reloading The Apache HTTP Server.
Apr 24 09:38:26 g22 systemd[1]: Reloaded The Apache HTTP Server.
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# vim /etc/xinetd.d/http-2
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# cd /etc/xinetd.d/
root@g22:/etc/xinetd.d# cp http-2 http-3
root@g22:/etc/xinetd.d# vim http-3
root@g22:/etc/xinetd.d# cp http-2 http-4
root@g22:/etc/xinetd.d# vim http-4
root@g22:/etc/xinetd.d# cp http-2 daytime-2
çroot@g22:/etc/xinetd.d# cp http-2 daytime-2
root@g22:/etc/xinetd.d# vim daytime
root@g22:/etc/xinetd.d# vim daytime
daytime      daytime-2    daytime-udp  
root@g22:/etc/xinetd.d# vim daytime-
daytime-2    daytime-udp  
root@g22:/etc/xinetd.d# vim daytime-2
root@g22:/etc/xinetd.d# cp daytime-2 daytime-3
root@g22:/etc/xinetd.d# vim daytime-3 
root@g22:/etc/xinetd.d# cp daytime-2 daytime-4
root@g22:/etc/xinetd.d# vim daytime-4 
root@g22:/etc/xinetd.d# cp http-
http-2  http-3  http-4  
root@g22:/etc/xinetd.d# cp http-* /home/
anna/   guest/  marta/  origen/ pere/   prova1/ prova2/ users/  
root@g22:/etc/xinetd.d# cp http-* /home/users/inf/hisx2/a191466js/
26-telnet-client.py                 m06exam-juansanchez/
basex/                              .mozilla/
.bash_history                       Music/
.bash_logout                        phpMyAdmin-5.1.1-all-languages.zip
.bashrc                             Pictures/
.cache/                             .pki/
.config/                            practica.schema
cv                                  .profile
.dbus/                              .psql_history
Desktop/                            Public/
dns-server                          .putty/
.docker/                            .python_history
docker/                             .redhat/
Documents/                          .ssh/
Downloads/                          Templates/
.fltk/                              Videos/
.gnome/                             .vim/
.gnupg/                             .viminfo
.java/                              .vnc/
.lemminx/                           .vscode/
.local/                             
root@g22:/etc/xinetd.d# cp http-* /home/users/inf/hisx2/a191466js/Documents/m11/uf2/
certificados/ kerberos/     tunels/       
root@g22:/etc/xinetd.d# cp http-* /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/
apuntes/  iptables/ 
root@g22:/etc/xinetd.d# cp http-* /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables/ip
ip-01-input.sh  ip_default.sh   
root@g22:/etc/xinetd.d# cp http-* /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables/.
root@g22:/etc/xinetd.d# cp daytime- /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables/.
daytime-2    daytime-3    daytime-4    daytime-udp  
root@g22:/etc/xinetd.d# cp daytime-2 daytime-3 /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables/.
daytime-3
root@g22:/etc/xinetd.d# cp daytime-2 daytime-3 /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables/.
daytime-3
root@g22:/etc/xinetd.d# cp daytime-2 daytime-3 daytime-4 /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables/.
root@g22:/etc/xinetd.d# ls
chargen      daytime	daytime-3  daytime-udp	discard-udp  echo-udp  http-3  servers	 time
chargen-udp  daytime-2	daytime-4  discard	echo	     http-2    http-4  services  time-udp
root@g22:/etc/xinetd.d# systemctl status xinetd.service 
● xinetd.service - LSB: Starts or stops the xinetd daemon.
     Loaded: loaded (/etc/init.d/xinetd; generated)
     Active: active (running) since Mon 2023-04-24 08:03:44 CEST; 3h 31min ago
       Docs: man:systemd-sysv-generator(8)
      Tasks: 1 (limit: 9383)
     Memory: 832.0K
        CPU: 140ms
     CGroup: /system.slice/xinetd.service
             └─1727 /usr/sbin/xinetd -pidfile /run/xinetd.pid -stayalive -inetd_compat -inetd_ipv6

Apr 24 08:03:44 g22 xinetd[1727]: Reading included configuration file: /etc/xinetd.d/discard [file=/etc/>
Apr 24 08:03:44 g22 xinetd[1727]: Reading included configuration file: /etc/xinetd.d/discard-udp [file=/>
Apr 24 08:03:44 g22 xinetd[1727]: Reading included configuration file: /etc/xinetd.d/echo [file=/etc/xin>
Apr 24 08:03:44 g22 xinetd[1727]: Reading included configuration file: /etc/xinetd.d/echo-udp [file=/etc>
Apr 24 08:03:44 g22 xinetd[1727]: Reading included configuration file: /etc/xinetd.d/servers [file=/etc/>
Apr 24 08:03:44 g22 xinetd[1727]: Reading included configuration file: /etc/xinetd.d/services [file=/etc>
Apr 24 08:03:44 g22 xinetd[1727]: Reading included configuration file: /etc/xinetd.d/time [file=/etc/xin>
Apr 24 08:03:44 g22 xinetd[1727]: Reading included configuration file: /etc/xinetd.d/time-udp [file=/etc>
Apr 24 08:03:44 g22 xinetd[1727]: 2.3.15.3 started with libwrap loadavg labeled-networking options compi>
Apr 24 08:03:44 g22 xinetd[1727]: Started working: 3 available services
root@g22:/etc/xinetd.d# telnet localhost 13
Trying ::1...
Connected to localhost.
Escape character is '^]'.
24 APR 2023 11:36:27 CEST
Connection closed by foreign host.
root@g22:/etc/xinetd.d# telnet localhost 3013
Trying ::1...
Trying 127.0.0.1...
telnet: Unable to connect to remote host: Connection refused
root@g22:/etc/xinetd.d# telnet localhost 2013
Trying ::1...
Trying 127.0.0.1...
telnet: Unable to connect to remote host: Connection refused
root@g22:/etc/xinetd.d# telnet localhost 5013
Trying ::1...
Trying 127.0.0.1...
telnet: Unable to connect to remote host: Connection refused
root@g22:/etc/xinetd.d# cd /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# vim ip-01-input.sh 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# vim ip-01-input.sh 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# 



# root interactivo
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ ip a status
Command "status" is unknown, try "ip address help".
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 1c:1b:0d:95:a1:b4 brd ff:ff:ff:ff:ff:ff
    inet 10.200.245.222/24 brd 10.200.245.255 scope global dynamic noprefixroute enp1s0
       valid_lft 12339sec preferred_lft 12339sec
    inet6 fe80::1e1b:dff:fe95:a1b4/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 52:54:00:d3:51:bc brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
4: br-f79f03bc45f0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:8e:47:53:80 brd ff:ff:ff:ff:ff:ff
    inet 172.18.0.1/16 brd 172.18.255.255 scope global br-f79f03bc45f0
       valid_lft forever preferred_lft forever
5: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:7b:33:e5:b3 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
       valid_lft forever preferred_lft forever
6: br-a27c9877217c: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:da:23:89:ae brd ff:ff:ff:ff:ff:ff
    inet 172.19.0.1/16 brd 172.19.255.255 scope global br-a27c9877217c
       valid_lft forever preferred_lft forever
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ pwd
/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ su -
Password: 
root@g22:~# cd /home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# ls
ip_default.sh
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# bash ip_default.sh 
Bad argument `-'
Try `iptables -h' or 'iptables --help' for more information.
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  10.200.245.0/24      anywhere            

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             10.200.245.0/24     
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# bash ip_default.sh 
Bad argument `-'
Try `iptables -h' or 'iptables --help' for more information.
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  10.200.245.0/24      anywhere            

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             10.200.245.0/24     
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# bash ip-01-input.sh 
Bad argument `-'
Try `iptables -h' or 'iptables --help' for more information.
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# bash ip-01-input.sh 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  10.200.245.0/24      anywhere            
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:daytime
REJECT     tcp  --  anywhere             anywhere             tcp dpt:2013 reject-with icmp-port-unreachable
DROP       tcp  --  anywhere             anywhere             tcp dpt:3013

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             10.200.245.0/24     
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  10.200.245.0/24      anywhere            
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:daytime
REJECT     tcp  --  anywhere             anywhere             tcp dpt:2013 reject-with icmp-port-unreachable
DROP       tcp  --  anywhere             anywhere             tcp dpt:3013

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             10.200.245.0/24     
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  10.200.245.0/24      anywhere            
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:daytime
REJECT     tcp  --  anywhere             anywhere             tcp dpt:2013 reject-with icmp-port-unreachable
DROP       tcp  --  anywhere             anywhere             tcp dpt:3013

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             10.200.245.0/24     
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# uname -a
Linux g22 5.10.0-18-amd64 #1 SMP Debian 5.10.140-1 (2022-09-02) x86_64 GNU/Linux
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# systemctl stop nftables
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# systemctl status nftables
● nftables.service - nftables
     Loaded: loaded (/lib/systemd/system/nftables.service; disabled; vendor preset: enabled)
     Active: inactive (dead)
       Docs: man:nft(8)
             http://wiki.nftables.org
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# bash ip-01-input.sh 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# systemctl disable nftables
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# systemctl status nftables
● nftables.service - nftables
     Loaded: loaded (/lib/systemd/system/nftables.service; disabled; vendor preset: enabled)
     Active: inactive (dead)
       Docs: man:nft(8)
             http://wiki.nftables.org
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# bash ip-01-input.sh 
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# 

















root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  10.200.245.0/24      anywhere            
ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:daytime
REJECT     tcp  --  anywhere             anywhere             tcp dpt:2013 reject-with icmp-port-unreachable
DROP       tcp  --  anywhere             anywhere             tcp dpt:3013

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination         
ACCEPT     all  --  anywhere             anywhere            
ACCEPT     all  --  anywhere             10.200.245.0/24     
root@g22:/home/users/inf/hisx2/a191466js/Documents/m11/uf3/iptable/iptables# 


# como guest para mi pc
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ telnet localhost 13
Trying ::1...
Connected to localhost.
Escape character is '^]'.
24 APR 2023 11:45:57 CEST
Connection closed by foreign host.
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ telnet localhost 2013
Trying ::1...
Trying 127.0.0.1...
telnet: Unable to connect to remote host: Connection refused
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ ssh guest@i25
The authenticity of host 'i25 (10.200.243.225)' can't be established.
ECDSA key fingerprint is SHA256:3eKFczuZXaPFSSYxDzoNGZiz1fQynivnit8gShpECiA.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'i25,10.200.243.225' (ECDSA) to the list of known hosts.
guest@i25's password: 
Permission denied, please try again.
guest@i25's password: 
Permission denied, please try again.
guest@i25's password: 
guest@i25: Permission denied (publickey,password).
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ ssh guest@i25
guest@i25's password: 
Permission denied, please try again.
guest@i25's password: 
Permission denied, please try again.
guest@i25's password: 
guest@i25: Permission denied (publickey,password).
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ ssh guest@g25
The authenticity of host 'g25 (10.200.245.225)' can't be established.
ECDSA key fingerprint is SHA256:B1bmwmvPKfw3KAw89FHcn+Mifp/Ke+IuewNDD64nXjk.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'g25,10.200.245.225' (ECDSA) to the list of known hosts.
guest@g25's password: 
Linux g25 5.10.0-21-amd64 #1 SMP Debian 5.10.162-1 (2023-01-21) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Mon Apr 24 11:47:46 2023 from 10.200.245.211
guest@g25:~$ telnet 10.200.245.222 13
Trying 10.200.245.222...
Connected to 10.200.245.222.
Escape character is '^]'.
24 APR 2023 11:48:19 CEST
Connection closed by foreign host.
guest@g25:~$ telnet 10.200.245.222 2013
Trying 10.200.245.222...
telnet: Unable to connect to remote host: Connection refused
guest@g25:~$ telnet 10.200.245.222 3013
Trying 10.200.245.222...
telnet: Unable to connect to remote host: Connection refused
guest@g25:~$ 
logout
Connection to g25 closed.
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ ssh guest@g25
guest@g25's password: 
Linux g25 5.10.0-21-amd64 #1 SMP Debian 5.10.162-1 (2023-01-21) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Mon Apr 24 11:48:18 2023 from 10.200.245.218
guest@g25:~$ telnet 10.200.245.222 2013
Trying 10.200.245.222...
telnet: Unable to connect to remote host: Connection refused
guest@g25:~$ telnet 10.200.245.222 3013
Trying 10.200.245.222...
telnet: Unable to connect to remote host: Connection refused
guest@g25:~$ telnet 10.200.245.222 13
Trying 10.200.245.222...
Connected to 10.200.245.222.
Escape character is '^]'.
24 APR 2023 12:00:06 CEST
Connection closed by foreign host.
guest@g25:~$ 
logout
Connection to g25 closed.
a191466js@g22:~/Documents/m11/uf3/iptable/iptables$ ssh guest@g25
guest@g25's password: 
Linux g25 5.10.0-21-amd64 #1 SMP Debian 5.10.162-1 (2023-01-21) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Mon Apr 24 11:59:59 2023 from 10.200.245.222
guest@g25:~$ telnet 10.200.245.222 13
Trying 10.200.245.222...
Connected to 10.200.245.222.
Escape character is '^]'.
24 APR 2023 12:01:08 CEST
Connection closed by foreign host.
guest@g25:~$ telnet 10.200.245.222 2013
Trying 10.200.245.222...
telnet: Unable to connect to remote host: Connection refused
guest@g25:~$ telnet 10.200.245.222 3013
Trying 10.200.245.222...
telnet: Unable to connect to remote host: Connection refused
guest@g25:~$ 

