#! /bin/bash
# @juan22 ASIX M11 Curs 22-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 10.200.245.222/24 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.222/24 -j ACCEPT

# Exemples regles input
# =====================================================
# Obrir a tothom el port 13
iptables -A INPUT -p tcp --dport 13 -j ACCEPT

# Tancar a tothom el port 2013 (reject)
iptables -A INPUT -p tcp --dport 2013 -j REJECT

# Tancar a tothom el port 2013 (drop)
iptables -A INPUT -p tcp --dport 3013 -j DROP

# Tancar al g25 el port 4013 pero obert a tothom
iptables -A INPUT -p tcp --dport 4013 -s 10.200.245.225 -j DROP
iptables -A INPUT -p tcp --dport 4013 -j ACCEPT

# port tancat a tothom, pero obert a hisx2(aula) pero tancat a g25
iptables -A INPUT -p tcp --dport 5013 -s 10.200.245.225 -j REJECT
iptables -A INPUT -p tcp --dport 5013 -s 10.200.245.0/24 -j ACCEPT
iptables -A INPUT -p tcp --dport 5013 -j DROP

# port 6013, obert tothom, tancat aula, obert g25
iptables -A INPUT -p tcp --dport 6013 -s 10.200.245.225 -j ACCEPT
iptables -A INPUT -p tcp --dport 6013 -s 10.200.245.0/24 -j DROP
iptables -A INPUT -p tcp --dport 6013 -j ACCEPT

# Tancar acces als ports del 3000 al 8000
#iptables -A INPUT -p tcp --dport 3000:8000 -j REJECT

# Barrera final (tancar ports 0:1023)
#iptables -A INPUT -p tcp --dport 1:1024 -j REJECT
