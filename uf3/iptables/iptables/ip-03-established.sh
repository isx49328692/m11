#! /bin/bash
# @juan22 ASIX M11 Curs 22-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 10.200.245.222 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.222 -j ACCEPT

# Exemples de trafic related, established
# dos sentits de comunicacio

# permetre navegar per la web (port 80)
iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT

# Somos un servidor web, permito que me hagan peticiones web y permito las respuestas
iptables -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state --state RELATED,ESTABLISHED -j ACCEPT

# Som un servidor web, pero al g25 esta vetat
iptables -A INPUT -p tcp --dport 80 -s 10.200.245.225 -j DROP
iptable -A INPUT -p tcp --dport 80 -j ACCEPT
iptables -A OUTPUT -p tcp --sport 80 -m tcp -m state RELEATED,ESTABLISHED -j ACCEPT
