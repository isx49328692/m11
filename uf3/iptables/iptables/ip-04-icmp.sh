#! /bin/bash
# @juan22 ASIX M11 Curs 22-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 10.200.245.222 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.222 -j ACCEPT

# Regles ucmp
#------------------------------------------------------------

# No permetem fer pings a l'exterior
#iptables -A OUTPUT -p icmp --icmp-type 8 -j DROP

# Nosaltres no permetem fer ping cap al g25
#iptables -A OUTPUT -p icmp --icmp-type 8 -d 10.200.245.225 -j DROP

# La nostra maquina no respon als pings que rep
#iptables -A OUTPUT -p icmp --icmp-type 0 -j DROP

# La nostra maquina no permet rebre pings
#iptables -A INPUT -p icmp --icmp-type 8 -j DROP

# ---------------------------------------------------------------------

# Regles explicites permeto fer ping a la meva maquina
iptables -A OUTPUT -p icmp --icmp-type 8 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type 0 -j ACCEPT

# Regles explicites per denegar la nostra maquina respondre pings
iptables -A INPUT -p icmp --icmp-type 8 -j DROP
iptables -A OUTPUT -p icmp --icmp-type 0 -j DROP
