#! /bin/bash
# @juan22 ASIX M11 Curs 22-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 10.200.245.222 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.222 -j ACCEPT

# Fer nat para la red docker NetA (19) i NetB (22)
iptables -t nat -A POSTROUTING -s 172.19.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/16 -j MASQUERADE

# Regles forward
# RedA no puede acceder a la redB
#iptables -A FORWARD -s 172.19.0.0/16 -d 172.22.0.0/16 -j REJECT

# La RedA no puede acceder a B1
#iptables -A FORWARD -s 172.19.0.0/16 -d 172.22.0.2 -j REJECT

# La RedA no puede acceder a ningun puerto 13
iptables -A FORWARD -p tcp -s 172.19.0.0/16 --dport 13 -j REJECT
