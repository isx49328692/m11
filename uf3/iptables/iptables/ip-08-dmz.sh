#! /bin/bash
# @juan22 ASIX M11 Curs 22-23
# iptables
#
# echo 1 > /proc/sys/net/ipv4/ip_forward

# Regles flush
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# politica por defecto
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# obrir el localhost
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# obrir la nostra ip
iptables -A INPUT -s 10.200.245.222 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.222 -j ACCEPT

# Fer nat para la red docker NetA (19) i NetB (22)
iptables -t nat -A POSTROUTING -s 172.21.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 172.22.0.0/16 -j MASQUERADE
iptables -t nat -A POSTROUTING -s 10.0.0.0/16 -j MASQUERADE

#1 de la xarxaA només es pot accedir del router/fireall als serveis: ssh i daytime(13)
iptables -A INPUT -s 172.21.0.0/16 -p tcp --dport 22 -j ACCEPT #Acepta netA --> ssh(22)
iptables -A INPUT -s 172.21.0.0/16 -p tcp --dport 13 -j ACCEPT #Acepta netA --> daytime(13)
iptables -A INPUT -s 172.21.0.0/16 -j REJECT #Niega la entrada a cualquiera de la netA

#2 de la xarxaA només es pot accedir a l'exterior als serveis web, ssh i daytime(2013)
iptables -A FORWARD -s 172.21.0.0/16 -p tcp --dport 80 -j ACCEPT #Acepta netA --> web(80)
iptables -A FORWARD -d 172.21.0.0/16 -p tcp --sport 80 -m state --state ESTABLISHED,RELATED -j ACCEPT #Acepta respuesta web(80)
iptables -A FORWARD -s 172.21.0.0/16 -p tcp --dport 22 -j ACCEPT #Acepta netA --> ssh(22)
iptables -A FORWARD -d 172.21.0.0/16 -p tcp --sport 22 -m state --state ESTABLISHED,RELATED -j ACCEPT #Acepta respuesta ssh(22)
iptables -A FORWARD -s 172.21.0.0/16 -p tcp --dport 2013 -j ACCEPT #Acepta netA --> daytime(2013)
iptables -A FORWARD -d 172.21.0.0/16 -p tcp --sport 2013 -j -m state --state ESTABLISHED,RELATED -j ACCEPT #Acepta respuesta --> netA
iptables -A FORWARD -s 172.21.0.0/16 -j REJECT #Niega cualquier salida de NetA
iptables -A FORWARD -d 172.21.0.0/16 -j REJECT #Niega cualquier entrada --> NetA

#3 
