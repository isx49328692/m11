#!/bin/bash

# Borramos reglas iptables
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Politicas predeterminadas
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT

# Trafico interno de la red
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Abrir nuestra IP
iptables -A INPUT -s 10.200.245.222 -j ACCEPT
iptables -A OUTPUT -d 10.200.245.222 -j ACCEPT

# El router hace NAT de la red interna
iptables -A POSTROUTING -t nat -s 172.54.0.0/16 -o enp1s0 -j MASQUERADE

# El puerto 3001 del router redirige al servicio del host1 y el puerto 3002 al servicio del host2
iptables -A PREROUTING -t nat -i enp1s0 -p tcp --dport 3001 -j DNAT --to 172.54.0.2:13
iptables -A PREROUTING -t nat -i enp1s0 -p tcp --dport 3002 -j DNAT --to 172.54.0.3:14

# El puerto 4001 del router redirige al servicio SSH del host1 y el puerto 4002 al servicio SSH del host2
iptables -A PREROUTING -t nat -i enp1s0 -p tcp --dport 4001 -j DNAT --to 172.54.0.2:22
iptables -A PREROUTING -t nat -i enp1s0 -p tcp --dport 4002 -j DNAT --to 172.54.0.3:22

# El puerto 4000 del router redirige al servicio SSH del propio router
iptables -A PREROUTING -t nat -i enp1s0 -p tcp --dport 4000 -j DNAT --to 172.54.0.1:22

# Los hosts de la red privada interna pueden navegar por internet pero no acceder a otros servicios
iptables -A FORWARD -s 172.54.0.0/16 -p tcp --dport 80 -o enp1s0 -j ACCEPT
iptables -A FORWARD -d 172.54.0.0/16 -p tcp --sport 80 -i enp1s0 -j ACCEPT
iptables -A FORWARD -s 172.54.0.0/16 -o enp1s0 -j DROP
iptables -A FORWARD -d 172.54.0.0/16 -i enp1s0 -j DROP

# Los hosts de la red interna no pueden hacer ping al exterior (8 --> echo-request)
iptables -A FORWARD -s 172.54.0.0/16 -p icmp --icmp-type=8 -o enp1s0 -j DROP

# El router no responde a los pings que recibe pero puede hacer ping (0 --> echo-reply)
iptables -A INPUT -p icmp --icmp-type=8 -j ACCEPT
iptables -A OUTPUT -p icmp --icmp-type=0 -j DROP
