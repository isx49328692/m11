# Practica iptable aula (Net 172.54.0.0/16)

## Configurar router
Asignar direccion ip a la interfaz del router que esta concetado a la red del aula.

    ip address add 172.200.0.1/16 dev enp5s0

Habilitar enrutamiento

    echo 1 > /proc/sys/net/ipv4/ip_forward

### Configurar firewall router
* Borramos reglas iptables (flush)
    iptables -F
    iptables -X
    iptables -Z
    iptables -t nat -F

* Politicas predeterminadas
    
    iptables -P INPUT ACCEPT \
    iptables -P OUTPUT ACCEPT \
    iptables -P FORWARD ACCEPT \
    iptables -t nat -P PREROUTING ACCEPT \
    iptables -t nat -P POSTROUTING ACCEPT \

* trafico interno de la red (abrir localhost)

    iptables -A INPUT -i lo -j ACCEPT \
    iptables -A OUTPUT -o lo -j ACCEPT \

* Abrir nuestra ip ¿?¿?

    iptables -A INPUT -s 10.200.245.222 -j ACCEPT \
    iptables -A OUTPUT -d 10.200.245.222 -j ACCEPT \

* El router fa NAT de la xarxa intern (enmascara)

    iptables -A POSTROUTING -t nat -s 172.200.0.0/16 -o enp5s0 -j MASQUERADE \

* en el router el port 3001 porta a un servei del host1 i el port 3002 a un servei del host2. (--to ip:puerto-servicio)

    iptables -A PREROUTING -t nat -i enp5s0 -p tcp --dport 3001 -j DNAT --to 172.200.0.2:PuertoServicio \
    iptables -A PREROUTING -t nat -i enp5s0 -p tcp --dport 3002 -j DNAT --to 172.200.0.3:PuertoServicio \

* en el router el port 4001 porta al servei ssh del host1 i el port 4002 al servei ssh del host2.

    iptables -A PREROUTING -t nat -i enp5s0 -p tcp --dport 4001 -j DNAT --to 172.200.0.2:22 \
    iptables -A PREROUTING -t nat -i enp5s0 -p tcp --dport 4002 -j DNAT --to 172.200.0.3:22 \

* en el router el port 4000 porta al servei ssh del propi router. (¿-i lo?)
    iptables -A PREROUTING -t nat -i enp5s0 -p tcp --dport 4000 -j DNAT --to 172.200.0.1:22

* als hosts de la xarxa privada interna se'ls permet navegar per internet, però no cap altre accés a internet. (¿solo 80? y 443... hay más, reglas DROP denegaran todo)

    iptables -A FORWARD -s 172.200.0.0/16 -p tcp --dport 80 -o enp5s0 -j ACCEPT \
    iptables -A FORWARD -d 172.200.0.0/16 -p tcp --sport 80 -i enp5s0 -j ACCEPT \
    iptables -A FORWARD -s 172.200.0.0/16 -o enp5s0 -j DROP \
    iptables -A FORWARD -d 172.200.0.0/16 -i enp5s0 -j DROP \

* no es permet que els hosts de la xarxa interna facin ping a l'exterior.

    iptables -A FORWARD -s 172.200.0.0/16 -p icmp --icmp-type=8 -o enp5s0 -j DROP \

* el router no contesta als pings que rep, però si que pot fer ping.

    iptables -A INPUT -p icmp --icmp-type=8 -j ACCEPT \
    iptables -A OUTPUT -p icmp --icmp-type=0 -j DROP \



## Configuracion hoss1
Iniciamos sistema en modo1

    ip link set lo up

Activamos interfaz de red

    sudo ip link set enp5s0 up

Asignamos direccion ip

    sudo ip address add 172.20.0.2/16 dev enp5s0

Añadimos ruta predeterminada (host1 --> Router)

    sudo ip route default via 172.20.0.1

Verificacion

    ip address show \
    ip route show

## Configuracion host2
Iniciamos sistema en modo1

    ip link set lo up

Activamos interfaz de red

    sudo ip link set enp5s0 up

Asignamos direccion ip

    sudo ip address add 172.20.0.3/16 dev enp5s0

Añadimos ruta predeterminada (host1 --> Router)

    sudo ip route default via 172.20.0.1

Verificacion

    ip address show
    ip route show