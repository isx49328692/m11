# Exercici: Practicar fer el seguiment de NAT i Port Forwarding
## Topologia

S’ha implementat una topologia amb docker segons el model següent: \
◦ Neta 172.20.0.0/16, hosts a1(.2) i a2(.3). \
◦ Netb 172.30.0.0./16, hosts b1(.2) i b2(.3). \
◦ Netdmz 10.0.0.0/8, servers s1(.2) i s2(.3). \

El router té les interfícies \
◦ docker0 per a la xarxa Netdmz \
◦ dockera per a la xarxa Neta \
◦ dockerb per a la xarxa Netb \
◦ eno1 per a la xarxa de l’aula. Useu l’adreça IP del host on esteu asseguts. \
◦ lo per el loopback \

1. El host Remot anomenat Remote accedeix al port 2222 del Router que en realitat és \
redirigit al servei SSH del servidor s1 de la DMZ. Indica ip-origen, port-origen, \
ip-destí, port-destí en cada cas: \
* Camí d’anada: Ip-origen:Port-origen || IP-destí:Port-destí
    * Sortida en el host remot: ipRemot:dinamic1 (d1) || ipRouter:2222
    * Entrada en el router: ipRemot:d1 || ipRouter:2222
    * Sortida el Router: ipRemot:d1 || 10.0.0.2:22
    * Entrada en el host s1: ipRemot:d1 || 10.0.0.2:22

* Camí de tornada: Ip-origen:Port-origen || IP-destí:Port-destí
    * Sortida del host s1: 10.0.0.2:22 || ipRemot:d1
    * Entrada al router: 10.0.0.2:22 || ipRemot:d1
    * Sortida del router: ipRouter:2222 || ipRemot:d1
    * Entrada en el host remot: ipRouter:2222 || ipRemot:d1

2. El host a1 es connecta al servei daytime (port 13) del host extern Remote Indica \
ip-origen, port-origen, ip-destí, port-destí en cada cas: \
* Camí d’anada: Ip-origen:Port-origen || IP-destí:Port-destí
    * Sortida en el host a1: 172.20.0.2:d1 || ipRemot:13
    * Entrada en el router: 172.20.0.2:d1 || ipRemot:13
    * Sortida el Router: ipRouter:d2 || ipRemot:13
    * Entrada en el host remot: ipRouter:d2 || ipRemot:13

* Camí de tornada: Ip-origen:Port-origen || IP-destí:Port-destí
    * Sortida del host remot: ipRemot:13 || ipRouter:d2
    * Entrada al router: ipRemot:13 || ipRouter:d2
    * Sortida del router: ipRemot:13 || 172.20.0.2
    * Entrada el host a1: ipRemot:13 || 172.20.0.2

3. El B2 se connecta al puerto 80 de remoto (NAT)
* Camí d’anada: Ip-origen:Port-origen || IP-destí:Port-destí
    * Sortida en el host b2: 172.30.0.3:d1 || ipRemot:80
    * Entrada en el router: 172.30.0.3:d1 || ipRemot:80
    * Sortida el Router: ipRouter: ipRouter:d2 || ipRemot:80
    * Entrada en el host remot: ipRouter:d2 || ipRemot:80

* Camí de tornada: Ip-origen:Port-origen || IP-destí:Port-destí
    * Sortida del host remot: ipRemot:80 || ipRouter:d2
    * Entrada al router: ipRemot:80 || ipRouter:d2
    * Sortida del router: ipRemot:80 || 172.30.0.3:d1
    * Entrada el host b2: ipRemot:80 || 172.30.0.3:d1


4. Redireccionamiento de puertos, Remot se conecta con al puerto 22 de b1
* Camí d’anada: Ip-origen:Port-origen || IP-destí:Port-destí
    * Sortida en el host b1: ipRemot:d1 || ipRouter:6022
    * Entrada en el router: ipRemot:d1 || ipRouter:6022
    * Sortida el Router: ipRemot:d1 || 172.30.0.2:22
    * Entrada en el host remot: ipRemot:d1 || 172.30.0.2:22

* Camí de tornada: Ip-origen:Port-origen || IP-destí:Port-destí
    * Sortida del host remot: 172.30.0.2:22 || ipRemot:d1
    * Entrada al router: 172:30.0.2:22 || ipRemot:d1
    * Sortida del router: ipRouter:6022 || ipRemot:d1
    * Entrada el host b1: ipRouter:6022 || ipRemot:d1