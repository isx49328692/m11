UF1 --> data at rest --> datos estaticos (en el disco duro)
UF2 --> data at motion --> cuando los datos se mueven

# LVM - Logical Volume Manager (organizacion de datos)


            +-------------------+     +---------+
            | Sistema | XDatosX |     | XDatosX |
            +-------------------+     +---------+
        windows; c:        D:               E:
            +--------------------------------+
            |           Datos     D:         |
            +--------------------------------+

                                S.O
                            +----------------+
                +-------->| Sistema | Datos|<---+
                |         +-----------^----+    |
                |                     |         |
                |               +-----+         |
            +---|---------------|---------------|-------------+    
            |   |               |               |             |    
            |   | +-------------|----+    +-----|-------+     |    
            |   | | Sistema | Datos  |    | Datos | ....|     |    
            |   | +------------------+    +-------------+     |    
            |   |                         +--------+          |    
            |   +-------------------------| Sistema|          |    
            |                             +--------+          |    
            |                                                 |    
            +---------|-------------------------|-------------+    
                      |                         | ------------------> Pasa los datos a un
                     \ /                       \ /                      dd mas grande
            +--------------------------------------------+
            |  SISTEMA             |    DATOS            |
            +--------------------------------------------+


        +----+       +----+
        | LV |       | LV |
        +----+       +----+
         /|\           /|\
    +-----|-------------|-----------+   +------------------+                LV --> Logical Volume
    |     |     sda     |         X |   |                  |                PV --> Physical Volume
    | +---|----+    +---|-------+ X |   |       sdb        |<-+
    | |        |    |           | X |   +------------------+  |           
    | +--------+    +-----------+ X |                         |           *juntamos particiones para
    |       /|\                   X |                         |              formar un volume --> disco
    +--------|----------------------+                         |
             |                                                |
    +--------|------------------------+    fisic              |
    |   PV --+          | PV           |        /dev/loopø    |
    |       |           |              |                      |
    |       |           |              |                      |
    +-------|--------------------------+                      |
            |                                                 |
            |                                                 |
    +-------|--------------------------+                      |
    |   PV--+                          |    /dev/loop1        |
    +----------------------------------+                      |
    +----------------------------------+                      |
    |   PV +                           |    /dev/loop2        |
    +------|---------------------------+                      |
           |                                                  |
           +--------------------------------------------------+

---
---

## practicando

    dd if=/dev/zero of=disk01.img bs=1k count=100k status=progress
    dd if=/dev/zero of=disk02.img bs=1k count=100k status=progress
    dd if=/dev/zero of=disk03.img bs=1k count=100k status=progress

    ls -lh

    losetup /dev/loop1 disk01.img
    losetup /dev/loop1 disk02.img
    losetup /dev/loop1 disk03.img

*** Ahora tenemos tres discos ***

    lsblk
    tree /dev/disk

## lo marcamos como PV

    pvcreate /dev/loop1
    
    pvdisplay /dev/loop1

    pvcreate /dev/loop2 /dev/loop3
    
    pvdisplay

## Creamos discos

    vgcreate diskedt /dev/loop1 /dev/loop2

    vgdisplay
            //... Metadata Area 2 --> significa que lo forman 2 loops
            PE --> espacio de almacenamietno (blocs)

## Creamos particion logica

    lvcreate -L 50M -n sistema /dev/diskedt/sistema

    lvdisplay /dev/diskedt/sistema

    ls /dev/*

    lvcreate -L 150M -n dades /dev/diskedt --> No hay espacio, no lo hara
     |
     |
     +--> lvcreate -l100%FREE -n dades /dev/diskedt

## Montamos

### formateamos

    mkfs -t ext4 /dev/diskedt/sistema
    mkfs -t ext4 /dev/diskedt/dades

    mkdir /mnt/sistema
    mkdir /mnt/dades

### montamos

    mount /dev/diskedt/sistema /mnt/sistema
    mount /dev/diskedt/dades /mnt/dades

### metemos datos
    cp .... sistema
    cp .... dades

    df -h -t ext4

## Añadimos más espacio con loop3

    hexdump -c /dev/loop3

### añadimos el espacio

    vgextend diskedt /dev/loop3

    vgdisplay

### le damos espacio "extender" sistema

    lvextend -L +30M /dev/diskedt/sistema

    df -h -t ext4

### cambiamos tamaño de sistema de ficheros

    resize2fs /dev/diskedt/sistema

lvdisplay /dev/diskedt/sistema
    ...
    ...
    segments 2  --> ahora esta formado por 2

## Creamos un nuevo volumen - services

    lvcreate -L 60M -n services /dev/diskedt

    vgdisplay
        ...
        ...
        metadata Area 3 --> 3 volumenes; sistema, dades, services

## Reducir y mover

Para reducir primero se debe reducir el sistema de ficheros

    resize2fs /dev/diskedt/sistema --> No es posible en caliente, necesario desmontar

### desmontamos

    umount /mnt/sistema

### reducimos espacio

    e2fsck -f /dev/diskedt/sistema

    resize2fs /dev/diskedt/sistema 50M

### ahora reducimos

    lvreduce -L 50M -r /dev/diskedt/sistema

### Añadimos todo a dades

    lvextend -l +100%FREE /dev/diskedt/dades

### creamos nuevo disco

    dd if=/dev/zero of=disk04.img bs=1k count=500k status=progress

    losetup /dev/loop4 disk04.img

    pvcreate /dev/loop4

    vgextend diskedt /dev/loop4

    mount ---

    pvmove /dev/loop2 /dev/loop4
    pvmove /dev/loop3 /dev/loop4

## Eliminar un elemento que forma parte de un dd disco

    vgreduce diskedt /dev/loop2

## Eliminar todo

    umount /mnt/dades

    lvremove /dev/diskedt/dades /dev/diskedt/sistema /dev/diskedt/services

    vgremove diskedt

    losetup -a

    losetup -d /dev/loop{1..4}

    losetup -a